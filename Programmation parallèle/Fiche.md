# Mesure de performance
>[!Define] Speedup
>$S = {T_1(n) \over {T_s(n)}}$

>[!Define] Efficacité
>$E = {S \over s}$ (en pourcentage).

>[!important] Loi d'Amdahl
>Temps d'exécution = Temps parallélisable / p + Temps séquention
>$S_{latence}(n_{proc}) = {1 \over {1 -  F_p + {F_p \over n_{proc}}}}$
$s$ nombre de processeurs.
$F_p$ proportion de tâches parallélisables.

>[!important] Loi de Gustafson
>Garder le temps d'exécution mais faire plus de travail, plus de charge.
$S_{latence} = (1 - F_p) + s \times F_p = 1 + (s - 1) \times F_p$

---

# OpenMP
## Nombre de threads
```shell
#!/bin/bash
export OMP_NOM_THREADS = 8
./example01
```
```C
void omp_set_num_threads(num_threads);
#pragma omp parallel num_threads(num_threads)
```

## Informations
```C
int omp_get_num_threads();
int omp_get_thread_num();
int omp_get_max_threads();
int omp_get_num_procs();
int omp_in_parallel();
```

## Construction de région parallèle
```C
#pragma omp parallel [clause1] [clause2] ...
```

### Clauses:
- if (expression)
- num_threads(integer)
- private(list of var)
- shared(list of var)
- default(list of var)
- default(shared | none)
- firstprivate(list of var) ⟶ utilise l'initialisation précédente de la variable
- reduction(operator: list of var) exemple reduction +:b) ⟶ additionne les valeurs de b obtenues dans chaque threads en fin de région.
- copyin(list of var)

## Partage du travail
### For
```C
#pragma omp for [clauses]
	for loop
```
#### Schedule
```C
#pragma omp for schedule(type[,block size])
```
- **static**: répartition équitable,
- **dynamic**: récupération de nouveau chunk au fur et à mesure,
- **guided**: comme dynamic mais les chunks sont de plus en plus petits,
- **runtime**: permet de décider en dehors du programme
```bash
export OMP_SCHEDULE = "guided, 4"
```
- **auto**: choisit seul.

#### Collapse
```C
#pragma omp for collapse(positive value)
```
Permet de paralléliser des boucles imbriquées.

#### Nowait
Permet de poursuivre le parallélisme sur plusieurs boucles consécutives.

### Sections
Répartit plusieurs sections précédées par:
```C
#pragma omp sections [clause]
{
	#pragma omp section
}
```

### Single
Seul le premier thread a atteindre le bloc l'exécute:
```C
#pragma omp single [clause]
{
	//structured block
}
```
Possibilité d'utiliser copyprivate(list of variables) comme clause pour indiquer qu'une valeur privée du bloc sera donnée aux autres threads.

## Synchronisation
### Master
```C
#pragma omp master
```
Le thread master exécute le bloc et les autres le passe.

### Critical
``` C
#pragma omp critical[(name)]
```
Tous les threads exécutent le bloc l'un après l'autre.

### Atomic
``` C
#pragma omp atomic
```
Similaire à critical pour les opérations atomiques.

### Barrier
```C
#pragma omp barrier
```
Fait attendre les threads entre eux à la barrière

### Ordered
```C
#pragma omp ordered
```
Annule le comportement du for pour un bloc de code.

## Gestion des tâches (Task)
```C
#pragma omp task[clause]
```
Permet aux threads d'ajouter des nouvelles tâches qui seront effectuées par un thread disponible.
### Clauses
#### If
N'ajoute la tâche que si la condition est vérifiée.

#### Untied
Permet à d'autres processeurs d'effectuer les tâches.

**taskwait** crée une barrière explicite.

---

# MPI
## Header
```C
#include <mpi.h>
```

## Initialisation
```C
MPI_Init(int *argc, char **argv[])
```

## Fin
```C
MPI_Finalize()
```

## Contexte
``` C
MPI_Comm_size(MPI_Comm comm, int *np)
MPI_Comm_rank(MPI_Comm comm, int *myid)
```

## Mesure du temps d'exécution
``` C
double MPI_WTime()
```

## Compilation
```shell
mpicc -o nom nom.c
```

## Exécution
``` shell
mpirun -np 4 nom # np = nombre de processus
```

## Communication
```C
int MPI_Send(void *message, int taille, MPI_Datatype type, int destination, int tag, MPI_Comm comm); // type: MPI_<NOM_DU_TYPE>

int MPI_Recv(void *message, int taille, MPI_Datatype type, int src, int tag, MPI_Comm comm, MPI_Status *status); // status: struct qui contient MPI_SOURCE, MPI_TAG, MPI_ERROR, ... ; source: ou MPI_ANY_SOURCE ; tag: ou MPI_ANY_TAG

int MPI_Sendrecv(void *sendbuff, int sendcount, MPI_Datatype sendtype, int dest, int sendtag, void* recvbuff, int recvcount, MPI_Datatype recvtype, int source, int recvtag, MPIComm comm, MPI_Status *status);
```

## Communication collective
```C
int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm); // root = Qui envoie le message ?

int MPI_Scatter(void *sendbuf, int sendcount, MPI_Datatype sendtype, void* recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm); // Partage les données envoyées entre tous les processus, lui compris

int MPI_Gather(void *sendbuf, int sendcount, MPI_Datatype sedtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm); // Inverse de Scatter.

int MPI_AllGather(void *sendbuf, int sendcnt, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm); // Similaire à Gather mais tous les processus reçoivent toutes les données.

int MPI_Alltoall(void *sbuf, int sendcnt, MPI_Datatype sendtype, void *rbuf, int recvcnt, MPI_Datatype datatype, MPI_Comm comm); // "transposition" des données sur les processus

int MPI_Scatterv(const void *sendbuf, const int sendcounts[], const int displs[], MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm); // taille variable
```

## Calculs collectifs
```C
int MPI_Reduce(void* sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm); // MPI_Op = MPI_<NOM_OPERATION>

int MPI_Reduce_scatter(void *sbuf, void *rbuf, int *rcounts, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
```

## Synchronisation
``` C
int MPI_Barrier(MPI_Comm comm);
```