# Communication collectives avec MPI
>[!Define] Communication collective
>Tous les processus du communicateur participent.

Peut être créé avec une boucle et plusieurs MPI_Send et MPI_Recv.

## Caractéristiques
- Communication **bloquante**,
- **Tous les processus** du communicateur doivent exécuter la fonction,
- La **taille du buffer** et celle du message doivent être exactement égales.

## Fonction`
```C
int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);
// root = Qui envoie le message ?
```

### Si on veut diviser équitablement
```C
MPI_Scatter(void *sendbuf, int sendcount, MPI_Datatype sendtype, void* recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
// Partage les données envoyées entre tous les processus, lui compris
MPI_Gather(void *sendbuf, int sendcount, MPI_Datatype sedtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
// Inverse de Scatter.
MPI_AllGather(void *sendbuf, int sendcnt, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);
// Similaire à Gather mais tous les processus reçoivent toutes les données.
MPI_Alltoall(void *sbuf, int sendcnt, MPI_Datatype sendtype, void *rbuf, int recvcnt, MPI_Datatype datatype, MPI_Comm comm);
// "transposition" des données sur les processus
```

### Pour des tailles variables
```C
int MPI_Alltoallv(void *sendbuf, int* sendcounts, int* sdispls, MPI_Datatype sendtype, void *recvbuf, int* recvcounts, int *rdispls, MPI_Datatype recvtype, MPI_Comm comm);
```

### Calculs
```C
MPI_Reduce(void* sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm);
// MPI_AllReduce existe.
MPI_Reduce_scatter(void *sbuf, void *rbuf, int *rcounts, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
// Util pour les multiplications matrice-vecteur.
```

### Synchronisation
``` C
int MPI_Barrier(MPI_Comm comm);
MPI_Wait
MPI_Waitall
MPI_Waitany
MPI_Waitsome
```

>[!Warning] Attention
>Deadlock si les deux processus tentent d'écrire puis lire tout les deux.

⟹ Sendrecv
```C
int MPI_Sendrecv(void *sendbuff, int sendcount, MPI_Datatype sendtype, int dest, int sendtag, void* recvbuff, int recvcount, MPI_Datatype recvtype, int source, int recvtag, MPIComm comm, MPI_Status *status);
```

## Autres modes de communication
### Communication par buffer
```C
MPI_BSend // Similaire à MPI_Send
int MPI_Buffer_attach(void* buffer, int size); // malloc
int MPI_Buffer_detach(void* buffer, int size); // free
```

### Communication après accord
```C
MPI_Rsend // Similaire à MPI_Send
```

### Communication synchrone
```C
MPI_Ssend // Similaire à MPI_Send
```

### Communication non bloquante
```C
MPI_Isend // Similaire à MPI_Send avec une variable MPI_Request *request
MPI_Irecv
void MPI_Test(MPI_Request* request, int* flag, int* status);;
```

### Test de communication
Permet de connaître la **taille du message** sans l'ouvrir.
```C
int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status* status);
int MPI_Iprobe(int source, int tag, MPI_Comm comm, int* flag, MPI_Status *status);
```

## Création de communicateur
```C
int MPI_Comm_group(MPI_Comm comm, MPI_Group* group);
int MPI_Group_incl(MPI_Group group, int newsize, int *ids, MPI_Group* newgroup);
MPI_Comm_create(MPI_Comm comm, MPI_Group newGroup, MPI_Comm* newComm);

MPI_Comm_split(MPI_Comm comm, int color, int key, MPI_Comm* newComm);
```