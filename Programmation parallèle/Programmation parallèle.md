[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progPar)
# Architecture parallèle
= Faire plusieurs choses en même temps

## Types d'architectures parallèles
Flynn's Taxonomy:
- **SISD** (Single Instruction Single Date) → ligne à ligne, programmation classique.
- **SIMD** (Single Instruction Multiple Data) -> l'unité de contrôle globale fait exécuter une même instruction à plusieurs processeurs "très bêtes" avec des données différentes (eg. carte graphique).
- **MISD** (Multiple Instruction Single Data)
- **MIMD** (Multiple Instruction Multiple Data): à mémoire partagée (plusieurs core (PE + control unit), eg. micro-processeur), ou à mémoire distribuée (chaque processeur à sa propre mémoire).

Deux types de MIMD partagée: UMA(Uniform Memory Access) et NUMA(Non-uniform Memory Access) selon l’uniformité de la latence.
Exemple de UMA : SMP (Symetric MultiProcessing) = bande passante parallèle

Mémoire distribuée:
Par exemple, connecter beaucoup d’ordinateurs pas cher en ethernet.
MPP (Massively Parallel Processing) : très scalable.

# Parallel Programming
**Paradigm SPMP**
Pour la mémoire partagée : OpenMP → parallélise un code séquentielle, fourni avec linux
Pour la mémoire distribuée : passage de message, plus compliqué, par exemple MPI

## Étapes de parallélisation d'un code:
1. Décomposition du travail
2. Assignation du travail aux processeurs (dynamiquement ou statiquement)
3. Coordination (messages, wait, ...)
4. Map (Adaptation au système)

## Mesure de performance
Accélération
Efficacité
Répartition du travail
Scalabilité
Latence et longueur de bande
$T_m(n)$ : temps pour $m$ processeurs.

Importance de la communication
Temps du CPU = temps de calcul + temps de communication/synchronisation
→ Plus de processeurs = plus de communication (T_m(n) > T_1(n)  ÷ m)

>[!Define] Speedup
>$S = {T_1(n) \over {T_p(n)}}$

>[!Define] Efficacité
>$E = {S \over p}$ (en pourcentage).

>[!important] Loi d'Amdahl
>Permet d'obtenir théoriquement le Speedup pour une charge de travail fixe.
>Temps d'exécution = Temps parallélisable / p + Temps séquention
>$S_{latence}(s) = {1 \over {1 -  F_p + {F_p \over s}}}$
$s$ nombre de processeurs.
$F_p$ proportion de tâches parallélisables.

>[!important] Loi de Gustafson
>Garder le temps d'exécution mais faire plus de travail, plus de charge.
$S_{latence} = (1 - F_p) + s \times F_p = 1 + (s - 1) \times F_p$

>[!Define] Programmation parallèle
>Un programme est séparé en processus et threads qui coopèrent et se coordonnent.

# SMP
![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/SMP_-_Symmetric_Multiprocessor_System.svg/1920px-SMP_-_Symmetric_Multiprocessor_System.svg.png)

>[!Define] Threads
>- Similaires à processus mais partage une mémoire,
>- La mémoire partagée peut être accédée par tous les threads,
>- La mémoire privée ne peut être accéder que par le thread propriétaire,
>- Plusieurs threads peuvent suivre des flots de contrôle différents,
>- Souvent un thread par processeur.

# OpenMP
![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Fork_join.svg/1920px-Fork_join.svg.png)

## Principales directives:
Construction de régions parallèles:
- parallel

Partage de travail:
- for
- section
- single

Synchronisation:
- master
- critical
- atomic
- barrier

Gestion de tâche:
- task
- taskwait

## Régions parallèles
Le programme démarre son exécution sur un thread unique (=**master thread**).
À la première région parallèle rencontrée, le master thread crée une équipe de threads selon le fork/join model.
Chaque thread exécute les instructions de la région parallèle.
À la fin de la région parallèle, le master thread attends la fin des autres threads, puis poursuit son fonctionnement.
→ Barrière implicite à la fin de chaque région parallèle
Les variables définies dans toutes les branches de la région parallèle sont privées.
Préciser le nombre de threads avec:
```shell
#!/bin/bash
export OMP_NOM_THREADS = 8
./example01
```
```C
void omp_set_num_threads(num_threads);
```
```C
#pragma omp parallel num_threads(num_threads)
```

## Fonctions
### Informations
```C
int omp_get_num_threads();
int omp_get_thread_num();
int omp_get_max_threads();
int omp_get_num_procs();
int omp_in_parallel();
```

### Syntaxe:
```C
#pragma omp parallel [clause1] [clause2] ...
```

### Clauses:
- if (expression)
- num_threads(integer)
- private(list of var)
- shared(list of var)
- default(list of var)
- default(shared | none)
- firstprivate(list of var) ⟶ utilise l'initialisation précédente de la variable
- reduction(operator: list of var) exemple reduction +:b) ⟶ additionne les valeurs de b obtenues dans chaque threads en fin de région.
- copyin(list of var)

## Partage du travail
### For
```C
#pragma omp for [clauses]
	for loop
```
**Partage les iterations** entre les threads.
Barrière implicite à la fin de la boucle si pas de nowait.
#### Schedule
```C
#pragma omp for schedule(type[,block size])
```
- schedule(static\[, chunk\]): divise l'espace équitablement,
- schedule(dynamic\[, chunk\]): les threads récupèrent de nouveaux chunks quand ils finissent leur précédente,
- schedule(guided\[, chunk\]): pareil que dynamic mais les chunks sont de plus en plus petits,
- schedule(runtime): permet de décider en dehors du programme
```bash
export OMP_SCHEDULE = "guided, 4"
```
- schedule(auto): choisit seul.

#### Collapse
```C
#pragma omp for collapse(positive value)
```
Permet de paralléliser des boucles imbriquées:
```C
#pragam oomp for collapse(2)
for (int i = 0; i < 10; i++) {
	for (int j = 0; j < 10; j ++) {
		myfunction(i, j);
	}
}
// ⟺
#pragma omp for
for (int i = 0; i < 100; i ++) {
	myfunction(i / 10, i % 10);
}
```

#### Nowait
Permet de poursuivre le parallélisme sur plusieurs boucles consécutives.

### Sections
Répartit plusieurs sections précédées par:
```C
#pragma omp section
```

### Single
Seul le premier thread a atteindre le bloc l'exécute:
```C
#pragma omp single [clause]
{
	//structured block
}
```
Possibilité d'utiliser copyprivate(list of variables) comme clause pour indiquer qu'une valeur privée du bloc sera donnée aux autres threads.

## Synchronisation
Assure l'ordre des entrées/sorties et modifications des variables partagées.
### Master
```C
#pragma omp master
	//structured block
```
Le thread master exécute le bloc et les autres le passe.

### Critical
``` C
#pragma omp critical[(name)]
	//structured block
```
Tous les threads exécutent le bloc l'un après l'autre.
Permet de protéger certaines variables partagées.

### Atomic
``` C
#pragma omp atomic
	//code statement
```
Similaire à critical pour les opérations atomiques.

### Barrier
```C
#pragma omp barrier
```
Fait attendre les threads entre eux à la barrière

### Ordered
Annule le comportement du for pour un bloc de code.

## Gestion des tâches (Task)
```C
#pragma omp task[clause]
	//structured clock
```
Permet aux threads d'ajouter des nouvelles tâches qui seront effectuées par un thread disponible.
### Clauses
#### If
N'ajoute la tâche que si la condition est vérifiée.

#### Untied
Permet à d'autres processeurs d'effectuer les tâches.

#### firstprivate / private / shared / default
Similaires aux closes de construction

**taskwait** crée une barrière explicite.

### Exemple
```C
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long fib(int n) {
	if (n < 2) return n;
	return fib(n - 1) + fib(n - 2);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		fprintf(stderr, "Error: 1 argument expected but %d given\n", argc - 1);
		exit(-1);
	}
	int n = atoi(argv[1]);
	if (n <= 0 && strcmp(argv[1], "0") != 0) {
		fprintf(stderr, "Error: %s is not a positive ingeger\n", argv[1]);
		exit(-1)
	}
	printf("fig(%d): %ld\n", n, fib(n));
}
```

# MPI

>[!Define] MPI
>Librairie de fonctions de **communications par message** entre processus pour le modèle **SPMD**.
>MPI est utilisable en:
>- C,
>- C++,
>- Fortran,
>- Python...

>[!Define] Communicateur
>Ensemble de processus qui peuvent communiquer entre eux par MPI.

## Stratégies de communication
### Communication synchrone
>[!Define] Communication synchrone
>Émetteur et récepteur tout le temps disponible.
>1. Le transmetteur envoit une requête,
>2. Le récepteur confirme la réception,
>3. Le transmetteur transmet la donnée.

>[!Success] Avantages
>Plus rapide

>[!Failure] Inconvénient
>Toujours bloquante.

### Communication asynchrone
>[!Define] Communication asynchrone
>Le processus n'est pas bloqué pendant la communication.

### Communication par buffer
>[!Define] Communication par buffer
>Copier les données dans un buffer du système d'opération qui s'occupe lui-même de transmettre les données au buffer du récepteur.

## Synchronisation
>[!Define] Barrière
>Tout les processus s'attendent pour reprendre en même temps au même endroit.

>[!Define] Radiation
>Un seul processus envoie les messages à plusieurs récepteurs.

>[!Define] Réduction
>Un processus reçoit et utilise les données de plusieurs autres.

## Programmation avec MPI
Les processus sont triés et numérotés à partir de 0.
### Header
```C
#include <mpi.h>
```

### Forme des fonctions
``` C
int error;
error = MPI_Xxxx(parameters);
```

### Initialisation
```C
MPI_Init(int *argc, char **argv[])
```

### Fin
```C
MPI_Finalize()
```

### Contexte
``` C
MPI_Comm_size(MPI_Comm comm, int *np)
// Renvoie le nombre de processus du communicateur dans np.
//Exemple:
MPI_Comm_size(MPI_COMM_WORLD, &np)
```
```C
MPI_Comm_rank(MPI_Comm comm, int *myid)
// Renvoie l'id du processus de com dans myid.
```

### Compilation
```shell
mpicc -o nom nom.c
```

### Exécution
``` shell
mpirun -np 4 nom # np = nombre de processus
```

### Communication
```C
int MPI_Send(
	void *message,
	int taille,
	MPI_Datatype type, // MPI_NOM_DU_TYPE
	int destination,
	int tag,
	MPI_Comm comm
)
```
``` C
int MPI_Recv(
	void *message,
	int taille,
	MPI_Datatype type,
	int src, // ou MPI_ANY_SOURCE
	int tag, // ou MPI_ANY_TAG
	MPI_Comm comm,
	MPI_Status *status // struct qui contient MPI_SOURCE, MPI_TAG, MPI_ERROR, ...
)
```

La taille dans MPI_Recv doit être supèrieure ou égale à celle de MPI_Send.

>[!Example]
>```C
>#include <mpi.h>
>#include <stdio.h>
>#include <string.h>
>#define size 20
>int main(int argc, char *argv[]){
>	int myid, np, i;
>	int tag = 0;
>	char msg[size] = "";
>	MPI_Status stt;
> 
>	MPI_Init(&argc, &argv);
>	MPI_Comm_size(MPI_COMM_WORLD, &np);
>	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
>	if(myid == 0){
>		strcpy(msg,"Hello MPI");
>		for(i=1; i<np;i++)
>			MPI_Send(msg, size, MPI_CHAR, i, tag, MPI_COMM_WORLD);
>	}else {
>		MPI_Recv(msg, size, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &stt);
>	}
>
>	printf("I am node %d: %s\n", myid, msg);
>	MPI_Finalize();
> }
>```

Généralement, un seul processus communique avec l'utilisateur via l'écran et le clavier.

### Mesure du temps d'exécution
``` C
double MPI_WTime()
double MPI_Wtick() // pour la précision de la mesure.
```

# Communication collectives avec MPI
>[!Define] Communication collective
>Tous les processus du communicateur participent.

## Fonction`
```C
int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);
// root = Qui envoie le message ?
```

### Si on veut diviser équitablement
```C
MPI_Scatter(void *sendbuf, int sendcount, MPI_Datatype sendtype, void* recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
// Partage les données envoyées entre tous les processus, lui compris
MPI_Gather(void *sendbuf, int sendcount, MPI_Datatype sedtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
// Inverse de Scatter.
MPI_AllGather(void *sendbuf, int sendcnt, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);
// Similaire à Gather mais tous les processus reçoivent toutes les données.
MPI_Alltoall(void *sbuf, int sendcnt, MPI_Datatype sendtype, void *rbuf, int recvcnt, MPI_Datatype datatype, MPI_Comm comm);
// "transposition" des données sur les processus
```

### Pour des tailles variables
```C
int MPI_Alltoallv(void *sendbuf, int* sendcounts, int* sdispls, MPI_Datatype sendtype, void *recvbuf, int* recvcounts, int *rdispls, MPI_Datatype recvtype, MPI_Comm comm);
```

### Calculs
```C
MPI_Reduce(void* sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm);
// MPI_AllReduce existe.
MPI_Reduce_scatter(void *sbuf, void *rbuf, int *rcounts, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
// Util pour les multiplications matrice-vecteur.
```

### Synchronisation
``` C
int MPI_Barrier(MPI_Comm comm);
MPI_Wait
MPI_Waitall
MPI_Waitany
MPI_Waitsome
```

```C
int MPI_Sendrecv(void *sendbuff, int sendcount, MPI_Datatype sendtype, int dest, int sendtag, void* recvbuff, int recvcount, MPI_Datatype recvtype, int source, int recvtag, MPIComm comm, MPI_Status *status);
```

## Autres modes de communication
### Communication par buffer
```C
MPI_BSend // Similaire à MPI_Send
int MPI_Buffer_attach(void* buffer, int size); // malloc
int MPI_Buffer_detach(void* buffer, int size); // free
```

### Communication après accord
```C
MPI_Rsend // Similaire à MPI_Send
```

### Communication synchrone
```C
MPI_Ssend // Similaire à MPI_Send
```

### Communication non bloquante
```C
MPI_Isend // Similaire à MPI_Send avec une variable MPI_Request *request
MPI_Irecv
void MPI_Test(MPI_Request* request, int* flag, int* status);;
```

### Test de communication
Permet de connaître la **taille du message** sans l'ouvrir.
```C
int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status* status);
int MPI_Iprobe(int source, int tag, MPI_Comm comm, int* flag, MPI_Status *status);
```

## Création de communicateur
```C
int MPI_Comm_group(MPI_Comm comm, MPI_Group* group);
int MPI_Group_incl(MPI_Group group, int newsize, int *ids, MPI_Group* newgroup);
MPI_Comm_create(MPI_Comm comm, MPI_Group newGroup, MPI_Comm* newComm);

MPI_Comm_split(MPI_Comm comm, int color, int key, MPI_Comm* newComm);
```