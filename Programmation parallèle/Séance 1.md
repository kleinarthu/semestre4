outils: OpenMP, MPI
slides en anglais, examen en français

# Introduction
Monde complexe → science utilise théorie (explication ou hypothèse bien appuyée) pour le comprendre
Puis on fait des expériences pour constater le succès ou l'échec répété d'une théorie.
Ces validation ou invalidations nécessitent donc le traitement de beaucoup de données.
On peut faire des simulations pour les problèmes:
- à trop grande échelle
- à trop petite échelle
- trop complexes
- trop chers
- trop dangereux
- ...

Les simulations s'appuient sur des modèles (représentation assez précise mais simplifiée).
Deux méthodes de simulation:
- analytique: réduire le nombre de variables, simplifier, utiliser des équations pour arriver à un résultat précis
- computational: système plus grand et simulation plus longue, beaucoup d'issues en parallèle

>[!success] Positifs
>Beaucoup de comportements sont continus mais les ordinateurs ont besoin d'un modèle discret.
>Simulation peut être répétée, permet de connaître les valeurs de toutes les variables.

>[!failure] Négatifs
>Mais on perd la compréhension et la précision mathématique.
>On n'est pas sûr que le modèle fonctionne pour tous les cas.

→ Besoin d'ordinateurs pouvant faire ces simulations **→ Architecture parallèle**

# Architecture parallèle
==Faire plusieurs choses en même temps==

Pour améliorer les performances d'un programme:
- améliorer les processeurs (limité aujourd'hui par des effets quantiques, devient trop compliqué et trop cher).
- améliorer l'algorithme (limite dans l'amélioration).
- demander de l'aide au processeur.
>[!Define] HPC
>High Performance Computing = architecture, algorithme et programmation parallèle.

## Types d'architectures parallèles
Flynn's Taxonomy:
- **SISD** (Single Instruction Single Date) → ligne à ligne, programmation classique.
- **SIMD** (Single Instruction Multiple Data) -> l'unité de contrôle globale fait exécuter une même instruction à plusieurs processeurs "très bêtes" avec des données différentes (eg. carte graphique).
- **MISD** (Multiple Instruction Single Data)
- **MIMD** (Multiple Instruction Multiple Data): à mémoire partagée (plusieurs core (PE + control unit), eg. micro-processeur), ou à mémoire distribuée (chaque processeur à sa propre mémoire).

Pour visualiser les actions de chaque processeur:
```bash
htop
```

Deux types de MIMD partagée: UMA(Uniform Memory Access) et NUMA(Non-uniform Memory Access) selon l’uniformité de la latence.
Exemple de UMA : SMP (Symetric MultiProcessing) = bande passante parallèle

Mémoire distribuée:
Par exemple, connecter beaucoup d’ordinateurs pas cher en ethernet.
MPP (Massively Parallel Processing) : très scalable.

Aujourd’hui, systèmes hybrides.

# Parallel Programming
**Paradigm SPMP**
Pour la mémoire partagée : OpenMP → parallélise un code séquentielle, fourni avec linux
Pour la mémoire distribuée : passage de message, plus compliqué, par exemple MPI

## Étapes de parallélisation d'un code:
1. Décomposition du travail
2. Assignation du travail aux processeurs (dynamiquement ou statiquement)
3. Coordination (messages, wait, ...)
4. Map (Adaptation au système)

1.et 2. → Partitionnement

## Mesure de performance
Accélération
Efficacité
Répartition du travail
Scalabilité
Latence et longueur de bande
$T_m(n)$ : temps pour $m$ processeurs.
Unités:
- MIPS (millions d'instruction par seconde)
- FLOPS (opérations sur flottants par seconde)
- IPS (instruction par seconde)
- CPI (cycle par instruction)

Importance de la communication
$Temps du CPU = temps de calcul + temps de communication/synchronisatino$
→ Plus de processeurs = plus de communication → $T_m(n) > T_1(n) \over m$

**Speedup** = $S = {T_1(n) \over {T_p(n)}}$
**Efficacité** = $E = {S \over p}$ (en pourcentage).

## Comportement
Linéaire / sous-linéaire / super-linéaires (très rare)

## Répartition des charges
Statique (choisi avant l'exécution) ou dynamique (choisie pendant l'exécution).

>[!Define] Scalabilité
>Capacité du système à maintenir son efficacité quand on augmente le nombre de processeurs.

>[!Define] Latence
>Temps entre la réponse et l'envoi.

>[!Define] Bande passante
>Débit de la communication dans le réseau.

>[!important] Loi d'Amdahl
>Permet d'obtenir théoriquement le Speedup pour une charge de travail fixe.
>Temps d'exécution = Temps parallélisable / p + Temps séquention
>$S_{latence}(s) = {1 \over {1 -  F_p + {F_p \over s}}}$

Très pessimiste (partie séquentielle < 0.1%).
$s$ nombre de processeurs.
$F_p$ proportion de tâches parallélisables.

>[!important] Loi de Gustafson
>Garder le temps d'exécution mais faire plus de travail, plus de charge.
$S_{latence} = (1 - F_p) + s \times F_p = 1 + (s - 1) \times F_p$