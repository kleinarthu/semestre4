# Directives OpenMP
- Construction: parallel,
- Partage du travail: for, sections, single,
- Synchronisation: master, critical, atomic, barrier,
- Gestion des tâches: task, taskwait.

## Partage du travail
### For
```C
#pragma omp for [clauses]
	for loop
```
**Partage les iterations** entre les threads.
Barrière implicite à la fin de la boucle si pas de nowait.
#### Schedule
```C
#pragma omp for schedule(type[,block size])
```
- schedule(static\[, chunk\]): divise l'espace équitablement,
- schedule(dynamic\[, chunk\]): les threads récupèrent de nouveaux chunks quand ils finissent leur précédente,
- schedule(guided\[, chunk\]): pareil que dynamic mais les chunks sont de plus en plus petits,
- schedule(runtime): permet de décider en dehors du programme
```bash
export OMP_SCHEDULE = "guided, 4"
```
- schedule(auto): choisit seul.

#### Collapse
```C
#pragma omp for collapse(positive value)
```
Permet de paralléliser des boucles imbriquées:
```C
#pragam omp for collapse(2)
for (int i = 0; i < 10; i++) {
	for (int j = 0; j < 10; j ++) {
		myfunction(i, j);
	}
}
// ⟺
#pragma omp for
for (int i = 0; i < 100; i ++) {
	myfunction(i / 10, i % 10);
}
```

#### Nowait
Permet de poursuivre le parallélisme sur plusieurs boucles consécutives.

### Sections
Répartit plusieurs sections précédées par:
```C
#pragma omp section
```

### Single
Seul le premier thread a atteindre le bloc l'exécute:
```C
#pragma omp single [clause]
{
	//structured block
}
```
Possibilité d'utiliser copyprivate(list of variables) comme clause pour indiquer qu'une valeur privée du bloc sera donnée aux autres threads.

## Synchronisation
Assure l'ordre des entrées/sorties et modifications des variables partagées.
### Master
```C
#pragma omp master
	//structured block
```
Le thread master exécute le bloc et les autres le passe.

### Critical
``` C
#pragma omp critical[(name)]
	//structured block
```
Tous les threads exécutent le bloc l'un après l'autre.
Permet de protéger certaines variables partagées.

### Atomic
``` C
#pragma omp atomic
	//code statement
```
Similaire à critical pour les opérations atomiques.

### Barrier
```C
#prgma omp barrier
```
Fait attendre les threads entre eux à la barrière

### Ordered
Annule le comportement du for pour un bloc de code.

## Gestion des tâches (Task)
```C
#pragma omp task[clause]
	//structured clock
```
Permet aux threads d'ajouter des nouvelles tâches qui seront effectuées par un thread disponible.
### Clauses
#### If
N'ajoute la tâche que si la condition est vérifiée.

#### Untied
Permet à d'autres processeurs d'effectuer les tâches.

#### firstprivate / private / shared / default
![[Programmation parallèle/Séance 2#Clauses:]]

**taskwait** crée une barrière explicite.

### Exemple
```C
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long fib(int n) {
	if (n < 2) return n;
	return fib(n - 1) + fib(n - 2);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		fprintf(stderr, "Error: 1 argument expected but %d given\n", argc - 1);
		exit(-1);
	}
	int n = atoi(argv[1]);
	if (n <= 0 && strcmp(argv[1], "0") != 0) {
		fprintf(stderr, "Error: %s is not a positive ingeger\n", argv[1]);
		exit(-1)
	}
	printf("fig(%d): %ld\n", n, fib(n));
}
```