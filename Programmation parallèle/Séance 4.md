# MPI
>[!Define] Mémoire distribuée
> **Plusieurs ordinateurs** connectés par un réseau pour échanger du travail.
> Chaque ordinateur exécute un processus différent (contre un unique processus et plusieurs threads pour la mémoire partagée).

Modèle de programmation de passage de message.
Les variables sont privées car sont dans des ordinateurs différents ⟹ envoyer un message pour communiquer des valeurs.

>[!Success] Avantages
>- Très flexible,
>- Meilleurs performances.

>[!Failure] Inconvénients
>- Plus compliqué à synchroniser,
>- Plus compliqué de détecter les erreurs,
>- Pas possible de paralléliser petit à petit.

>[!Define] MPI
>Librairie de fonctions de **communications par message** entre processus pour le modèle **SPMD**.
>MPI est utilisable en:
>- C,
>- C++,
>- Fortran,
>- Python...

Tout doit être expliqué.

Plusieurs implémentation (on utilisera OpenMPI qui est la version gratuite).

MPI crée automatiquement des communicateurs (par défaut tout le monde dans MPI_COMM_WORLD).

>[!Define] Communicateur
>Ensemble de processus qui peuvent communiquer entre eux par MPI.

## Types de communication
>[!Define] Point à point
>Deux processus qui travaillent ensemble (émetteur- récepteur).

>[!Define] Colectives
>Plus de deux processus communiquent entre eux.
>Possible de le recréer à partir de Point à Point

## Stratégies de communication
### Communication synchrone
>[!Define] Communication synchrone
>Émetteur et récepteur tout le temps disponible.
>1. Le transmetteur envoit une requête,
>2. Le récepteur confirme la réception,
>3. Le transmetteur transmet la donnée.

⟶ Besoin de confirmation de réception du message.

Certaines opérations sont bloquantes: il faut attendre la fin de la communication pour poursuivre les autres tâches.
Similaire à un fax.

>[!Success] Avantages
>Plus rapide

>[!Failure] Inconvénient
>Toujours bloquante.

### Communication asynchrone
On envoie le message mais on ne sait pas si il est reçu.
>[!Define] Communication asynchrone
>Le processus n'est pas bloqué pendant la communication.

Similaire à une lettre.

### Communication par buffer
>[!Define] Communication par buffer
>Copier les données dans un buffer du système d'opération qui s'occupe lui-même de transmettre les données au buffer du récepteur.

## Synchronisation
>[!Define] Barrière
>Tout les processus s'attendent pour reprendre en même temps au même endroit.

>[!Define] Radiation
>Un seul processus envoie les messages à plusieurs récepteurs.

>[!Define] Réduction
>Un processus reçoit et utilise les données de plusieurs autres.

## Programmation avec MPI
Les processus sont triés et numérotés à partir de 0.
### Header
```C
#include <mpi.h>
```

### Forme des fonctions
``` C
int error;
error = MPI_Xxxx(parameters);
```

### Initialisation
```C
MPI_Init(int *argc, char **argv[])
```

### Fin
```C
MPI_Finalize()
```

### Contexte
``` C
MPI_Comm_size(MPI_Comm, comm, int *np)
// Renvoie le nombre de processus du communicateur dans np.
//Exemple:
MPI_Comm_size(MPI_COMM_WORLD, &np)
```
```C
MPI_Comm_rank(MPI_Comm comm, int *myid)
// Renvoie l'id du processus de com dans myid.
```

### Compilation
```shell
mpicc -o nom nom.c
```

### Exécution
``` shell
mpirun -np 4 nom # np = nombre de processus
```

### Communication
```C
int MPI_Send(
	void *message,
	int taille,
	MPI_Datatype type,
	int destination,
	int tag,
	MPI_Comm comm
)
```
``` C
int MPI_Recv(
	void *message,
	int taille,
	MPI_Datatype type,
	int src, // ou MPI_ANY_SOURCE
	int tag, // ou MPI_ANY_TAG
	MPI_Comm comm,
	MPI_Status *status // struct qui contient MPI_SOURCE, MPI_TAG, MPI_ERROR, ...
)
```

Types MPI:
- MPI_CHAR,
- MPI_UNSIGNED_LONG,
- MPI_SHORT,
a MPI_INT,
- MPI_DOUBLE,
- MPI_LONG,
- ...

La taille dans MPI_Recv doit être supèrieure ou égale à celle de MPI_Send.

>[!Example]
>```C
>#include <mpi.h>
>#include <stdio.h>
>#include <string.h>
>#define size 20
>int main(int argc, char *argv[]){
>	int myid, np, i;
>	int tag = 0;
>	char msg[size] = "";
>	MPI_Status stt;
> 
>	MPI_Init(&argc, &argv);
>	MPI_Comm_size(MPI_COMM_WORLD, &np);
>	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
>	if(myid == 0){
>		strcpy(msg,"Hello MPI");
>		for(i=1; i<np;i++)
>			MPI_Send(msg, size, MPI_CHAR, i, tag, MPI_COMM_WORLD);
>	} else {
>		MPI_Recv(msg, size, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &stt);
>	}
>
>	printf("I am node %d: %s\n", myid, msg);
>	MPI_Finalize();
> }
>```

Généralement, un seul processus communique avec l'utilisateur via l'écran et le clavier.