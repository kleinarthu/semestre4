>[!Define] Programmation parallèle
>Un programme est séparé en processus et threads qui coopèrent et se coordonnent.

# SMP
![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/SMP_-_Symmetric_Multiprocessor_System.svg/1920px-SMP_-_Symmetric_Multiprocessor_System.svg.png)
>[!Define] Directive
>Ligne dans le code source qui n'a de signification que pour le compilateur. Il commence par #.

>[!Define] Threads
>- Similaires à processus mais partage une mémoire,
>- La mémoire partagée peut être accédée par tous les threads,
>- La mémoire privée ne peut être accéder que par le thread propriétaire,
>- Plusieurs threads peuvent suivre des flots de contrôle différents,
>- Souvent un thread par processeur.

# OpenMP
OpenMP basé sur la notion de thread.
>[!Define] OpenMP
>Open MultiProcessing
>Ensemble de directives, librairies et variables disponibles sur Fortran, C et C++.
>[Documentation](https://www.openmp.org)

Basé sur le fork-join:
![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Fork_join.svg/1920px-Fork_join.svg.png)

## Principales directives:
Construction de régions parallèles:
- parallel

Partage de travail:
- for
- section
- single

Synchronisation:
- master
- critical
- atomic
- barrier

Gestion de tâche:
- task
- taskwait

## Régions parallèles
Le programme démarre son exécution sur un thread unique (=**master thread**).
À la première région parallèle rencontrée, le master thread crée une équipe de threads selon le fork/join model.
Chaque thread exécute les instructions de la région parallèle.
À la fin de la région parallèle, le master thread attends la fin des autres threads, puis poursuit son fonctionnement.
→ Barrière implicite à la fin de chaque région parallèle

Exemple:
```C
# include <omp.h>
# include <stdio.h>

int main() {
	int sharedvar = 1;
	printf("Hola! I am the master thread :-) \n");
	#pragma omp parallel
	{
		int id = omp_get_thread_num();
		printf("I am thread %d. sharedvar = %d\n, id, sharedvar");
	}
	printf("The master thread ends...\n");
	return 0;
}
```
Les variables définies dans toutes les branches de la région parallèle sont privées.
Préciser le nombre de threads avec:
```shell
#!/bin/bash
export OMP_NOM_THREADS = 8
./example01
```
```C
void omp_set_num_threads(num_threads);
```
```C
#pragma omp parallel num_threads(num_threads)
```

## Fonctions
### Informations
```C
int omp_get_num_threads();
int omp_get_thread_num();
int omp_get_max_threads();
int omp_get_num_procs();
int omp_in_parallel();
```

### Syntaxe:
```C
#pragma omp parallel [clause1] [clause2] ...
```

### Clauses:
- if (expression)
- num_threads(integer)
- private(list of var)
- shared(list of var)
- default(list of var)
- default(shared | none)
- firstprivate(list of var) ⟶ utilise l'initialisation précédente de la variable
- reduction(operator: list of var) exemple reduction +:b) ⟶ additionne les valeurs de b obtenues dans chaque threads en fin de région.
- copyin(list of var)