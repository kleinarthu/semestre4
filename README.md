# Semestre4

## Organisation du repository

Toutes les opérations sont directement effectuées sur le main.
Les changements dans le workspace ne sont pas enregistrés dans le repository.

## Outils de création des notes

[Obsidian](https://obsidian.md/) est conseillé.
Tous les fichiers sont en markdown et peuvent être lus par n'importe quel éditeur de texte avec un formatage plus ou moins complet.

## Récupérer ces notes

Pour la première copie:
```shell
git clone https://gitlab.etude.cy-tech.fr/kleinarthu/semestre4/
cd semestre4
```
Pour les suivantes
```shell
cd semestre4
git pull origin main
```

## Ajout de notes
```shell
cd semestre4
git add <fichier>
git commit
git push origin main
```

## Projets associés
- [Notes de cours](https://gitlab.etude.cy-tech.fr/kleinarthu/semestre4)
- [Tp de programmation système](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progSys)
- [Tp de programmation parallèle](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progPar)
- [Tp de programmation fonctionnelle](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progFonc)
- [Tp d'IA Algorithmes](https://gitlab.etude.cy-tech.fr/kleinarthu/td_ia)
- [Tp de C++](https://gitlab.etude.cy-tech.fr/kleinarthu/td_cpp)
