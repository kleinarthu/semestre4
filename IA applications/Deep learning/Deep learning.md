# Réseau de neurone
![](https://metalblog.ctif.com/wp-content/uploads/sites/3/2021/04/Reseau-de-neurones-pour-faire-du-Machine-Learning.jpg)
Descente de gradient pour entraîner le réseau de neurone.
$\eta$ est le taux d'apprentissage.
Si le taux d'apprentissage est trop faible, on reste bloqué dans un minimum local et on converge lentement.
Si on prend un taux d'apprentissage trop important, on explore plus mais on risque de rester de passer d'un côté à l'autre du minimum.

# Introduction à l'apprentissage par renforcement
>[!Define] Apprentissage par renforcement
>On considère une agent qui apprend à se comporter dans un environnement en faisant des essais et des erreurs.
> ⟶ Pénaliser les erreurs,
> ⟶ Valoriser les bons comportements.

## Déroulement
- L'agent prend **connaissance** de son environnement,
- Il prend une **décision**,
- Il modifie son environnement et reçoit un **retour**,
- Il souhaite **maximiser** la somme des retours,
- Il **apprend** en répétant ces étapes.

## Représentation mathématique.
- Ensemble d'états $\mathcal{S}$,
- Ensemble d'actions $\mathcal{A}$,
- Ensemble de signaux de renforcement,
- $I$ fonction de $\mathcal S$, associant l'élément donné à l'agent pour un état donné.
- Fonction de récompense $r$.

Il a besoin d'une stratégie $\pi : \mathcal S \rightarrow \mathcal A$ maximisant la récompense.

## Modèles d'optimalité
Détermine sur quelle durée on tente de maximiser le gain.
>[!Define] Modèle à horizon fini
>L'agent doit maximiser la moyenne de ses récompenses sur les $h$ prochaines étapes.
>$E(\sum_{t = 0}^{h - 1}{r_t})$

>[!Define] Modèle à horizon infini
>L'agent prend en compte toutes ses futures récompenses avec un facteur d'actualité $\gamma, 0 \le \gamma \lt 1$.
>$E(\sum_{t=0}^{+\infty}{\gamma^t \times r_t})$

>[!Define] Modèle à récompense moyenne
>Veut maximiser la moyenne de toutes ses récompenses.
>$lim_{h \rightarrow \infty} E({1 \over h} \times \sum_{t=0}^{h-1}{r_t})$

## Propriété de Markov
>[!Define] Propriété/Processus de Markov
>Un processus stochastique vérifie la **propriété de Markov** si la prédiction de l'avenir à partir du présent n'est pas rendue plus précise par la connaissance du passé. Un tel processus est appelé **processus de Markov**. Un processus de Markov peut être continu ou discret.

>[!Define] Chaîne de Markov
>Cas discret: Soit $X_t, t \in \mathbb{N}$ une suite de variables aléatoires prenant leurs valeurs dans l'ensemble finie ou dénombrable $S = \{s_1, s_2, ..., s_m, ...\}$, appelé espace d'états. Cette suite est appelée **chaîne de Markov** si nous avons la propriété:
>Pour tout ensemble de valeurs $x_0, ..., x_{t-1}, x, y$ prises dans $S$, $P(X_{t+1}=y|X_t = x, X_{t-1} = x_{t-1}, ..., X_{0} = x_{0}) = P(X_{t + 1} = y|X_t = x)$

⟹ Le passé n'a pas d'importance.

>[!Define] Matrice de transition
>$P_{i,j}$ est la propriété de passer de $s_i$ à $s_j$: $P_{i,j} = P(X_{n+1}=s_j|X_n = s_i)$. Ces valeurs peuvent être placées dans une matrice carrée appelée matrice de transition $T$.

## Valeur d'un état
>[!Define] Équation de Bellman
>$V(s) = R_s + \gamma \times \sum_{s'}(P_{s, s'}\times V(s'))$

## Processus de décision markovien
>[!Define] Processus de décision markovien
>Défini par:
>- un ensemble d'états $\mathcal S$,
>- un ensemble d'actions $\mathcal A$
>- Une fonction de transition $T: S \times \mathcal A \times \mathcal S \rightarrow [0, 1]$ où  $T(s, a, s')$ est la probabilité de passer de l'état $s$ à l'état $s'$ en utilisant l'action $a$,
>- Une fonction de récompense $R:S \times \mathcal A \rightarrow \mathbb R$,
>- Un facteur d'actualisation $\gamma \in [0, 1[$.

>[!Define] Stratégie
>$\pi(s, a) = P(A_t = a|S_t = s)$.

>[!Define] Valeur d'un état
>$V_{\pi}(s) = E_{\pi} (\sum_{t=0}^{\infty}(\gamma^t \times r_t))$

>[!Define] Valeur optimale d'un état.
>$V^{*} = \max_{\pi}V_{\pi}(s)$ solution de $V^{*} = \max_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V^{*}(s')))$

>[!Define] Stratégie optimale
>$\pi^{*}$ argument de la valuer optimale.

>[!Question] Comment trouver cette stratégie optimale ?
>On utilisera deux algorithmes **value iteration** et **policy iteration** quand on connait le modèle (la fonction de transition).

## Value Iteration
Méthode avec modèle.
1. Initialiser $V(s), s \in S$ à des valeurs quelconques.
2. Répéter jusqu'au critère d'arrêt, pour chaque état $s$:
	1. Pour chaque action $a$, $Q(s, a) = R(s, a) + \gamma \times \sum_{s'} (T(s, a, s')\times V(s'))$
	2. $V(s) = \max_a Q(s, a)$

**Converge** vers $V^{*}$.
**Critère d'arrêt**: La maximum des différences entre deux valeurs successives de $V$ est inférieur à $\epsilon$ pour un paramètre $\epsilon$ donné.
**Complexité**: Quadratique en $|S|$ et linéaire en $|\mathcal A|$ dans le pire des cas mais généralement linéaire pour les deux.

## Policy iteration
Méthode avec modèle.
1. Choisir une stratégie quelconque $\pi'$.
2. Répéter jusqu'à $\pi' = \pi$
	1. $\pi = \pi'$
	2. Calculer $V_\pi(s) = R(s, \pi(s)) + \gamma \times \sum_{s'}(T(s, \pi(s), s') \times V_\pi(s'))$.
	3. Mettre à jour la stratégie $\pi' = argmax_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V_\pi(s'))$.

## Model-based (Dyna)
Méthode sans modèle.
1. On construit le modèle (la fonction de transition et de récompense).
2. On applique un des algorithmes précédents.

**Dyna**:
On boucle avec un quadruplet $(s, a, s', r)$:
1. Mettre à jour les valuers du modèle $\widehat T$ et $\widehat R$.
2. Mettre à jour la stratégie en s en utilisant la dernière version du modèle: $Q(s, a) = \widehat R(s, a) + \gamma \times \sum_{s'} (\widehat T(s, a, s') \times \max_{a'}Q(s', a'))$.
3. Mettre à jour k autres valeurs de $Q(s_i, a_i), i = 1, ..., k$ pour des couples $(s_i, a_i)$ choisis au hasard.
4. Choisir une action $a'$ à exécuter en $s'$ et recommencer.

## Model-free (Q-learning)
Méthode sans modèle.
Estime l'optimal sans estimer le modèle.

**Q-learning**:
Évaluer la qualité de chaque action partant de chaque état à l'aide de la récompense obtenue:
1. Pour un état $s$ et une action $a$, $Q^{*}(s, a)$ est la moyenne de la récompense cumulée après $a$ à partir de $s$ puis un comportement optimal: $Q^{*}(s, a) = R(s, a) + \gamma \times \sigma_{s'}(T(s, a, s') \times \max_{a'}Q^{*}(s', a'))$.
2. Déduire la valeur optimale de chaque état et la stratégie optimale: $V^{*}(s) = \max_aQ^{*}(s, a)$ et $\pi ^{*}(s) = argmax_aQ^{*}(s, a)$.
**L'algorithme:**
1. L'agent observe son état $s$.
2. Il choisit et exécute une action $a$.
3. Il observe le nouvel état $s'$.
4. Il reçoit une récompense immédiate $r$.
5. Il met à jour la valeur de $Q$ pour le couple $(s, a)$.
**Choix de l'action**:
- Au hasard pour explorer,
- La meilleure récompense pour exploiter.
⟹ paramètre de compromis $\epsilon$.
**Mise à jour de $Q$:**
$Q(s, a) = (1 - \alpha) \times Q(s, a) + \alpha \times (r + \gamma \times \max_{a'}Q(s', a'))$
Où $\alpha \in [0, 1]$ facteur d'apprentissage.

# Convolution
## Librairies
- **Tenserflow**(vérifier la version de Keras) qui utilise Keras,
- **Pandas** pour l'affichage,
- **Numpy** pour le calcul matriciel.

## Convolution
>[!Define] Apprentissage par motifs:
>- Invariants par translation
>- Hiérarchie spatiale de motifs: séparer l'image en détails de plus en plus fins.

>[!Define] Convolution en dimension 2
> Opération qui associe à une matrice d'**entrée** A et une matrice appelée **motif** ou filtre (petite) M une nouvelle matrice obtenue en appliquant le produit des sous-matrice de A de même taille que M à la matrice M.

>[!Define] Padding
>**Remplir de 0** ce qui est en dehors de la matrice pour obtenir une sortie de même taille que l'entrée.

>[!Define] Stride
>Pas duquel on déplace la sous-matrice suivante.

### Simplification
>[!Define] Pooling
>Le **pooling** est une opération de sous-échantillonnage (= remplacer par les sorties de la convolution pour diminuer la taille).

>[!Define] Couche de Pooling
>Une couche de Pooling de taille $k$ transforme une entrée de taille $n \times p$ en matrice de taille $\lfloor n/k \rfloor \times \lfloor p/k \rfloor$.
>⟶On remplace les blocs de matrices par leurs valeurs.

### Déroulement
On applique plusieurs convolution sur l'image ⟶ On fait du pooling ⟶ On recommence.

>[!Success] Avantages
>- Moins de paramètres,
>- Convergence plus rapide,
>- Convergence vers un meilleur score.

### Manque de données
>[!Define] Data augmentation
>Partir des données existantes et en générer de nouvelles en effectuant des transformations:
>- Rotations,
>- Translations,
>- Zooms,
>- ...

### Utilisation
On peut créer des modèles avec une base de convolution pour les réutiliser.
>[!Example] Exemples de **modèles**:
>- VGG16 (fourni avec tenserflow et keras),
>- Resnet50,
>- ...

>[!Info] Informations
>[Hugging face](https://huggingface.co/)
>[Keras](https://keras.io/)
>[Tenserflow](https://www.tensorflow.org/)
>
>Pour l'aspect mathématique: [exo7](https://exo7math.github.io/deepmath-exo7/)
>[scikit](https://scikit-learn.org/stable/)

# Théorie des languages
>[!Define] Alphabet
>Un **alphabet** $\Sigma$ est un ensemble fini de symbôles.

>[!Define] Mot
>Un **mot** $\omega$ défini sur un alphabet $\Sigma$ est une suite de symboles $s_1, ..., s_n$ appartenant à l'alphabet $\Sigma$.

>[!Define] Langage
>Un **langage** $L$ défini sur un alphabet $\Sigma$ est un ensemble de mots définis sur $\Sigma$.

>[!Define] Machine de Turing
>Une **machine de Turing** est définie par un quadruplet ($K, \Sigma, \delta, q_0$) où:
>- $\Sigma$ est un **alphabet**,
>- $K$ est un ensemble fini d'**états**,
>- $q_0 \in K$ est l'état **initial**,
>- $\delta$ est la fonction de **transition** de $\Sigma \times K$ vers $\Sigma \times K \times \{\rightarrow, \leftarrow, -\}$.

# Natural Language Processing
>[!Define] NLP
>Branche de l'IA permettant à l'homme de converser avec la machine.
## Analyse lexicale
### Tokenization
>[!Define] Tokenisation
>Découpage du texte brut en tokens (mots, ponctuation, ...).

### Découpage morpho-syntaxique
>[!Define]
>On distingue les adjectifs des noms propres, verbes, ...

### Stemming
>[!Define]
>Tronquer le nom pour ne garder que la partie lui donnant son sens.

### Lemmatisation
>[!Define]
>On revient à la racine du nom.

### Analyse syntaxique
>[!Define]
>Recherche de signification du texte: on crée mathématiquement les règles et symboles constituant une grammaire.

⟹ On crée un arbre pour **décomposer** la phrase.

## Approche connexionniste
- On **vectorise** le texte: on le segmente en mots, caractères ou n-grammes,
- On transforme les unités en **token** (on peut utiliser *nltk* ou *tenserflow* en python),

### Embedding pré-entraînés
- *Glove*,
- *Fasttext*,
- *Word2Vec* (Gensim avec python)
- ...

### Réseaux récurrents
La sortie repasse dans l'entrée du réseau.

# Expressions régulières
>[!Define] Expression régulière
>Motif décrivant un ensemble de chaînes de caractères en utilisant des métacaractères ayant une signification particulière.

## Métacaractères
### Caractères:
| Symbole | Signification             |
| ------- | ------------------------- |
| .       | Tous sauf nouvelle ligne. |
| ^       | Début de ligne.           |
| $       | Fin de ligne.             |
| \\           | Séquence spéciale ou dé-spécialise un caractère spécial. |     |
| \\d     | Chiffre.                  |
| \\D     | Tous sauf chiffre.        |
| \\w     | Caractère de mot.         |
| \\W     | Pas caractère de mot.     |
| \\s     | Espace.                   |
| \\S     | Taus sauf espace.         |
| \\b     | Début ou fin de mot.      |
| \\A     | Début de mot.             |
| \\Z     | Fin de mot.               |
| \\B     | Milieu de mot.            |

### Ensemble de caractères
| Symbole      | Signification                                            |     |
| ------------ | -------------------------------------------------------- | --- |
| \[agf\]      | Un parmi ces caractères.                                 |     |
| \[a-f0-3\]   | Un entre ces caractères.                                 |     |
| \[\^\]       | Un sauf ces caractères.                                  |     |
| ()           | Ce groupe de caractère.                                  |     |
| expr1\|expr2 | expr1 OU expr2                                           |     |

### Répétition
| Symbole    | Signification                |
| ---------- | ---------------------------- |
| ?          | 0 ou 1 répétition.           |
| *          | Au moins 0 répétitions.      |
| +          | Au moins 1 répétition.       |
| {n}        | n répétitions.               |
| {min, max} | Entre min et max répétition. |
| {min, }    | Au moins min répétitions.    |
| {, max}    | Jusqu'à max répétitions.      |
