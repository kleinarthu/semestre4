# Décidabilité: Éléments de base
>[!Question] Question centrale
>Étant donné un problème P:
>1. Peut-on le résoudre sur ordinateur ?
>2. Si oui, avec quel coût ?

>[!Define] Complexité
>$\mathcal{O} (f(n))$ signifie que le nombre d'opérations a comme terme de plus haut degré $f(n)$.
>$\exists n_0 \in \mathbb{N} | \forall n \ge n_0, f(n) \ge S_n$

>[!Define] Proposition
>Symbole ayant une valeur de vé

>[!Define] Formule bien formulée (fbf)
>Formule constituée de propositions, des symboles logiques($\lnot, \lor, \land, \Rightarrow, \Leftrightarrow$), et d'autres fbf.

>[!Define] Interprétation
>Valeur d'une formule logique pour un certain ensemble de valeurs associé à l'ensemble des variables d'une fbf.

>[!Define] Satisfaisabilité
>Un fbf est satifaisable ssi une interprétation lui donne une valeur vrai.

# Théorie des languages
>[!Define] Alphabet
>Un **alphabet** $\Sigma$ est un ensemble fini de symbôles.

>[!Define] Mot
>Un **mot** $\omega$ défini sur un alphabet $\Sigma$ est une suite de symboles $s_1, ..., s_n$ appartenant à l'alphabet $\Sigma$.

>[!Define] Langage
>Un **langage** $L$ défini sur un alphabet $\Sigma$ est un ensemble de mots définis sur $\Sigma$.

>[!Define] Machine de Turing
>Une **machine de Turing** est définie par un quadruplet ($K, \Sigma, \delta, q_0$) où:
>- $\Sigma$ est un **alphabet**,
>- $K$ est un ensemble fini d'**états**,
>- $q_0 \in K$ est l'état **initial**,
>- $\delta$ est la fonction de **transition** de $\Sigma \times K$ vers $\Sigma \times K \times \{\rightarrow, \leftarrow, -\}$.

# Problèmes de l'IA
2 types de tâches:
1. Calculs long et difficiles,
2. Reconnaissance, jeux, discussion, ...

Le premier groupe est très calculatoire. ⟹ La machine est très forte.
Le second groupe demande plus un raisonnement et une compréhension. ⟹ Intelligence.

# Expressions régulières
>[!Info] Entraînement expressions régulières
>[w3schools](https://www.w3schools.com/python/python_regex.asp)

# [Autoencoders in Keras](https://blog.keras.io/building-autoencoders-in-keras.html)
>[!Define] t-SNE
> - On a un **jeu de données** en dimensions n très grande et on veut une dimension beaucoup plus grande.
> - On crée une matrice de distance qui représente la **probabilité que deux éléments soient proches**.
> - On crée un **nouvel espace** pour lequel si deux points sont proches dans l'espace de départ, ils sont proches dans l'espace d'arrivée. On utilise pour cela la **divergence de Kullback-Leibler**.

>[!Define] Autoencoders
>Compresse les donnnées en les encodant et en les décodant. ⟹On diminue puis ré-augmente fortement la dimension des couches du réseau de neurones.

On peut utiliser des auto-encodeurs pour faire du **clustering** car on a déjà une bonne représentation des classes au milieu du réseau de neurone où la dimension de la couche diminue.
On peut donc ajouter à l'**erreur de reconstruction** un **score de clustering**.

>[!Info] Modèles avec SpaCy
>[lien](https://medium.com/in-pursuit-of-artificial-intelligence/named-entity-recognition-using-spacy-ner-da6eebd3d08)

# Mesure de la consommation d'un programme
RAPL pour les processeurs Intel.
[AIPowerMeter](https://github.com/GreenAI-Uppa/AIPowerMeter)
[codecarbon](https://github.com/mlco2/codecarbon)
[carbonai](https://pypi.org/project/carbonai/)