Cours sur Arel
# Réseau de neurone
![](https://metalblog.ctif.com/wp-content/uploads/sites/3/2021/04/Reseau-de-neurones-pour-faire-du-Machine-Learning.jpg)
Descente de gradient pour entraîner le réseau de neurone.
$\eta$ est le taux d'apprentissage.
Si le taux d'apprentissage est trop faible, on reste bloqué dans un minimum local et on converge lentement.
Si on prend un taux d'apprentissage trop important, on explore plus mais on risque de rester de passer d'un côté à l'autre du minimum.

# Outils utilisés
Le language utilisé sera **python** mais C et C++ sont intéressants car proche de la machine ⟶ rapide.
On peut utilisé les GPU pour le deep learning mais seulement avec Nvidia.
On travaillera avec google collab ou en local avec Jupyter, VSCode ou Anaconda.

# Généralités sur l'IA
L'explosion de l'IA est notamment liée aux progrès des **cartes graphiques** qui permettent d'utiliser beaucoup plus de paramètres.
Notre avantage est le bagage mathématique.

# Introduction à l'apprentissage par renforcement
>[!Example]
>- Alpha go
>- Voitures autonomes
>- ...

Les jeux de données sont souvent mauvais (données mal mesurées, champs mal remplies, données manquantes, droits d'utilisation des données, ...).

>[!Define] Apprentissage par renforcement
>On considère une agent qui apprend à se comporter dans un environnement en faisant des essais et des erreurs.
> ⟶ Pénaliser les erreurs,
> ⟶ Valoriser les bons comportements.

## Déroulement
- L'agent prend **connaissance** de son environnement,
- Il prend une **décision**,
- Il modifie son environnement et reçoit un **retour**,
- Il souhaite **maximiser** la somme des retours,
- Il **apprend** en répétant ces étapes.

## Représentation mathématique.
- Ensemble d'états $\mathcal{S}$,
- Ensemble d'actions $\mathcal{A}$,
- Ensemble de signaux de renforcement,
- $I$ fonction de $\mathcal S$, associant l'élément donné à l'agent pour un état donné.
- Fonction de récompense $r$.

Il a besoin d'une stratégie $\pi : \mathcal S \rightarrow \mathcal A$ maximisant la récompense.

## Modèles d'optimalité
Détermine sur quelle durée on tente de maximiser le gain.
>[!Define] Modèle à horizon fini
>L'agent doit maximiser la moyenne de ses récompenses sur les $h$ prochaines étapes.
>$E(\sum_{t = 0}^{h - 1}{r_t})$

>[!Define] Modèle à horizon infini
>L'agent prend en compte toutes ses futures récompenses avec un facteur d'actualité $\gamma, 0 \le \gamma \lt 1$.
>$E(\sum_{t=0}^{+\infty}{\gamma^t \times r_t})$

>[!Define] Modèle à récompense moyenne
>Veut maximiser la moyenne de toutes ses récompenses.
>$lim_{h \rightarrow \infty} E({1 \over h} \times \sum_{t=0}^{h-1}{r_t})$

## Qu'est-ce que l'on cherche
On veut déterminer le meilleur $\pi: \mathcal{S} \rightarrow \mathcal{A}$
$V^\pi(s)$ cumul des récompenses à partir de l'état $s$ pour une stratégie $\pi$.

## Propriété de Markov
>[!Define] Processus stochastique
>Un processus stochastique est une famille de variables aléatoires $X_t, t \in T$ indexée par le temps.

>[!Define] Propriété/Processus de Markov
>Un processus stochastique vérifie la **propriété de Markov** si la prédiction de l'avenir à partir du présent n'est pas rendue plus précise par la connaissance du passé. Un tel processus est appelé **processus de Markov**. Un processus de Markov peut être continu ou discret.

>[!Define] Chaîne de Markov
>Cas discret: Soit $X_t, t \in \mathbb{N}$ une suite de variables aléatoires prenant leurs valeurs dans l'ensemble finie ou dénombrable $S = \{s_1, s_2, ..., s_m, ...\}$, appelé espace d'états. Cette suite est appelée **chaîne de Markov** si nous avons la propriété:
>Pour tout ensemble de valeurs $x_0, ..., x_{t-1}, x, y$ prises dans $S$, $P(X_{t+1}=y|X_t = x, X_{t-1} = x_{t-1}, ..., X_{0} = x_{0}) = P(X_{t + 1} = y|X_t = x)$

⟶ Le passé n'a pas d'importance.

>[!Define] Matrice de transition
>$P_{i,j}$ est la propriété de passer de $s_i$ à $s_j$: $P_{i,j} = P(X_{n+1}=s_j|X_n = s_i)$. Ces valeurs peuvent être placées dans une matrice carrée appelée matrice de transition $T$.

>[!Important] Somme sur ligne de matrice de transition
>La somme des éléments de chaque ligne est égale à 1.

>[!Imoportant] Utilisation des $P_{i, j}$
>$P(X_n = x_n, X_{n-1} = x_{n-1}, ..., X_0 = x_0) = P_{x_n, x_{n-1}}...P_{x_1, x_0} \times P(X_0 = x_0)$

## Valeur d'un état
La suite des états possibles est un chaîne de Markov.
On associe une récompense à chaque état. C'est la moyenne des récompenses à l'état t+1 quand on vient de l'état s: $R_s = E(R_{t+1}|S_t = s)$ 

On utilisera un modèle à horizon infini et actualisé:
$v(s) = E(R_{t+1} + \gamma \times R_{t+2} + \gamma^2 \times R_{t+3}+...|S_s=s)$
$V(s) = E(R_{t+1}|S_t=s)+\gamma \times E(R_{t+2}+ \gamma \times R_{t+3}+ ...|S_t = s)$
>[!Define] Équation de Bellman
>$V(s) = R_s + \gamma \times \sum_{s'}(P_{s, s'}\times V(s'))$

On veut résoudre cette équation et y ajouter des actions.

## Processus de décision markovien
>[!Define] Processus de décision markovien
>Défini par:
>- un ensemble d'états $\mathcal S$,
>- un ensemble d'actions $\mathcal A$
>- Une fonction de transition $T: S \times \mathcal A \times \mathcal S \rightarrow [0, 1]$ où  $T(s, a, s')$ est la probabilité de passer de l'état $s$ à l'état $s'$ en utilisant l'action $a$,
>- Une fonction de récompense $R:S \times \mathcal A \rightarrow \mathbb R$,
>- Un facteur d'actualisation $\gamma \in [0, 1[$.

>[!Define] Stratégie
>$\pi(s, a) = P(A_t = a|S_t = s)$.

>[!Define] Valeur d'un état
>$V_{\pi}(s) = E_{\pi} (\sum_{t=0}^{\infty}(\gamma^t \times r_t))$

>[!Define] Valeur optimale d'un état.
>$V^{*} = \max_{\pi}V_{\pi}(s)$ solution de $V^{*} = \max_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V^{*}(s')))$

>[!Define] Stratégie optimale
>$\pi^{*}$ argument de la valuer optimale.

>[!Question] Comment trouver cette stratégie optimale ?
>On utilisera deux algorithmes **value iteration** et **policy iteration** quand on connait le modèle (la fonction de transition).

## Value Iteration
Méthode avec modèle.
1. Initialiser $V(s), s \in S$ à des valeurs quelconques.
2. Répéter jusqu'au critère d'arrêt, pour chaque état $s$:
	1. Pour chaque action $a$, $Q(s, a) = R(s, a) + \gamma \times \sum_{s'} (T(s, a, s')\times V(s'))$
	2. $V(s) = \max_a Q(s, a)$

**Converge** vers $V^{*}$.
**Critère d'arrêt**: La maximum des différences entre deux valeurs successives de $V$ est inférieur à $\epsilon$ pour un paramètre $\epsilon$ donné.
**Complexité**: Quadratique en $|S|$ et linéaire en $|\mathcal A|$ dans le pire des cas mais généralement linéaire pour les deux.

>[!Failure] Inconvénient
>Il faut ensuite retrouver le $\pi$ correspondant.

## Policy iteration
Méthode avec modèle.
1. Choisir une stratégie quelconque $\pi'$.
2. Répéter jusqu'à $\pi' = \pi$
	1. $\pi = \pi'$
	2. Calculer $V_\pi(s) = R(s, \pi(s)) + \gamma \times \sum_{s'}(T(s, \pi(s), s') \times V_\pi(s'))$.
	3. Mettre à jour la stratégie $\pi' = argmax_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V_\pi(s'))$.

## Model-based (Dyna)
Méthode sans modèle.
1. On construit le modèle (la fonction de transition et de récompense).
2. On applique un des algorithmes précédents.

**Dyna**:
On boucle avec un quadruplet $(s, a, s', r)$:
1. Mettre à jour les valuers du modèle $\widehat T$ et $\widehat R$.
2. Mettre à jour la stratégie en s en utilisant la dernière version du modèle: $Q(s, a) = \widehat R(s, a) + \gamma \times \sum_{s'} (\widehat T(s, a, s') \times \max_{a'}Q(s', a'))$.
3. Mettre à jour k autres valeurs de $Q(s_i, a_i), i = 1, ..., k$ pour des couples $(s_i, a_i)$ choisis au hasard.
4. Choisir une action $a'$ à exécuter en $s'$ et recommencer.

## Model-free (Q-learning)
Méthode sans modèle.
Estime l'optimal sans estimer le modèle.

**Q-learning**:
Évaluer la qualité de chaque action partant de chaque état à l'aide de la récompense obtenue:
1. Pour un état $s$ et une action $a$, $Q^{*}(s, a)$ est la moyenne de la récompense cumulée après $a$ à partir de $s$ puis un comportement optimal: $Q^{*}(s, a) = R(s, a) + \gamma \times \sigma_{s'}(T(s, a, s') \times \max_{a'}Q^{*}(s', a'))$.
2. Déduire la valeur optimale de chaque état et la stratégie optimale: $V^{*}(s) = \max_aQ^{*}(s, a)$ et $\pi ^{*}(s) = argmax_aQ^{*}(s, a)$.
**L'algorithme:**
1. L'agent observe son état $s$.
2. Il choisit et exécute une action $a$.
3. Il observe le nouvel état $s'$.
4. Il reçoit une récompense immédiate $r$.
5. Il met à jour la valeur de $Q$ pour le couple $(s, a)$.
**Choix de l'action**:
- Au hasard pour explorer,
- La meilleure récompense pour exploiter.
⟹ paramètre de compromis $\epsilon$.
**Mise à jour de $Q$:**
$Q(s, a) = Q(s, a) + \alpha \times (r + \gamma \times \max_{a'} Q(s', a') - Q(s, a))$
$Q(s, a) = (1 - \alpha) \times Q(s, a) + \alpha \times (r + \gamma \times \max_{a'}Q(s', a'))$
Où $\alpha \in [0, 1]$ facteur d'apprentissage.