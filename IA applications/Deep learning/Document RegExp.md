# Expressions régulières
>[!Define] Expression régulière
>Motif décrivant un ensemble de chaînes de caractères en utilisant des métacaractères ayant une signification particulière.

# Métacaractères
## Caractères:
| Symbole | Signification             |
| ------- | ------------------------- |
| .       | Tous sauf nouvelle ligne. |
| ^       | Début de ligne.           |
| $       | Fin de ligne.             |
| \\           | Séquence spéciale ou dé-spécialise un caractère spécial. |     |
| \\d     | Chiffre.                  |
| \\D     | Tous sauf chiffre.        |
| \\w     | Caractère de mot.         |
| \\W     | Pas caractère de mot.     |
| \\s     | Espace.                   |
| \\S     | Taus sauf espace.         |
| \\b     | Début ou fin de mot.      |
| \\A     | Début de mot.             |
| \\Z     | Fin de mot.               |
| \\B     | Milieu de mot.            |

## Ensemble de caractères
| Symbole      | Signification                                            |     |
| ------------ | -------------------------------------------------------- | --- |
| \[agf\]      | Un parmi ces caractères.                                 |     |
| \[a-f0-3\]   | Un entre ces caractères.                                 |     |
| \[\^\]       | Un sauf ces caractères.                                  |     |
| ()           | Ce groupe de caractère.                                  |     |
| expr1\|expr2 | expr1 OU expr2                                           |     |

## Répétition
| Symbole    | Signification                |
| ---------- | ---------------------------- |
| ?          | 0 ou 1 répétition.           |
| *          | Au moins 0 répétitions.      |
| +          | Au moins 1 répétition.       |
| {n}        | n répétitions.               |
| {min, max} | Entre min et max répétition. |
| {min, }    | Au moins min répétitions.    |
| {, max}    | Jusqu'à max répétitions.      |

>[!Example] Chaîne hexadécimale
>[0-9a-fA-F]*

# En python
Module **re**.
## Fonctions
- **findall**,
- **finditer**,
- **search**,
- **split**,
- **sub** (substitute).