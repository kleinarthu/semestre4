>[!Info] Cours de théorie des languages
>Christine Solmon, *Théorie des languages*

>[!Info]
>[scikit](https://scikit-learn.org/stable/)

# Natural Language Processing
>[!Define] NLP
>Branche de l'IA permettant à l'homme de converser avec la machine.

>[!Define] Test de Turing
>Il ne doit pas pouvoir être possible de différencier un humain d'un ordinateur lors d'une conversation.

## Applications
- Traduction,
- Réponse aux questions,
- Résumé de texte,
- Sous-titrage,
- ...

## Domaines utilisés
- Théorie des automates,
- Machine learning,
- Statistiques,
- Logique,
- Linguistique,
- ...

## Analyse lexicale
### Tokenization
>[!Define] Tokenisation
>Découpage du texte brut en tokens (mots, ponctuation, ...).

### Découpage morpho-syntaxique
>[!Define]
>On distingue les adjectifs des noms propres, verbes, ...

### Stemming
>[!Define]
>Tronquer le nom pour ne garder que la partie lui donnant son sens.

### Lemmatisation
>[!Define]
>On revient à la racine du nom.

### Analyse syntaxique
>[!Define]
>Recherche de signification du texte: on crée mathématiquement les règles et symboles constituant une grammaire.

⟹ On crée un arbre pour **décomposer** la phrase.

>[!Warning] Difficultés
>Beaucoup d'expressions sont ambiguës est la résolution de cette ambiguïté est une des difficultés majeures du NLP.

## Exemples d'outils
>[!Example] Bert
>Outils NLP qui a permis le développement de google translate en utilisant les transformers.

>[!Define] Transformers
>Modèle qui réduit grandement le nombre de paramètres puis le ré-augmente.
>Ils sont formés de couches d'attention qui donnent des scores d'importance aux mots.

>[!Define] Wordnet
>Base de données lexicales sur l'anglais.

## Approche connexionniste
- On **vectorise** le texte: on le segmente en mots, caractères ou n-grammes,
- On transforme les unités en **token** (on peut utiliser *nltk* ou *tenserflow* en python),

>[!Define] Équilibre des classes
>Faits que les classes soient également représentées, ce qui évite qu'une classe sous-représentée ne soit jamais représentée par le système.

### Embedding pré-entraînés
- *Glove*,
- *Fasttext*,
- *Word2Vec* (Gensim avec python)
- ...

### Réseaux récurrents
La sortie repasse dans l'entrée du réseau.