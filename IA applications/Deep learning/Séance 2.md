# Introduction au traitement d'images et à la Convolution
Deep learning permet d'aller plus loin que shadow learning dans l'exploitation.
## Rappels sur les réseaux Fully Connected
[Vidéos de 3b1b](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)

## Librairies
- **Tenserflow**(vérifier la version de Keras) qui utilise Keras,
- **Pandas** pour l'affichage,
- **Numpy** pour le calcul matriciel.

Souvent beaucoup de warnings.
Possibilité de faire les calculs en GPU.

## Données
>[!Warning] Attention
>Les données doivent être bien labellisé (souvent une grosse partie du travail).

>[!Define] Tenseur
>Matrice avec une dimension supplémentaire.

Les images sont des tenseurs: (r; g; b).
On peut transformer les matrices en vecteurs en mettant bout à bout les colonnes ou lignes.
Activation choisi par la pratique mais souvent:
- sigmoïde,
- relu,
- softmax pour la sortie (somme = 1).

>[!Define] Dropout
>Pourcentage du réseau que l'on active en même temps.

>[!Define] Taille de lot
>Nombre de données qui passeront en même temps le test.
>Plus grand ⟹ plus de mémoire.

>[!Define] Époque
>Nombre de rétro-propagations à faire.

>[!Failure] Inconvénients
>- En utilisant directement les images, on a un taux de réussite médiocre,
>- On a un grand nombre de paramètres,
>- Certaines zones ne comptant pas rentre en compte.

⟹ **Covnets**

## Convolution
>[!Define] Apprentissage par motifs:
>- Invariants par translation
>- Hiérarchie spatiale de motifs: séparer l'image en détails de plus en plus fins.

>[!Define] Convolution en dimension 2
> Opération qui associe à une matrice d'**entrée** A et une matrice appelée **motif** ou filtre (petite) M une nouvelle matrice obtenue en appliquant le produit des sous-matrice de A de même taille que M à la matrice M.

La dimension de la sortie est réduite de la taille de M - 1 ⟶ Padding.

>[!Define] Padding
>**Remplir de 0** ce qui est en dehors de la matrice pour obtenir une sortie de même taille que l'entrée.

>[!Define] Stride
>Pas duquel on déplace la sous-matrice suivante.

### Simplification
>[!Define] Pooling
>Le **pooling** est une opération de sous-échantillonnage (= remplacer par les sorties de la convolution pour diminuer la taille).

>[!Define] Couche de Pooling
>Une couche de Pooling de taille $k$ transforme une entrée de taille $n \times p$ en matrice de taille $\lfloor n/k \rfloor \times \lfloor p/k \rfloor$.
>⟶On remplace les blocs de matrices par leurs valeurs.

>[!Question] Comment faire si les images sont en couleurs ?
>On utilise plusieurs filtres et on somme les applications du filtre sur les différentes couches du tenser.

### Déroulement
On applique plusieurs convolution sur l'image ⟶ On fait du pooling ⟶ On recommence.

>[!Success] Avantages
>- Moins de paramètres,
>- Convergence plus rapide,
>- Convergence vers un meilleur score.

### Manque de données
>[!Define] Data augmentation
>Partir des données existantes et en générer de nouvelles en effectuant des transformations:
>- Rotations,
>- Translations,
>- Zooms,
>- ...

### Utilisation
On peut créer des modèles avec une base de convolution pour les réutiliser.
>[!Example] Exemples de **modèles**:
>- VGG16 (fourni avec tenserflow et keras),
>- Resnet50,
>- ...

>[!Info] Informations
>[Hugging face](https://huggingface.co/)
>[Keras](https://keras.io/)
>[Tenserflow](https://www.tensorflow.org/)
>
>Pour l'aspect mathématique: [exo7](https://exo7math.github.io/deepmath-exo7/)

Pytorch est trop compliqué

## Transfert de style.
>[!Define] Transfert de style.
Opérations de convolutions en sens inverse

⟶ Permet d'avoir le style de détails d'une image sur une autre image.

## Segmentation d'image
Simplifier une image en la découpant en zones.
⟵ Réseau de neurone avec une couche intermédiaire très petite.

>[!Info]
>[Pense-bête](https://stanford.edu/~shervine/l/fr/teaching/cs-230/pense-bete-reseaux-neurones-convolutionnels)