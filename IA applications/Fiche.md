# Méthode du recuit simulé
```
Fonction critereMetropolis(Δf, T):
	Si Δf <= 0 alors
		retourner VRAI
	sinon
		retourner aléa(0, 1) < exp(-Δf / T)
```

```
X, un candidat, fX = f(X) énergie du système, T Température initiale
Xmin <- X
fmin <- f(X)
Tant que T > Tmin et non critèreConvergence()
	Tant que non équilibreThermodynamique()
		Xvois <- perturbation(X)
		Δf = f(Xvois) - fX
		Si critèreMetropolis(Δf,T) alors
			X <- Xvois
			fX <- f(Xvois)
			Si Δf < 0 et f(Xvois) < fmin alors
				fmin <- f(Xvois)
				Xmin <- Xvois
			Fin si
		Fin si
	Fin tant que
T <- refroidissement(T)
Fin tant que
```

# LAHC (Late Acceptance Hill Climbing)
```
X un candidat, f(X) son évaluation
fmin <- f(X), Xmin <- X, tabMemoire <- [fmin, ..., fmin]
n <- 1
Tant que non critereConvergence()
	Xvois <- genereVoisin(X)
	fmem <- tabMemoire[n % tailleMem]
	Si fmem > = f(Xvois) alors
		X <- Xvois
	Fin si
	tabMemoire[n % tailleMem] <- f(X)
	Si fmin > f(Xvois) alors
		Xmin <- Xvois
		fmin <- f(Xvois)
	Fin si 
	n ++
Fin tant que
```

# Optimisations par Essaims Particulaires
Chaque particule à un groupe, met à jour sa vitesse, puis sa position en suivant la formule:
$V_{k + 1} = \psi \times V_k + c_{max} \times rand(0, 1) \times (X_{pbest} - X_k) + c_{max} \times rand(0, 1) \times (X_{vbest} - X_k)$
où $\psi$ est l'inertie, $c_{max}$ est la confiance maximale, $X_{pbest}$ est la meilleure position rencontrée par la particule, et $X_{vbest}$ est la meilleure position rencontrée par son groupe.

---

# Apprentissage par renforcement
## Modèles d'optimalité
>[!Define] Modèle à horizon fini
>$E(\sum_{t = 0}^{h - 1}{r_t})$

>[!Define] Modèle à horizon infini
>$E(\sum_{t=0}^{+\infty}{\gamma^t \times r_t})$

>[!Define] Modèle à récompense moyenne
>$lim_{h \rightarrow \infty} E({1 \over h} \times \sum_{t=0}^{h-1}{r_t})$

## Valeur d'un état
>[!Define] Équation de Bellman
>$V(s) = R_s + \gamma \times \sum_{s'}(P_{s, s'}\times V(s'))$

## Processus de décision markovien
>[!Define] Processus de décision markovien
>Défini par:
>- un ensemble d'états $\mathcal S$,
>- un ensemble d'actions $\mathcal A$
>- Une fonction de transition $T: S \times \mathcal A \times \mathcal S \rightarrow [0, 1]$ où  $T(s, a, s')$ est la probabilité de passer de l'état $s$ à l'état $s'$ en utilisant l'action $a$,
>- Une fonction de récompense $R:S \times \mathcal A \rightarrow \mathbb R$,

>[!Define] Matrice de transition
>Matrice des $P_{i,j} = P(X_{n+1}=s_j|X_n = s_i)$.- Un facteur d'actualisation $\gamma \in [0, 1[$.

>[!Define] Stratégie
>$\pi(s, a) = P(A_t = a|S_t = s)$.

>[!Define] Valeur d'un état
>$V_{\pi}(s) = E_{\pi} (\sum_{t=0}^{\infty}(\gamma^t \times r_t))$

>[!Define] Valeur optimale d'un état.
>$V^{*} = \max_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V^{*}(s')))$

## Value Iteration
Méthode avec modèle.
1. Initialiser $V(s), s \in S$ à des valeurs quelconques.
2. Répéter jusqu'au critère d'arrêt, pour chaque état $s$:
	1. Pour chaque action $a$, $Q(s, a) = R(s, a) + \gamma \times \sum_{s'} (T(s, a, s')\times V(s'))$
	2. $V(s) = \max_a Q(s, a)$

## Policy iteration
Méthode avec modèle.
1. Choisir une stratégie quelconque $\pi'$.
2. Répéter jusqu'à $\pi' = \pi$
	1. $\pi = \pi'$
	2. Calculer $V_\pi(s) = R(s, \pi(s)) + \gamma \times \sum_{s'}(T(s, \pi(s), s') \times V_\pi(s'))$.
	3. Mettre à jour la stratégie $\pi' = argmax_a(R(s, a) + \gamma \times \sum_{s'}(T(s, a, s') \times V_\pi(s'))$.

## Dyna
On boucle avec un quadruplet $(s, a, s', r)$:
1. Mettre à jour les valuers du modèle $\widehat T$ et $\widehat R$.
2. Mettre à jour la stratégie en s en utilisant la dernière version du modèle: $Q(s, a) = \widehat R(s, a) + \gamma \times \sum_{s'} (\widehat T(s, a, s') \times \max_{a'}Q(s', a'))$.
3. Mettre à jour k autres valeurs de $Q(s_i, a_i), i = 1, ..., k$ pour des couples $(s_i, a_i)$ choisis au hasard.
4. Choisir une action $a'$ à exécuter en $s'$ et recommencer.

## Q-learning
```
entrée : taux d'apprentissage α > 0 et taux γ > 0
initialiser Q[s,a] pour tout état s non final, toute action a de façon arbitraire, et Q(état terminal, a) = 0
répéter
	s := état initial
	 répéter
               choisir une action a depuis s
			   exécuter l'action a
               observer la récompense r et le nouvel état s'
               Q[s, a] := Q[s, a] + α × (r + γ max_a' Q[s', a'] - Q[s, a])
               s := s'
      jusqu'à ce que s soit l'état terminal
```
Choix de l'action = compromis entre récompense prévue et exploration aléatoire (paramètre $\epsilon$).

---

# Convolution
>[!Define] Convolution en dimension 2
> Opération qui associe à une matrice d'**entrée** A et une matrice appelée **motif** ou filtre (petite) M une nouvelle matrice obtenue en appliquant le produit d'Hadamard des sous-matrice de A de même taille que M à la matrice M.

>[!Define] Padding
>**Remplir de 0** ce qui est en dehors de la matrice pour obtenir une sortie de même taille que l'entrée.

>[!Define] Stride
>Pas duquel on déplace la sous-matrice suivante.

>[!Define] Pooling
>Le **pooling** est une opération de sous-échantillonnage; stride = taille_filtre, généralement avec un filtre max ou moyenne.

## Nombre de paramètres
|                      | Convolution                          | Pooling | FCN                           |
| -------------------- | ------------------------------------ | ------- | ----------------------------- |
| Entrée               | I×I×C                                | I×I×C   | N_in                          |
| Sortie               | O×O×K                                | O×O×C   | N_out                         |
| Nombre de paramètres | $(F \times F \times C + 1) \times K$ | 0       | $(N_{in} + 1) \times N_{out}$ |

---

# Natural Language Processing
## Analyse lexicale
1. Tokenisation: Découpage du texte brut en tokens (mots, ponctuation, ...),
2. Lemmatisation: remplacer les mots par leurs formes canoniques,
3. Séparation en POS: adjectif, verbes, noms, ...,

### Analyse syntaxique
>[!Define] Analyse syntaxique
>Recherche de signification du texte: on crée mathématiquement les règles et symboles constituant une grammaire.
>⟹ On crée un arbre pour **décomposer** la phrase.

>[!Define] Grammaire sans contexte
>Un seul symbole non terminal à gauche.

### Ambiguïtés
- **Lexicale**: Mot à plusieurs sens,
- **Syntaxique**: Plusieurs arbres possibles,
- **Référentielle**: Pronoms peut se rapporter à plusieurs personnes.

# Expressions régulières
## Caractères:
| Symbole | Signification             |
| ------- | ------------------------- |
| .       | Tous sauf nouvelle ligne. |
| ^       | Début de ligne.           |
| $       | Fin de ligne.             |
| \\           | Séquence spéciale ou dé-spécialise un caractère spécial. |     |
| \\d     | Chiffre.                  |
| \\D     | Tous sauf chiffre.        |
| \\w     | Caractère de mot.         |
| \\W     | Pas caractère de mot.     |
| \\s     | Espace.                   |
| \\S     | Taus sauf espace.         |
| \\b     | Début ou fin de mot.      |
| \\A     | Début de mot.             |
| \\Z     | Fin de mot.               |
| \\B     | Milieu de mot.            |

## Ensemble de caractères
| Symbole      | Signification                                            |     |
| ------------ | -------------------------------------------------------- | --- |
| \[agf\]      | Un parmi ces caractères.                                 |     |
| \[a-f0-3\]   | Un entre ces caractères.                                 |     |
| \[\^\]       | Un sauf ces caractères.                                  |     |
| ()           | Ce groupe de caractère.                                  |     |
| expr1\|expr2 | expr1 OU expr2                                           |     |

## Répétition
| Symbole    | Signification                |
| ---------- | ---------------------------- |
| ?          | 0 ou 1 répétition.           |
| *          | Au moins 0 répétitions.      |
| +          | Au moins 1 répétition.       |
| {n}        | n répétitions.               |
| {min, max} | Entre min et max répétition. |
| {min, }    | Au moins min répétitions.    |
| {, max}    | Jusqu'à max répétitions.      |