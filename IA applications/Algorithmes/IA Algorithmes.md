[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_ia)
# Méthode du recuit simulé
```
Fonction critereMetropolis(Δf, T):
	Si Δf <= 0 alors
		retourner VRAI
	sinon
		retourner aléa(0, 1) < exp(-Δf / T)
```
On accepte les candidats selon le critère de Metropolis en diminuant peu à peu le paramètre T. **Initialement, l'ordre de grandeur de T et de Δf doivent être les mêmes.**

```
X, un candidat, fX = f(X) énergie du système, T Température initiale
Xmin <- X
fmin <- f(X)
Tant que T > Tmin et non critèreConvergence()
	Tant que non équilibreThermodynamique()
		// palier de température
		Xvois <- perturbation(X)
		Δf = f(Xvois) - fX
		Si critèreMetropolis(Δf,T) alors
			X <- Xvois
			fX <- f(Xvois)
			Si Δf < 0 et f(Xvois) < fmin alors
				fmin <- f(Xvois)
				Xmin <- Xvois
			Fin si
		Fin si
	Fin tant que
T <- refroidissement(T)
Fin tant que
```
## Équilibre thermodynamique
Faire plusieurs itérations avec la même température pour chercher à sortir de l'optimum local.

## Critère de convergence
Temps, température, amélioration, ...

## Refroidissement
Décroissance géométrique, logarithmique, exponentielle, ...

# LAHC (Late Acceptance Hill Climbing)
## Principe
1. Le candidat courant génère un nouveau voisin.
2. On ajoute le coût du candidat courant à la fin de la mémoire (modulo n).
3. On évalue le coût de ce voisin et on le compare à la performance du n-ème candidat passé (n: taille de la mémoire).
4. S'il l'améliore, alors le voisin devient le candidat courant.

```
X un candidat, f(X) son évaluation
fmin <- f(X), Xmin <- X, tabMemoire <- [fmin, ..., fmin]
n <- 1
Tant que non critereConvergence()
	Xvois <- genereVoisin(X)
	fmem <- tabMemoire[n % tailleMem]
	Si fmem > = f(Xvois) alors
		X <- Xvois
	Fin si
	tabMemoire[n % tailleMem] <- f(X)
	Si fmin > f(Xvois) alors
		Xmin <- Xvois
		fmin <- f(Xvois)
	Fin si 
	n ++
Fin tant que
```

# Optimisations par Essaims Particulaires
Adapté pour les problèmes **continus**.
Multitude d'individus "peu intelligent" (mémoire) mais **communicant**.

## Évolution de la position
- suit sa vitesse (inertie),
- se rappelle du meilleur endroit par lequel il est passé,
- connaît la meilleur position d'autres particules de l'essaim.
☞ Combinaison linéaire de ces vitesses pour obtenir une nouvelle position.

## Algorithme
```
initialisation de l’essaim de N particules
Répéter
	iter + 1
	Pour chaque particule Faire
		maj de sa meilleure position
		maj de la meilleure position du voisinage
	Fin Pour
	Pour chaque particule Faire
		calcul de la nouvelle vitesse
		déplacement de la particule
		évaluation de la nouvelle position
	Fin Pour
	mise à jour de la meilleure position globale
Jusqu’à critèreDArrêt()
```

## Paramètres
### Vitesse
- Coefficient de **confiance** individuel (cognitif) $\rho_1$,
= Coefficient de confiance du groupe (social) $\rho_2$,
- Coefficient d'**inertie** $\psi$.

On pose:
$\rho = c \times rand(0, 1)$

$V_{k + 1} = \psi \times V_k + c_{max} \times rand(0, 1) \times (X_{pbest} - X_k) + c_{max} \times rand(0, 1) \times (X_{vbest} - X_k)$
$c_1 = c_2 = c_{max}$
>[!Example]	Valeurs conseillées
>$(\psi; c_{max}) = (0.7; 1.47)\ ou\ (0.8; 1.62)$
Nombre de particules = 10 + 2 × √(D)

### Voisinages
Si tout le monde parle avec tout le monde, toutes les particules vont vouloir se déplacer vers le même point: 
- convergence rapide,
- mauvaise couverture de l'espace de recherche.

⟹ Autres **voisinages** préférables.

### Problème aux limites
Soit on:
- limite la vitesse,
- ignore l'évaluation de la particule,
- arrête la particule à la limite de l'espace,
- fait rebondir la particule à la limite de l'espace,
- replacer la particule dans un espace périodique,
- replacer la particule au point le plus proche de l'espace de recherche.

### Critère d'arrêt
Possibilités:
- Nombre d'évaluation,
- Temps d'exécution,
- Dernière amélioration.