# LAHC (Late Acceptance Hill Climbing)
>[!Define]
>Algorithme mono-agent adapté aux problèmes discrets.
>Utilisation d'une mémoire de scores de candidats déjà visités.
>Basé sur l'heuristique Greedy Hill Climbing: on accepte un voisin s'il a un meilleur score.
>On itère jusqu'à ce que tous les voisins soit moins bons.

## Principe
1. Le candidat courant génère un nouveau voisin.
2. On ajoute le coût du candidat courant à la fin de la mémoire (modulo n).
3. On évalue le coût de ce voisin et on le compare à la performance du n-ème candidat passé (n: taille de la mémoire).
4. S'il l'améliore, alors le voisin devient le candidat courant.

La comparaison à un moins bon candidat permet l'exploration.
Il devient de plus en plus difficile de trouver des meilleurs points donc on va vers l'exploitation.

```
X un candidat, f(X) son évaluation
fmin <- f(X), Xmin <- X, tabMemoire <- [fmin, ..., fmin]
n <- 1
Tant que non critereConvergence()
	Xvois <- genereVoisin(X)
	fmem <- tabMemoire[n % tailleMem]
	Si fmem > = f(Xvois) alors
		X <- Xvois
	Fin si
	tabMemoire[n % tailleMem] <- f(X)
	Si fmin > f(Xvois) alors
		Xmin <- Xvois
		fmin <- f(Xvois)
	Fin si 
	n ++
Fin tant que
```

Facile à utiliser car moins de paramètres que le recuit simulé.

## Détermination de voisin
Voisins du chemin 1 2 3 4 5 6 7:
- Inversion de points aléatoires pour maintenir un maximum de sommets: 1 6 3 4 5 2 7
- Inversions de points adjacents: 1 3 2 4 5 6 7
- Inversion par renversement pour maintenir un maximum d'arrêtes: 1 6 5 4 3 2 7

Pour le problème du sac à dos, on prend un objet au hasard, s'il est dans le candidat, on l'enlève dans le voisin. Sinon, on le rajoute dans le voisin. Ensuite, on "répare" le candidat s'il est impossible (enlever des éléments au hasard jusqu'à avoir un poids inférieur à la capacité).

## Variantes
Comparaison à un point particulier de la mémoire:
- moyenne
- médiane
- aléatoire
- minimum
- maximum
Augmentation dynamique de la taille de la mémoire