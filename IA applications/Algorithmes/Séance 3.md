# Optimisations par Essaims Particulaires
= Algorithme multi-agent.
Adapté pour les problèmes **continus**.
Multitude d'individus "peu intelligent" (mémoire) mais **communicant**.

## Évolution de la position
### D'une position actuelle:
- suit sa vitesse (inertie),
- se rappelle du meilleur endroit par lequel il est passé,
- connaît la meilleur position d'autres particules de l'essaim.
☞ Combinaison linéaire de ces vitesses pour obtenir une nouvelle position.

### Donc elle doit connaître:
- sa position et son évaluation,
- sa vitesse,
- la meilleure position par laquelle est est passée et son évaluation,
- la composition de son voisinage (pas forcément géométrique) d'informatrices,
- la meilleure position de son voisinage d'informatrices et son évaluation.

### Déroulement de l'algorithme:
- Mise à jour de la vitesse de toutes les particules,
- Déplacement.

## Algorithme
```
initialisation de l’essaim de N particules
Répéter
	iter + 1
	Pour chaque particule Faire
		maj de sa meilleure position
		maj de la meilleure position du voisinage
	Fin Pour
	Pour chaque particule Faire
		calcul de la nouvelle vitesse
		déplacement de la particule
		évaluation de la nouvelle position
	Fin Pour
	mise à jour de la meilleure position globale
Jusqu’à critèreDArrêt()
```

## Paramètres
### Vitesse
- Coefficient de **confiance** individuel (cognitif) $\rho_1$,
= Coefficient de confiance du groupe (social) $\rho_2$,
- Coefficient d'**inertie** $\psi$.

Les deux premiers sont égaux et supérieur au troisième à priori.
On pose:
$\rho = c \times rand(0, 1)$

$V_{k + 1} = \psi \times V_k + c_{max} \times rand(0, 1) \times (X_{pbest} - X_k) + c_{max} \times rand(0, 1) \times (X_{vbest} - X_k)$
$c_1 = c_2 = c_{max}$
>[!Example]	Valeurs conseillées
>$(\psi; c_{max}) = (0.7; 1.47)\ ou\ (0.8; 1.62)$
Nombre de particules = 10 + 2 × √(D)

### Voisinages
>[!Question]
>Qui parle avec qui?

Si tout le monde parle avec tout le monde, toutes les particules vont vouloir se déplacer vers le même point: 
- convergence rapide,
- mauvaise couverture de l'espace de recherche.

⟹ Autres **voisinages** préférables.

Initialisations généralement aléatoire.

### Problème aux limites
Soit on:
- limite la vitesse,
- ignore l'évaluation de la particule,
- arrête la particule à la limite de l'espace,
- fait rebondir la particule à la limite de l'espace,
- replacer la particule dans un espace périodique,
- replacer la particule au point le plus proche de l'espace de recherche.

### Critère d'arrêt
Possibilités:
- Nombre d'évaluation,
- Temps d'exécution,
- Dernière amélioration.