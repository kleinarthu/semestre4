# Optimisation Stochastique
On souhaite avoir la même solution à chaque exécution d'un programme.
Cependant, pour certains problèmes très complexes, il n'existe pas de tel programme efficace.
Dans, ce cas-là, on résout ce problème de manière approché.
> ### Exemple 1:
> Pour *où est Charlie?*, la solution optimale est Charlie 
> -> Le score est la resemblance à Charlie.
> Très grand espace de recherche, observer des solutions candidates, les évaluer et trouver la solution qui maximise la ressemblance avec Charlie.

>### Exemple 2:
>Recherche d'optimal sur une fonction: on cherche un optimum local par descente de gradient (voisinage de point). 

## Malédiction de la dimension
Problème complexe car trop de paramètres à optimiser.
Augmenter le nombre de dimension augmente l'espace "vide" dans un ensemble de n points: besoin de faire un sampling plus lourd.

## Cadre
Problèmes difficiles, de minimisation ou maximisation, à variables discrètes ou continues.

>[!Define] Méthode métaheuristique
>Méthodes globale, approchées, stochastiques, qui ne garantissent pas l'optimalité.

## Initialisation
L'initialisation est aléatoire ou intelligente (sampling, hypercube latin, séquence de Hamilton, ...).

## Déplacement
Exploration = aller loin.
Exploitation = faire de petits déplacements.
### Détermination du voisinage dans un problème combinatoire
Chaque candidat est la permutation d'un autre candidat.

Générer des voisins à l'aide de méthodes (gradient, Newton-Raphson, dichotomie, polytope de Nelder-Mead, ...).

# Méthode du recuit simulé
>[!Define]
>Analogie à la technique physique du recuit qui permet de solidifier un matériau en le liquéfiant puis en le refroidissant très doucement pour créer une réorganisation.

## Analogie
La fonction à minimiser (f) est l'énergie du système.
Un candidat faisable (X) représente un état du matériau.
L'équilibre thermodynamique est atteint lors d'un palier de température.

Amélioration de la méthode de Metropolis-Hastings: on cherche une solution optimale, si on trouve mieux on change, sinon on change parfois dans l'espoir de faire encore mieux ensuite).

```
Fonction critereMetropolis(Δf, T):
	Si Δf <= 0 alors
		retourner VRAI
	sinon
		retourner aléa(0, 1) < exp(-Δf / T)
```
On accepte les candidats selon le critère de Metropolis en diminuant peu à peu le paramètre T. **Initialement, l'ordre de grandeur de T et de Δf doivent être les mêmes.**

```
X, un candidat, fX = f(X) énergie du système, T Température initiale
Xmin <- X
fmin <- f(X)
Tant que T > Tmin et non critèreConvergence()
	Tant que non équilibreThermodynamique()
		// palier de température
		Xvois <- perturbation(X)
		Δf = f(Xvois) - fX
		Si critèreMetropolis(Δf,T) alors
			X <- Xvois
			fX <- f(Xvois)
			Si Δf < 0 et f(Xvois) < fmin alors
				fmin <- f(Xvois)
				Xmin <- Xvois
			Fin si
		Fin si
	Fin tant que
T <- refroidissement(T)
Fin tant que
```
## Équilibre thermodynamique
Faire plusieurs itérations avec la même température pour chercher à sortir de l'optimum local.

## Critère de convergence
Temps, température, amélioration, ...

## Refroidissement
Décroissance géométrique, logarithmique, exponentielle, ...

# No free lunch theorem
>[!Theorem]
>Il n'existe pas de menu parfait pour un omnivore.
> -> Il n'existe pas de méthode parfaite pour tous les problèmes.