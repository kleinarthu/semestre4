# Parties du cours
1. Langage CaML
2. Fonctions, polymorphisme
3. Listes
4. Évaluation différées

# Évaluation
Projet en groupe + QCM en 1h

# Définition
>[!define]
>Pas d'effet de bord.
>### Règles:
>- Plus de variables (car modifier une variable est un effet de bord, remplacées par constantes)
>- Plus d'instructions (remplacées par expressions **typées**)
>- Plus de procédure (remplacées par fonctions pures)
>- Plus de boucles (remplacées par récursivité)

Toute fonction est une expression typée.

# Exemples de langages
- Lisp
- CaML
- Scala
- Haskell
- ...

# Comparaison avec programmation impérative
Equivalence des représentations avec la programmation impérative.
>[!success] Points positifs
>Plus simple, Pas d'effet de bord.
>Donc plus facile à tester.
>Plus proche des mathématiques.

>[!failure] Points négatifs
>Plus gourmand en ressources.
>Plus compliqué à implementer pour les entrées-sorties et générations aléatoires.

Donc meilleurs car besoin de:
- Disponibilité
- Scalabilité
- Interfaces pour utilisateurs
- Concurrence (pas de problèmes de variables modifiées par plusieurs threads car pas de variables)

# Installation
``` shell
apt install ocaml
ocaml
```
Toute commande se termine par ;;
Quitter avec Ctrl-D ou
```OCaML
#quit ;;
```

## Consoles améliorées
```shell
apt intall rlwrap
rlwrap ocaml
```

```shell
apt install utop
utop
```

# Types
- int: + - \* / mod float_of_int(pas de conversion implicite)
-  float: +. -. \*. /. \*\*
- char entre '': int_of_char char_of_int
- string entre "": String.length ^(concaténation) l.[]
- bool (true ou false): not && ||

# Comparaison
Entre éléments du même type uniquement.
Opérateurs:
- =
- <>
- <=
- <
- \>
- \>=

# Déclaration
## Globale
```OCaML
let x = 5 ;;
```
## Locale
```OCaML
let x = 5 and y = 2 in x + y ;;
```

# Condition
else obligatoire car expression et renvoie le même type.
```OCaML
let b = true in
	if b then
		0
	else
		1 ;;
```

# Types composés
## Tuples
Type T1 \* T2 \* T3 \* ... \* Tn

## Couple
T1 \* T2
Accès direct par fst et snd

## Enregistrement
Tuple nommé à composantes nommées.
Composantes non modifiables mais possibilité de créer à partir d'anciens enregistrements.
```OCaML
type t_personne = {
	nom: string;
	prenom: string
} ;;
let rdl: t_personne = {
	nom = "Dujol";
	prenom = "Romain"
}
let other: t_personne = {
	rdl with
	nom = "Gary"
}
```

# Filtrage
## Filtrage par déclaration inversée
Déclaration des composantes, possibilité d'ignorer avec \_.
Imbrications possibles:
```OCaml
let ((x, _), z) = ((1, 2.), "trois") ;;
```

## Pattern Matching
Filtrage par reconnaissance de motifs.
Exhaustif grâce au cas défaut (\_).
Séquentialité: premier motif correspondant choisi.
Unicité: chaque identifiant ne peut apparaître qu'une fois (contournable avec when).
```OCaml
match (1, 2, 3) with
| (0, _, _) -> 0
| (1, _, _) -> 1
| _ -> 2 ;;
```
```OCaml
match (2, 2) with
| (x, y) when x = y -> x
| _ -> 0 ;;
```
```OCaml
match {nom = "KLEIN"; prenom = "Arthur"} with
| {nom = "KLEIN"; prenom = "Arthur"} -> "moi"
| {nom = "KLEIN"} -> "famille"
| _ -> "autre" ;;
```

# Lancer un fichier
```OCaml
# use nom_du_fichier ;;
```
S'arrête à la première erreur.

# Déclaration de fonctions
Fonction = type comme un autre (first-class citizen)
Déclarée avec let ou let ... in.
T -> R
T: types en entrée (tuple si plusieurs paramètres)
R: type du résultat
Instanciée avec function ou fun
Mention de récursivité avec rec.
```OCaml
let f = fun n -> n + 1 ;;
let f n = n + 1 ;;
```

# Curryfication
Transformer une fonction à plusieurs variables en une composition de fonctions à une seule variable.
Possibilité de réutiliser plus tard une fonction intermédiaire.
```OCaml
let f = fun (x, y) -> x * y ;;
let f = fun x -> fun y -> x * y ;;
let f x y = x * y ;;
```