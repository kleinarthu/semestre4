# Évaluations différées
Normalement pas besoin pour le projet + pas évaluer pendant le QCM.
Quelque chose qui se retrouve de plus en plus dans les langages fonctionnels (Haskell, Scala, ...).

# Différence avec l'évaluation standard
>[!Define] Évaluation standard
>L'expression est évaluée à la **déclaration**.
>>[!Example]
>>```OCamL
>>let x = expr;;
>>```

>[!Define] Évaluation différée
>L'expression est évaluée à la première **demande**.
>>[!Example]
>>```OCamL
>>let x = lazy (expr);;
>>```

# Utilisation en OCamL
Module **lazy** préchargé.
## Constructeur
```OCamL
lazy (expr) (* diffère si expr non constante. *)
```

## Accès
```OCamL
Lazy.force: 'a Lazy.t -> 'a
```

## Savoir si on y a déjà accédé
```OCamL
Lazy.is_val: Lazy.t -> bool
```

>[!info]
>```OCamL
>type Lazy_t = 'a Lazy.t;;
>```

## Différer l'image d'une valeur différée par une fonction
```OCamL
Lazy.map: ('a -> 'b) -> 'a Lazy.t -> 'b Lazy.t
Lazy.map f lzy = lazy (f (Lazy.force x));;

Lazy.map_val: ('a -> 'b) -> 'a Lazy.t -> 'b Lazy.t
(* La différence entre les deux est que si le lazy a déjà été évalué, map_val évalue directement sa valeur *)
```

# Séquence
>[!Define] Séquence
>Liste dont la tête et la queue sont en évaluation différée.

>[!Success] Avantages
>- On ne produit les éléments qu'à la demande.
>- On peut donc créer des séquences infinies.

>[!Failure] Inconvénients
>Certaines opérations sur les listes ne terminent pas pour certaines séquences infinies:
>- Fonctionnent **toujours** celles qui ne forcent pas l'évaluation (append, concat, filter, map, mapi, concat_map, filter_map, split, zip).
>- Fonctionnent **parfois** les fonctions de recherche (find, for_all, exists).
>- Ne fonctionneront **jamais** certaines fonctions (length, fold_left).

## Fonctions
```OCaml
type 'a Seq.t = unit -> 'a Seq.node (* Permet de différer l'évaluation *)
type 'a Seq.node = Nil | Cons of 'a * 'a Seq.t
```

Pattern matching peut recommandé, préférer les opérations du module.
### Lecture
```OCaml
Seq.is_empty: 'a Seq.t -> bool
Seq.uncons: 'a Seq.t -> ('a * 'a Seq.t) option (* None si vide et tete * queue sinon *)
```

### Construction
```OCaml
Seq.empty: 'a Seq.t (* Crée la séquence vide *)
Seq.return: 'a -> 'a Seq.t (* Crée la séquence ne contenant que l'élémont donné *)
Seq.cons: 'a -> 'a Seq.t -> 'a Seq.t
fun () -> Cons (h, s1) (* Pour construire sans évaluer la tête *)

(* Construction pas à pas *)
Seq.unfold: ('b -> ('a * 'b) option) -> 'b -> 'a Seq.t
let rec unfold f u =
	match f u with
	| None -> Seq.empty
	| Some(h, u1) -> Seq.cons h (unfold f u1);;

Seq.cycle: 'a Seq.t -> 'a Seq.t (* Répète une séquence indéfiniment *)
Seq.repeat: 'a 'a Seq.t (* Répète un élément indéfiniment *)
Seq.forever: (unit -> 'a) -> 'a Seq.t (* Réappelle f indéfiniment *)
Seq.iterate: ('a -> 'a) -> 'a -> 'a t (* Commence à la valeur donnée et appelle la fonction à l'élément précédent à chaque étape *)

Seq.ints: int -> Seq.t (* Séquence des entiers à partir de l'entire donné *)
```

>[!Example]
>```OCamL
>let jusqua n = Seq.init(n + 1) (fun x -> x)
>let nats = Seq.unfold (fun n -> Some(n, n + 1)) 0
>```

### Fonctions d'extraction
```OCaml
Seq.take: int -> 'a Seq.t -> 'a Seq.t (* Renvoie la séquence des n premiers éléments *)
Seq.drop: int -> 'a Seq.t -> 'a Seq.t (* Renvoie la séquence sans les n premiers éléments *)
Seq.take_while: ('a -> bool) -> 'a Seq.t -> 'a Seq.t
Seq.drop_while: ('a -> bool) -> 'a Seq.t -> 'a Seq.t
```

### Conversion avec les listes
```OCaml
List.to_seq: 'a List -> 'a Seq.t
List.of_seq: 'a Seq.t -> 'a List
```

## Exemple du crible d’Ératosthène
```OCaml
(* La récursivité ne se termine jamais mais l'évaluation du terme suivant est différée *)
# let rec crible s =
	match Seq.uncons s with
	| None -> Seq.empty
	|  (h, s1) -> let sans_mult_h = Seq.filter (fun n -> n mod h <> 0) s1 in
	                  fun() -> Seq.Cons (h, crible sans_mult_h);;
val crible: int Seq.t -> int Seq.t = <fun>

# let premiers = crible (Seq.ints 2);;
val premiers: int Seq.t = <fun>

# List.of_seq (Seq.take 10 premiers);;
- : int list = [2 ; 3 ; 5 ; 7 ; 11 ; 13 ; 17 ; 19 ; 23 ; 29]
```