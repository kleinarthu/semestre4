# Rappels sur les fonctions
**Toute fonction est citoyen de première classe.**
Une fonction a le même rang (ou citoyenneté) que n'import quel autre objet.
⟶ typée:
- utilisée comme valeur
- peut être paramètre
- peut être renvoyée
- ...

Type T -> R où T: types d'entrées et R: types de sorties

Instanciée avec function ou fun et déclarée avec let ou let...in
rec obligatoire si récursive.
```OCaml
let f = fun n -> n + 1;;
let f n = n + 1;;
```

## Curryfication
Transformer une fonction à plusieurs variables en une séquence de fonctions à une seule variable.
T1 * T2 -> R devient T1 -> T2 -> R

## Opérateurs
Les opérateurs binaires usuels sont curryfiés:
```OCaml
( + );;
-: int -> int -> int = <fun>
( + ) 3 5;;
-: int = 8;;
let inc = ( + ) 1 in inc 3;;
-: int = 4
```
Les opérateurs unaires sont précédés par ~:
```OCaml
(~-);;
-: int -> int;
```

# Types génériques
Précédés d'un '.
Type réel inféré lors de l'évaluation.
Type exact pas toujours déterminable ⟶ détermine les relations entre les types:
```OCaml
let id x = x ;;
val id: 'a -> 'a = <fun>
id 2 ;;
-: int = 2
id "deux" ;;
-: string = "deux" ;;
```
Opérateurs de comparaison génériques
```OCaml
(=) ;;
'a -> 'a -> bool = <fun>

type 'a valeur_etiquetee = {valeur: 'a; etiquette: string};;
```

# Types unions
>[!Define] Type unions
>Type T1 | T2 | T3 | ... | Tn
>où Ti constante ou constructeur.

```OCaml
type 'a option = None | Some of 'a ;;
type orentation = Nord | Sud | Est | Ouest ;; (* énumération *)
```

## Filtrage
```OCaml
let (x, y) = (1, 2) ;;
val x: int = 1
val y: int = 2

let Some(x) = Some(42) ;;
val: int = 42
```

# Types récursifs
>[!Define] Type récursif
>Type exprimé en fonction de lui-même
>⟶ nécessite:
>- un cas terminal
>- une définition en fonction du type

⟹ type union
```OCaml
type 'a liste = Vide | Cons of 'a * ('a liste) ;;

type expr_arith =
	| Nombre of int
	| Plus of expr_arith * expr_arith
	| Moins of expr_arith * expr_arith
	| Fois of expr_arith * expr_arith ;;
 
let e = Fois(Nombre 6, Plus(Nombre 4, Nombre 3));;

let rec valeur e = match e with
	| Nombre(x) -> x
	| Plus (e1, e2) -> (valeur e1) + (valeur e2)
	| Moins (e1, e2) -> (valeur e1) - (valeur e2)
	| Fois (e1, e2) -> (valeur e1) * (valeur e2) ;;
```

# Modules
>[!Define] Module
>Regroupement de déclaration et implémentation de:
>- types
>- constantes
>- fonctions

Organisés en deux sections:
- **sig** (optionnelle): déclare les valeurs publiques sans ;;
- **struct**: implémente obligatoirement toute les valeurs privées + celles déclarées dans sig.
S'il n'y a pas de sig, tout est publique.

Les noms de modules sont en Majuscule:
```OCaml
NomModule.nomValeur ;;
open NomModule ;; (* pour accéder directement à tout ce qui est publique *)
nomValeur;;
```

## Exemple
```OCaml
module ExprArith:
	sig
		type t
		val nb: int -> t
		val ( +! ) : t -> t -> t
		val ( -! ) : t -> t -> t
		val ( *! ) : t -> t -> t
		val valeur : t -> int
	end =
	struct
		(* Valeurs publiques *)
		type t =
			| Nombre of int
			| Plus of t * t
			| Moins of t * t
			| Fois of t * t ;;
		(* Valeur privée *)
		let e = Fois(Nombre 6, Plus(Nombre 4, Nombre 3));;
		let nb x = Nombre(x) ;;
		let ( +! ) e1 e2 = Plus(e1, e2) ;;
		let ( -! ) e1 e2 = Moins(e1, e2) ;;
		let ( *!) e1 e2 = Fois (e1, e2) ;;
		let rec valeur e = matchu27fc e with
			| Nombre(x) -> x
			| Plus(e1, e2) -> (valeur e1) + (valeur e2)
			| Moins (e1, e2) -> (valeur e1) - (valeur e2)
			| Fois (e1, e2) -> (valeur e1) * (valeur e2) ;;
	end ;;
```

```OCaml
#use "ExprArith.ml" ;;
ExprArith.valeur ;;

open ExprArith ;;
valeur ;;
```

# Type Option (préchargé)
```OCaml
type 'a option  = None | Some of 'a ;;
```
⟶ type union

## Fonctions prédéfinies
```OCaml
#use "option.ml"

let is_none o = match o with
	| Some(_) -> false
	| None -> true ;;
	
let is_some o = o |> is_none |> not ;;

let value o z = match o with
	| Some v -> v
	| None -> z ;;
	
let join o = match o with
	| Some v -> v
	| None -> None ;;

let bind o f = match o with
	| Some v -> f v
	| None -> None ;;

let map f o = match o with
	| Some v -> Some(v |> f);;
	| None -> None ;;

let fold z f o = match o with
	| Some v -> f v
	| None -> z ;;
```
⟶ Permet de gérer les erreurs (pas de try ... catch) sans perturber le fonctionnement normal.

# Listes
```OCaml
type 'a list = [] | (::) of 'a * 'a list ;;
```
[] =  liste liste vide
:: = construction