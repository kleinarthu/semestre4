[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progfonc)
# Définition
>[!define]
>Pas d'effet de bord.
>### Règles:
>- Plus de variables (car modifier une variable est un effet de bord, remplacées par constantes)
>- Plus d'instructions (remplacées par expressions **typées**)
>- Plus de procédure (remplacées par fonctions pures)
>- Plus de boucles (remplacées par récursivité)

# Installation
``` shell
apt install ocaml
ocaml
```
Toute commande se termine par ;;
Quitter avec Ctrl-D ou
```OCaML
#quit ;;
```

## Consoles améliorées
```shell
apt intall rlwrap
rlwrap ocaml
```

```shell
apt install utop
utop
```

# Types
- int: + - \* / mod float_of_int(pas de conversion implicite)
-  float: +. -. \*. /. \*\*
- char entre '': int_of_char char_of_int
- string entre "": String.length ^(concaténation) l.[]
- bool (true ou false): not && ||

# Comparaison
Entre éléments du même type uniquement.
Opérateurs:
- =
- <>
- <=
- <
- \>
- \>=

# Déclaration
## Globale
```OCaML
let x = 5 ;;
```
## Locale
```OCaML
let x = 5 and y = 2 in x + y ;;
```

# Condition
else obligatoire car expression et renvoie le même type.
```OCaML
let b = true in
	if b then
		0
	else
		1 ;;
```

# Types composés
Type T1 \* T2 \* T3 \* ... \* Tn

## Enregistrement
```OCaML
type t_personne = {
	nom: string;
	prenom: string
};;
let rdl: t_personne = {
	nom = "Dujol";
	prenom = "Romain"
};;
let other: t_personne = {
	rdl with
	nom = "Gary"
};;
```

# Filtrage
## Filtrage par déclaration inversée
Déclaration des composantes, possibilité d'ignorer avec \_.
```OCaml
let ((x, _), z) = ((1, 2.), "trois") ;;
```

## Pattern Matching
Exhaustif grâce au cas défaut (\_).
Séquentialité: premier motif correspondant choisi.
Unicité: chaque identifiant ne peut apparaître qu'une fois (contournable avec when).
```OCaml
match (2, 2) with
| (1, 0) | (0, 1) -> 1 (* Possibilité de lister plusieurs cas *)
| (x, y) when x = y -> x
| _ -> 0 ;;
```

# Lancer un fichier
```OCaml
# use nom_du_fichier ;;
```

# Déclaration de fonctions
Instanciée avec function ou fun
Mention de récursivité avec rec.
```OCaml
let f = fun n -> n + 1 ;;
let f n = n + 1 ;;
```

```OCaml
(~-);;
-: int -> int;
```

# Types génériques
Précédés d'un '.
Type réel inféré lors de l'évaluation.
```OCaml
val id: 'a -> 'a = <fun>
```

# Types unions
```OCaml
type 'a option = None | Some of 'a ;;
type orentation = Nord | Sud | Est | Ouest ;; (* énumération *)
```

# Modules
>[!Define] Module
>Regroupement de déclaration et implémentation de:
>- types
>- constantes
>- fonctions

Organisés en deux sections:
- **sig** (optionnelle): déclare les valeurs publiques sans ;;
- **struct**: implémente obligatoirement toute les valeurs privées + celles déclarées dans sig.
S'il n'y a pas de sig, tout est publique.

```OCaml
NomModule.nomValeur ;;
open NomModule ;; (* pour accéder directement à tout ce qui est publique *)
nomValeur;;
```

## Exemple
```OCaml
module ExprArith:
	sig
		type t
		val ( +! ) : t -> t -> t
		val ( -! ) : t -> t -> t
		val ( *! ) : t -> t -> t
		val valeur : t -> int
	end =
	struct
		(* Valeurs publiques *)
		type t =
			| Nombre of int
			| Plus of t * t
			| Moins of t * t
			| Fois of t * t ;;
		(* Valeur privée *)
		let e = Fois(Nombre 6, Plus(Nombre 4, Nombre 3));;
		let ( +! ) e1 e2 = Plus(e1, e2) ;;
		let ( -! ) e1 e2 = Moins(e1, e2) ;;
		let ( *!) e1 e2 = Fois (e1, e2) ;;
		let rec valeur e = matchu27fc e with
			| Nombre(x) -> x
			| Plus(e1, e2) -> (valeur e1) + (valeur e2)
			| Moins (e1, e2) -> (valeur e1) - (valeur e2)
			| Fois (e1, e2) -> (valeur e1) * (valeur e2) ;;
	end ;;
```

# Type Option
```OCaml
type 'a option  = None | Some of 'a ;;
```

## Fonctions prédéfinies
```OCaml
val is_none 'a Option 
val is_some 'a Option
val value 'a Option -> 'a
val join 'a Option Option -> 'a Option
val bind 'a Option -> ('a -> 'b Option) -> 'b Option
val map ('a Option -> 'b) -> 'a Option -> 'b Option) (* Similaire à bind pour les fonctions qui ne renvoient pas d'Option *)
let fold 'a -> ('b Option -> 'a) -> 'b Option -> 'a
```

# Listes
```OCaml
type 'a list = [] | (::) of 'a * 'a list;;
```
## Fonctions prédéfinies
>[!Info]
>[Documentation](HTTPS://V2.OCAML.ORG/API/lIST.HTML)

```OCaml
val List.length: 'a list -> int
val List.hd: 'a list -> 'a
val List.tl: 'a list -> 'a list
val List.nth 'a list -> int -> 'a
val List.nth_opt: 'a list -> int -> 'a option
val List.mem: 'a -> 'a list -> bool (* true ssi 'a est membre de la liste *)
val List.rev: 'a list -> 'a list
val List.init: int -> (int -> 'a) -> 'a list (* Construit à partir de la fonctin de construction *)
val List.append: 'a list -> 'a list -> 'a list (* (@) *)
val List.concat: 'a list list -> 'a list
val List.flatten: 'a list list -> 'a list (* Équivalent à List.concat *)

val List.exists: ('a -> bool) -> 'a list -> bool
val List.for_all: ('a -> bool) -> 'a list -> bool
val List.find: ('a -> bool) -> 'a list -> 'a
val List.find_opt: ('a -> bool) -> 'a list -> 'a option

val List.find_all: ('a bool) -> 'a list -> 'a list
val List.filteri: (int -> 'a -> bool) -> 'a list -> 'a list
val List.partition: ('a -> bool) -> 'a list -> 'a list * 'a list (* Liste qui vérifie * Liste qui ne vérifie pas *)

val List.map: ('a -> 'b) -> 'a list -> 'a list
val List.mapi: (int -> 'a -> 'b) -> 'a list -> 'b list
val List.concat_map: ('a -> 'b list) -> 'a list -> 'b list
val List.filter_map: ('a -> 'b option) -> 'a list -> 'b list (* Enlève les None et extrait la valeur des Some *)

let filter_map f l =
	let l1 = List.map f l in
	let l2 = List.filter Option.is_some l1 in
	List.map Option.get l2;;
 
val List.fold_left: ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a (* Terminal *)
let fold_left op z l = match l with
	| [] -> z
	| h :: t -> fold_left op (op z h) t;;
 val List.fold_right: ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b (* Non terminal *)
 
val List.assoc: 'a -> ('a * 'b) list -> 'b
val List.assoc_opt: 'a -> ('a * 'b) list -> 'b option
val list.mem_assoc: 'a -> ('a * 'b) list -> bool
val List.remove_assoc: 'a -> ('a * 'b) list -> ('a * 'b) list
val List.split: ('a * 'b) list -> 'a list * 'b list
val List.combine: 'a list -> 'b list -> ('a * 'b) list (* Les listes doivent être de même longueur *)

val List.sort: ('a -> 'a -> int) -> 'a list -> 'a list (* compare x y du signe de x - y *)
val List.stable_sort: ('a -> 'a -> int) -> 'a list -> 'a list (* Garantit l'ordre pour les éléments égaux *)
```

# Module lazy pour l'évaluation différée

## Constructeur
```OCamL
lazy (expr) (* diffère si expr non constante. *)
```

## Accès
```OCamL
Lazy.force: 'a Lazy.t -> 'a
```

## Savoir si on y a déjà accédé
```OCamL
Lazy.is_val: Lazy.t -> bool
```

## Différer l'image d'une valeur différée par une fonction
```OCamL
Lazy.map: ('a -> 'b) -> 'a Lazy.t -> 'b Lazy.t
Lazy.map f lzy = lazy (f (Lazy.force x));;

Lazy.map_val: ('a -> 'b) -> 'a Lazy.t -> 'b Lazy.t
(* La différence entre les deux est que si le lazy a déjà été évalué, map_val évalue directement sa valeur *)
```

# Séquence
>[!Define] Séquence
>Liste dont la tête et la queue sont en évaluation différée.

## Fonctions
```OCaml
type 'a Seq.t = unit -> 'a Seq.node (* Permet de différer l'évaluation *)
type 'a Seq.node = Nil | Cons of 'a * 'a Seq.t
```

### Lecture
```OCaml
Seq.is_empty: 'a Seq.t -> bool
Seq.uncons: 'a Seq.t -> ('a * 'a Seq.t) option (* None si vide et tete * queue sinon *)
```

### Construction
```OCaml
Seq.empty: 'a Seq.t (* Crée la séquence vide *)
Seq.return: 'a -> 'a Seq.t (* Crée la séquence ne contenant que l'élémont donné *)
Seq.cons: 'a -> 'a Seq.t -> 'a Seq.t
fun () -> Cons (h, s1) (* Pour construire sans évaluer la tête *)

(* Construction pas à pas *)
Seq.unfold: ('b -> ('a * 'b) option) -> 'b -> 'a Seq.t
let rec unfold f u =
	match f u with
	| None -> Seq.empty
	| Some(h, u1) -> Seq.cons h (unfold f u1);;

Seq.cycle: 'a Seq.t -> 'a Seq.t (* Répète une séquence indéfiniment *)
Seq.repeat: 'a 'a Seq.t (* Répète un élément indéfiniment *)
Seq.forever: (unit -> 'a) -> 'a Seq.t (* Réappelle f indéfiniment *)
Seq.iterate: ('a -> 'a) -> 'a -> 'a t (* Commence à la valeur donnée et appelle la fonction à l'élément précédent à chaque étape *)

Seq.ints: int -> Seq.t (* Séquence des entiers à partir de l'entire donné *)
```

### Fonctions d'extraction
```OCaml
Seq.take: int -> 'a Seq.t -> 'a Seq.t (* Renvoie la séquence des n premiers éléments *)
Seq.drop: int -> 'a Seq.t -> 'a Seq.t (* Renvoie la séquence sans les n premiers éléments *)
Seq.take_while: ('a -> bool) -> 'a Seq.t -> 'a Seq.t
Seq.drop_while: ('a -> bool) -> 'a Seq.t -> 'a Seq.t
```

### Conversion avec les listes
```OCaml
List.to_seq: 'a List -> 'a Seq.t
List.of_seq: 'a Seq.t -> 'a List
```
