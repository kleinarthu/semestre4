# Listes
>[!Info]
>[Documentation](HTTPS://V2.OCAML.ORG/API/lIST.HTML)


Liste = type récursif
```OCaml
type 'a list = [] | (::) of 'a * 'a list;;
let exemple = 5 :: 2 :: [];;
let exemple = [5, 2];;
```

Type récursif ⟶ Filtrage par motifs

```OCaml
let rec nombre_elements l = match l with
	| [] -> 0
	| _ :: t -> 1 + nombre_elements t;;
```

## Fonctions de base
```OCaml
val List.length: 'a list -> int
val List.hd: 'a list -> 'a
val List.tl: 'a list -> 'a list
val List.nth 'a list -> int -> 'a
val List.nth_opt: 'a list -> int -> 'a option
val List.mem: 'a -> 'a list -> bool (* true ssi 'a est membre de la liste *)
val List.rev: 'a list -> 'a list
val List.init: int -> (int -> 'a) -> 'a list (* Construit à partir de la fonction de construction *)
val List.append: 'a list -> 'a list -> 'a list (* (@) *)
val List.concat: 'a list list -> 'a list
val List.flatten: 'a list list -> 'a list (* Équivalent à List.concat *)
```

### Fonctions à prédicats
>[!Define] Prédicat
>```OCaml
>'a -> bool
>```

```OCaml
val List.exists: ('a -> bool) -> 'a list -> bool
val List.for_all: ('a -> bool) -> 'a list -> bool
val List.find: ('a -> bool) -> 'a list -> 'a
val List.find_opt: ('a -> bool) -> 'a list -> 'a option
```

### Fonctions d'extraction
```OCaml
val List.find_all: ('a -> bool) -> 'a list -> 'a list (* Equivalent à List.filter *)
val List.filteri: (int -> 'a -> bool) -> 'a list -> 'a list
val List.partition: ('a -> bool) -> 'a list -> 'a list * 'a list (* Liste qui vérifie * Liste qui ne vérifie pas *)
```

### Fonctions de transformation
```OCaml
val List.map: ('a -> 'b) -> 'a list -> 'a list
val List.mapi: (int -> 'a -> 'b) -> 'a list -> 'b list
val List.concat_map: ('a -> 'b list) -> 'a list -> 'b list
val List.filter_map: ('a -> 'b option) -> 'a list -> 'b list (* Enlève les None et extrait la valeur des Some *)

let filter_map f l =
	let l1 = List.map f l in
	let l2 = List.filter Option.is_some l1 in
	List.map Option.get l2;;
```

### Fonctions d'agrégation
```OCaml
val List.fold_left: ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a (* Terminal *)
let fold_left op z l = match l with
	| [] -> z
	| h :: t -> fold_left op (op z h) t;;
 val List.fold_right: ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b (* Non terminal *)
```
 
 >[!Example]
>```OCaml
>let longueur l = List.fold_left (fun z x -> z + 1) 0 l;;
>let inverser l = List.fold_left (fun z x -> x :: z) [] l;;
>```

### Fonctions d'association
>[!Define] Fonctions d'association
>Liste de couples clé/valeur.

```OCaml
val List.assoc: 'a -> ('a * 'b) list -> 'b
val List.assoc_opt: 'a -> ('a * 'b) list -> 'b option
val list.mem_assoc: 'a -> ('a * 'b) list -> bool
val List.remove_assoc: 'a -> ('a * 'b) list -> ('a * 'b) list
val List.split: ('a * 'b) list -> 'a list * 'b list
val List.combine: 'a list -> 'b list -> ('a * 'b) list (* Les listes doivent être de même longueur *)
```

### Fonctions de tri
```OCaml
val List.sort: ('a -> 'a -> int) -> 'a list -> 'a list (* compare x y du signe de x - y *)
val List.stable_sort: ('a -> 'a -> int) -> 'a list -> 'a list (* Garantit l'ordre pour les éléments égaux *)
```

# Consignes projet
**Objectif:** Mettre en place une API simplifiée de property testing.
**Groupes** de 4 à 5 étudiants
**Rendu** avant le 2 avril
**Soutenance** à partir du 5 avril

**Rendu:**
Méthodes à implémenter
Signatures obligatoirement respectée.
Rapport décrivant ce qui a été mis en place, les difficultés, l'intérêt des fonctionnalités.

**Soutenance:**
Soutenance de 10 à 15 minutes + questions (total: 20 min).

## Présentation générale
Inspiré de QuickCheck (Haskell):
- Définir une propriété devant être vérifiée par une (ou plusieurs) fonction(s) quels que soient les paramètres ,
- Générer aléatoirement des paramètres de test,
- Si un jeu de paramètre ne vérifie pas la propriété, en chercher un plus "simple" ne la vérifiant pas.

Composition:
- Property: Gestion des propriétés,
- Generator: Génération aléatoire de données,
- Reduction: Stratégies de simplifications,
- Test: Gère l'ensemble.

## Property
```OCaml
type 'a Property.t = 'a -> bool;;
```

## Génération
Définir le type générique.
Appliquer pour générer une valeur.
Utiliser le module Random.

## Réduction
```OCaml
type 'a Reduction.t = 'a -> 'a list;;
```
Stratégie vide (pas d'amélioration)
Stratégies pour les types de base
Stratégies pour les listes, chaînes de caractères, couples, ...
Ne jamais proposer la valeur donnée en paramètre.
Les suggestions commencent par la plus simple.

## Test
Définition du type générique
Création du test
Lancement d'un ou plusieurs tests.