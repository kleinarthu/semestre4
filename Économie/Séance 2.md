# Qu'est ce que le PIB ?
- **PIB** = mesure de la richesse d’un PIB = tout ce qui est produit / an = somme (VA)
**VA** : valeur des productions marchantes et non-marchantes (profs par exemple)
VA = CA – consommations intermédiaires
- PIB **nominal** dépend du prix quand la quantité ne change pas
Pas représentatif du niveau de richesse.
- ⇒ **IPC** mesuré par l’INSEE permet de déterminer le PIB réel
- Besoin de convertir dans une monnaie unique ($PPA) pour gommer l’écart entre les monnaies.
- Divisé par habitant pour potentiel de **richesse par habitant**
- Devrait prendre en compte la qualité de vie (**Bonheur Intérieur Brut**).

# Création monétaire, un juste équilibre :
- Biens créés pour prêts puis circulent puis sont remboursés. Plus il y a de **prêts**, plus il y a d’argent en circulation → **inflation**.
L’inflation doit être inférieur à **3 %** pour que les salaires augmentent autant que les prix.
- Si la monnaie ne circule pas, il y a **déflation** et les prix diminuent : les gens attendent pour acheter et acheter moins cher → moins de consommation et donc de production.
- ⟹ **Banque Centrale Européenne** contrôle et effectue les injections de monnaies à l’intérieur des pays avec un taux direct régulant l’inflation.