# Les courbes de Phillips
>[!Important] Courbes de Phillips (1958)
>Relation inverse entre le taux de croissance des salaires nominaux et le taux de chômage.

⟸ plus de chômage, plus de population pouvant remplacer ⟶ moins de pouvoir de négociation

Montre le conflit dans la politique économique entre la lutte contre le chômage et la lutte contre l'inflation.

## Limites
La courbe de Phillips trouve ses limites avec les travaux des économistes M. Friedman, E.Phelps, et Robert Lucas qui démontrent qu'il n'y avait pas de prévision possible à long terme.
### Courbe en sapin
![](https://finance-heros.fr/wp-content/uploads/2021/12/image-2.png)
Les salariés se rendent vite compte que les augmentations de salaires ne font que suivre l'évolution des prix (**anticipation adaptative**) et le chômage ré-atteint sa valeur précédente pour une inflation supérieure. Cette vision est confirmée dans les années 1970 par l'apparition de la stagflation.
>[!Define] Stagflation
>Situation économique où la croissance est faible et l'inflation est forte.

### Courbes verticales
Robert Lucas et Edmund Phelps pense que cette illusion ne devrait pas se traduire par une courbe en sapin, mais plutôt une courbe verticale (**anticipation rationnelle**): les agents économiques comprennent et connaissent à l'avance l'évolution des prix.

[Site de l'INSEE](https://www.insee.fr/fr/)