# PIB
Le PIB est constitué par 1/3 du revenu du capital et par 2/3 du revenu du travail.
>[!Important] Formules
>$PIB = VA + TVA + douane - subventions\ sur\ l'importation$

- PIB marchand = biens et services (business)
- PIB non-marchand = administration

## Mesures dérivées du PIB
- Taux de croissance économique de l'année $n$: ${{PIB_{n} - PIB_{n - 1}} \over {PIB_{n - 1}}} \times 100$
- Niveau de vie: $PIB \over population$
- Le Revenu National Brut (RNB): $PIB - revenus\ reçues\ de\ l'étranger + revenus\ versés\ à\ l'étranger$

# Inflation
## PIB réel
>[!Important] Formule
>$PIB_{reel} = {PIB_{nominal}\over IPC} \times 100$
>$IPC = Deflateur \times 100$

>[!Define] Déflater
>Corriger une valeur économique pour prendre en compte l'inflation.

## Causes de l'inflation
- Par la **monnaie**: trop de création de monnaie,
- Par la **demande**: demande trop supérieur à l'offre,
- Par les **coûts**: augmentation des coûts de production, on parle inflation importée si elle se répercute sur le reste de l'économie (eg essence).

## Conséquences de l'inflation
- Baisse du **pouvoir d'achat** (calculée en nominal et réel),
- Baisse de la **dette** ⟶ transfert du pouvoir d'achat (eg le remboursement des prêt sur l'immobilier devient mois significatif),
- Les prix de l'**importation** devienne plus faible que ceux de la production interne.

⟹ L'inflation doit être comprise entre 2% et 4%.

Déflation (baisse des prix, possibilité de spirale déflationniste) ≠ Désinflation (baisse de l'inflation)

# Chômage
>[!Important] Formule
>$u = {U \over L}$
>où $u$ est le taux de chômage,
>$U$ est le nombre de chômeurs,
>$L$ est la population active.

>[!Define] Population active
>Ceux qui reçoivent un salaire (ceux qui consomment).

## Définition du BIT (INSEE)
Utilisée pour les comparaisons internationales:
Prérequis:
- Avoir plus de 15 ans,
- Être sans travail,
- Possibilité de travailler sous 15 jours,
- Rechercher un emploi.

## Définition de Pôle Emploi
Plus souvent utilisé au niveau national.
5 catégories (tous demandeurs d'emploi):
- Catégorie A: Sans emploi + en recherche d'emploi
- Catégorie B: En emploi de courte durée (moins de 78h/mois) + en recherche d'emploi
- Catégorie C: En emploi de longue durée + en recherche d'emploi
- Catégorie D: Sans recherche active + sans emploi (stage, formation, maladie, ...)
- Catégorie E: Sans recherche active + en emploi (contrats aidés, ...)

## Halo du chômage
>[!Define] Halo du chômage
>Personnes inactives pour le BIT mais proche du marché de l'emploi.

![](https://media.kartable.fr/uploads/finalImages/final_5f19a86e6d06d3.50771677.png)

## Types de chômage
- Chômage **technique**: manque de ressource consommées par l'entreprise (plus d'énergie ⟶ licenciement),
- Chômage **partiel**: moins de travailleurs pour couvrir les coûts fixes,
- Chômage **conjectural**: inflation ⟶ moins de consommation ⟶ moins de production ⟶ licenciement
- Chômage **structurel**: déséquilibre entre ou dans une zone économique (eg fermeture du charbon ou gaz),
- Chômage **frictionnel**: période entre deux emplois.

# Loi d'Okun
>[!Important] Loi d'Okun
>Plus il y a de croissance, plus il y a d'emploi.
>Plus il y a de chômage, plus le PIB baisse.
>⟹ corrélation

Plus il y a de consommateurs, plus tu consommes, plus tu produits, plus le PIB augmente.
⟹ On peut donc calculer l'évolution du nombre d'emplois pour un point de croissance.
⟹ Donc on peut déterminer un taux de croissance minimal à atteindre pour réduire le chômage.

⟹ Réponse par:
- la croissance de la population active,
- l'augmentation de la productivité.

↳ On peut réguler l'économie pour éviter les crises