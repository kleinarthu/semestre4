# Mondialisation
>[!Define] Mondialisation (années 90)
>Libre échange ⟶ on peut acheter partout.
> Tous les biens économiques circulent sans entrave, les entreprises, les systèmes productifs et les États devenant concurrents.

>[!Success] Avantages
>Main d'oeuvre moins chère.

>[!Failure] Inconvénients
>Moins bonne qualité.

⟹ Le monde est un marché unique.
Permet par:
- des décisions politiques (déréglementation, libéralisation des échanges),
- des évolutions technologiques,
- des évolutions géopolitiques,
- la globalisation financière (1970),
- l'autorisation de concurrence étrangère dans les marchés protégés,
- certaines nouvelles menaces (emploi / délocalisation).

>[!Define] Firme multinationale
>Grand entreprise nationale qui possède ou contrôle plusieurs filiales de production dans **plusieurs pays**.

## Vidéo: [Un produit mondialisé: le jean](https://youtu.be/bE47ZXXyFr4)
Permet de diminuer le coût de production (main d'oeuvre et matière première.)
Permis par des transports moins chers.
Concerne, les mafias, terrorisme, drogues, ...
Impact négatif: conditions de travail + pollution pour la production et le transport.

>[!Define] [CEPII](http://www.cepii.fr/CEPII/fr/cepii/cepii.asp)
>Équivalent international de l'INSEE.

# Le commerce extérieur
## Indicateurs:
>[!Define] Solde commercial
>**Solde  commercial** = exportations - importations
>On parle de :
>- déficit commercial si négatif,
>- excédent si positif,
>- équilibre si nul.

>[!Define] Taux de couverture
>**Taux de couverture** = exportations / importations × 100

>[!Define] Euro constant/courant
>**Euro constant** = avec l'inflation
>**Euro courant** = sans l'inflation

>[!Define] Termes de l'échange
>**Termes de l'échange** = prix des exportations / prix des importations

>[!Define] Degré d'ouverture d'une économie
>**Degré d'ouverture d'une économie** = moyenne des exportations et importations / PIB
>Mesure l'importance des produits importés dans l'économie.

>[!Taux de pénétration d'un marché]
>**Taux de pénétration d'un marché** = importations / marché intérieur × 100
>où marché intérieur = Production nationale + importations - exportations

>[!Define] Efforts à l'exportation
>**Effort à l'exportation** = exportation / production × 100

## Compétitivité
>[!Define] Compétitivité
>Capacité d'une entreprise ou d'un secteur à faire face à la **concurrence**.

>[!Define] Compétivité prix
>**Prix** inférieure à la concurrence.

>[!Define] Compétitvité coût
>**Coût** de production inférieure à la concurrence.

>[!Define] Compétitivité structurelle
>-  **Qualité**, services, ... meilleure que la concurrence.

>[!Define] Contrainte extérieure
>**Dépendance** d'une économie à l'égard des autres économies.
>>[!Example]
>>Les contraintes énergétiques aggravent le déficit commercial et donc nous contraint à augmenter nos exportation pour compenser la balance commerciale.

Crise des sub-primes: Ils ont fait des MPS (empilement des prêts immobiliers) et les banques avaient tous des mauvais MPS et se sont effondrées et, comme elles subventionnait l'économie européenne, l'économie européenne s'est aussi effondrée.
⟹Économies conjoncturelles = économiques

### Vidéo: [Qu'est ce que la compétitivité? Et comment l'améliorer?](https://youtu.be/uHDpRAXiiq0)
**Compétitivité prix** déterminée par:
- les coûts et la productivité,
- le taux de change (taux de conversion de monnaies),
- la concurrence,
- les coûts de transport ...
Pour l'améliorer, on doit améliorer le coût de change (banque centrale) ou le coût de production (qualité des infrastructures, proximité des partenaires, qualification des employés, l'organisation, la technologie, ...).
**Compétitivité hors-prix** déterminés par la stratégie d'une économie et donc l'environnement:
- réglementation,
- social,
- fiscal,
- économique.
Cela peut être améliorer par l'État en jouant sur l'innovation, la montée en gamme et les pôles de compétitivité (Airbus).

### Raisons de la croissance des échanges mondiaux
- Baisse des coûts des transports et autre coûts de transaction,
- Évolution des politiques commerciales.

# Protectionnisme
>[!Define] Protectionnisme
> Doctrine et politique économique qui est fondée sur l'application de mesures visant à **favoriser les activités nationales** et à pénaliser la concurrence étrangère.

On peut jouer sur les **droits de douane ad valorem** (= sur la valeur).
Cependant, cela n'améliore pas l'image du produit (qualité, service après-vente, ...).

>[!Define] Dumping
>Pratique illégale qui consiste à **vendre à perte** un produit sur les marchés étrangers moins cher que sur son propre marché.

## Vidéo: [Qu'est ce que l'avantage comparatif ?](https://youtu.be/ip9d1UJ4RYg)
Ricardo explique comment améliorer les échanges mondiaux.
1. Supprimer les taxes douanières (libre-échange),
2. Chaque pays produit ce pour quoi il est le plus productif (avantage comparatif).
La valeur travail permet de déterminer la productivité.

>[!Define] Spécialisation
>Répartition des activités productives entre les différentes économies. Elle peut être sectorielle, géographique, ...