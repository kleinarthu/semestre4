# Keynes
Priorise la consommation pour renforcer la demande et inciter à l’offre.

# Produit intérieur brut: Croissance économique
>[!important] Calcul du PIB
>PIB = ∑(VA) = ∑(CA - CI)
>- où VA = Valeur ajoutée
>- où CA = Chiffre d'affaire
>- où CI = Consommations intermédiaires.
>
>PIB = Revenu du travail + EBE + RMB - Impôts sur la production et l'importation
>- où EBE = Excédent Brut d'Exploitation = VA - rémunération du travail
>- où RMB = Revenu Mixte Brut = les revenus des entrepreneurs individuels
>
>==PIB = Dépense de consommation finale + FBCF + exportations - importations==
>- où FBCF = Formation Brute de Capitale Fixe = Investissements

## Mesures dérivées du PIB
- Taux de croissance économique de l'année $n$: ${{PIB_{n} - PIB_{n - 1}} \over {PIB_{n - 1}}} \times 100$
- Niveau de vie: $PIB \over population$
- Le Revenu National Brut (RNB): PIB - revenus reçues de l'étranger + revenus versés à l'étranger

## PIB réel et nominal
PIB réel → prix fixe (ne dépend que des quantités)
PIB nominal → prix variables (moins bon indicateur de la croissance)
>[!Important] Formule
>$PIB_{reel} = {PIB_{nominal}\over IPC} \times 100$

# Taux d'inflation: Indice des Prix à la Consommation
>[!Define] IPC
>${IPC_{base}(annee)} = {{p_{panier}(annee) } \over {p_{panier}(base)}}$

>[!Define] Déflateur du PIB
>Rapport entre PIB réel et PIB nominal

>[!important] Formule
>$Inflation = {{IPC_f - IPC_i} \over {IPC_i}} \times 100\%$

# Chômage
>[!Important] Formule
>$u = {U \over L}$
>où $u$ est le taux de chômage,
>$U$ est le nombre de chômeurs,
>$L$ est la population active.

>[!Define] Population active
>Ceux qui peuvent travailler (salariés + chômeurs).

# Loi d'Okun
>[!Important] Loi d'Okun
>Corrélation négative entre le PIB et le chômage.

# Les courbes de Phillips
>[!Important] Courbes de Phillips (1958)
>Relation inverse entre le taux de croissance des salaires nominaux et le taux de chômage.

# Le commerce extérieur
## Indicateurs:
>[!Define] Solde commercial
>**Solde  commercial** = exportations - importations

>[!Define] Taux de couverture
>**Taux de couverture** = exportations / importations × 100

>[!Define] Termes de l'échange
>**Termes de l'échange** = prix des exportations / prix des importations

>[!Define] Degré d'ouverture d'une économie
>**Degré d'ouverture d'une économie** = moyenne des exportations et importations / PIB
>Mesure l'importance des produits importés dans l'économie.

>[!Taux de pénétration d'un marché]
>**Taux de pénétration d'un marché** = importations / marché intérieur × 100
>où marché intérieur = Production nationale + importations - exportations

>[!Define] Efforts à l'exportation
>**Effort à l'exportation** = exportation / production × 100

## Compétitivité
>[!Define] Compétitivité
>Capacité d'une entreprise ou d'un secteur à faire face à la **concurrence**.

>[!Define] Compétivité prix
>**Prix** inférieure à la concurrence.

>[!Define] Compétitvité coût
>**Coût** de production inférieure à la concurrence.

>[!Define] Compétitivité structurelle
>-  **Qualité**, services, ... meilleure que la concurrence.

>[!Define] Contrainte extérieure
>**Dépendance** d'une économie à l'égard des autres économies.

# Spécialisation
## Avantage absolu
>[!Define] Avantage absolu (Adam Smith)
>Un acteur a une meilleur productivité et coût salarial qu'un autre.

## Avantage relatif
>[!Define] Avantage relatif (Ricardo)
>Le pays A à un avantage comparatif sur le produit 1 ssi:
>${{prod_{A1}} \over {prod_{A2}}} \gt {{prod_{B1}} \over {prod_{B2}}}$.

>[!Define] Coût d'opportunité
>Quantité d'un produit ou service gagné sur le temps que l'on ne passe pas à produire un autre produit ou service.

>[!Important] Échange profitable
>${cout_{A1} \over cout_{A2}} \le taux \le {cout_{B1} \over cout_{B2}}$