# Spécialisation
>[!Exemple] Tête d'épingle (Adam Smith)
>Le tissage a toujours existé mais:
>- *Avant division du travail:* Un expert produit jusqu'à 10 épingles par jour,
>- *Après division du travail:* Quatre ouvriers peuvent produire 4000 épingles par jour.

Si les ressources sont rares, les agents ont un intérêt mutuel à les affecter entre eux de façon optimale.

## Avantages
### Avantage absolu
>[!Define] Avantage absolu (Adam Smith)
>Un acteur a une meilleur productivité et coût salarial qu'un autre.

### Avantage relatif
>[!Define] Avantage relatif (Ricardo)
>Le pays A à un avantage comparatif sur le produit 1 ssi:
>${{prod_{A1}} \over {prod_{A2}}} \gt {{prod_{A1}} \over {prod_{A2}}}$.

>[!Define] Productivité
>$prod = {q \over T}$

Le coût de production est généralement horaire.

## Vidéo [À quoi bon échanger ? Avantages comparatifs & coût d'opportunité - Argument frappant #5](https://www.youtube.com/watch?v=HAlayHY3Z8E)
2 personnes sont sur une île déserte avec 2 tâches:
- entretenir un abri,
- collecter 10 kg de nourriture.
La spécialisation et l'échange permettent de gagner du temps.
Vaut que chacun est un domaine dans lequel il est meilleur que l'autre (exemple 1) ou que l'un soit meilleur pour les deux (exemple 2) parce que le gain se mesure en coût d'opportunité.
### Exemple 1
|           | Productivité abri | Productivité collecte |
| --------- | ----------------- | --------------------- |
| Individu1 | 15h/sem           | 5h/kg                 |
| Individu2 | 20h/sem           | 3h/kg                 |
#### Avant spécialisation:
|           | Temps passé abri | Temps passé collecte | Temps total |
| --------- | ---------------- | -------------------- | ----------- |
| Individu1 | 15h (1 abri)     | 50h (10kg)           | 65h         | 
| Individu2 | 20h(1 abri)      | 30h (10kg)           | 50h         |   

#### Après spécialisation:
|           | Temps passé abri | Temps passé collecte | Temps total |
| --------- | ---------------- | -------------------- | ----------- |
| Individu1 | 30h (2 abris)    | 25h (5kg)            | 55h         |
| Individu2 | 0h               | 45h (10 + 5kg)       | 45h         |

### Exemple 2
|           | Productivité abri | Productivité collecte |
| --------- | ----------------- | --------------------- |
| Individu1 | 15h/sem           | 1h/kg                 |
| Individu2 | 20h/sem           | 3h/kg                 |
#### Avant spécialisation:
|           | Temps passé abri | Temps passé collecte | Temps total |
| --------- | ---------------- | -------------------- | ----------- |
| Individu1 | 15h (1 abri)     | 10h (10kg)           | 25h         | 
| Individu2 | 20h(1 abri)      | 30h (10kg)           | 50h         |   

#### Après spécialisation:
|           | Temps passé abri | Temps passé collecte | Temps total |
| --------- | ---------------- | -------------------- | ----------- |
| Individu1 | 0h               | 20h (20kg)           | 20h         |
| Individu2 | 40h (2 abris)    | 0h                   | 40h         |

>[!Define] Coût d'opportunité
>Quantité d'un produit ou service gagné sur le temps que l'on ne passe pas à produire un autre produit ou service.

⟹ L'économie n'est pas un jeu à somme nulle.