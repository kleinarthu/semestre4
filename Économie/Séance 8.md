# L'impact de la production sur le climat

>[!Example] Syndrome de la grenouille
>- On plonge une grenouille dans de l'eau froide puis de l'eau très chaude ⟶ la grenouille quitte l'eau et survit,
>- On plonge une grenouille dans de l'eau froide que l'on chauffe peu à peu à la même température ⟶ la grenouille meurt brûlée.

## Principes de thermodynamique
>[!Define] Principe de conservation de l'énergie
>L'énergie globale sur Terre est constante ⟹ on ne peut que profiter, exploiter les sources d'énergies.

Toutes les énergies primaires sont **gratuites**: le coût provient de:
- l'exploitation,
- le transport,
- la transformation.
⟹Les énergies plus **concentrées** sont plus rentables. ⟹ Importance du fossile, et en particularité du pétrole.

>[!Define] Deuxième principe
>Principe d'évolution.

>[!Define] Troisième règle
>Règle de proportionnalité.

## Importance de l'énergie
>[!Question] Pourquoi l'énergie est-elle importante ?
>L'énergie est la raison:
>- du climat,
>- de la biodiversité,
>- du confort,
>- de l'alimentation,
>- ...

>[!Example] Example du **cuivre**
>Important pour la production et le transport d'énergie.
>⟹ Besoin de de plus en plus de cuivre.
>Cependant, le cuivre est de plus en plus coûteux à exploiter car de moins en moins concentré.

Toutes les exploitations de minerais cumulés n'atteignent pas le marché de l'énergie.

## Facteurs de pollution
>[!Important] Équation d'après KAYA
>$CO2 = POP \times {{PIB} \over {POP}} \times {{E} \over {PIB}} \times {{CO2} \over {E}}$
>D'où l'importance de:
>- la population,
>- la richesse,
>- l'efficacité énergétique,
>- la propreté de l'énergie utilisée.

3 manières d'apprendre:
- créativité,
- copie,
- expérience.

## Pétrole au Venezuela
Le pétrole peut être extrait à la main au Venezuela (sable bitumineux). Ce procédé est très coûteux car le pétrole est peu concentré mais les 3-4% du marché mondial composés par cette production a un fort impact sur le prix du pétrole car une mine de pétrole peut être arrêtée, contrairement aux puits.

## Caractérisation de l'énergie
L'énergie se caractérise par un changement d'état:
- Température,
- Vitesse,
- Forme,
- ...