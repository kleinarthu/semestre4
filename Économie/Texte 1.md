# Chapitre 2 de *Macroéconomie* par Mankiw
La théorie et l'observation sont les outils de l'économie.
Ces observations peuvent aujourd'hui être faites aux quotidiens et les politiciens et économises utilisent de nombreuses études statistiques.
Ce chapitre présente les outils principaux de cette analyse:
- Le PIB,
- L'IPC,
- Le taux de Chômage.

## 1. La mesure de la valeur de l'activité économique: le Produit Intérieur Brut
>[!Define] PIB
>Une des ou la meilleure mesure de performance.
>Établie tous les 3 mois par le Bureau of Economic Analysis aux États Unis grâce:
>- aux données collectées par les impôts, les programmes d'éducation, la Défense et la réglementation,
>- aux enquêtes publiques.
> ==C'est la valeur en $ de l'activité économique pour une période donnée.==
> ==\= Revenu total de l'ensemble des agents de l'économie.==
> ==\= Dépense totale consacrée à l'acquisition de biens et services produits par l'économie.==

Intéressant car comprend tous les agents et représente une qualité de satisfaction des besoins.
Les deux dernières définitions sont équivalentes car les transactions font toujours intervenir un acheteur et un vendeur.
PIB mesuré dans le cadre de la comptabilité nationale.

### 1.1 Le revenu, la dépense et le circuit économique
![[CircuitEco.png]]
>[!Important] PIB
>= flux total au sein de l'économie
>= revenu total de production
>= salaires + profits
>= dépense totale consacrée au bien vendu

Augmenter la production = Augmenter l'achat + (Augmenter la main d'œuvre OU Augmenter le profit)
Donc dépenses = revenus.

Un stock est une quantité à un instant.
Un flux est une quantité en fonction du temps.
Le PIB est un flux.

### 1.2 Quelques règles de calcul du PIB
>[!Define] PIB
>Le PIB est la valeur marchande de tous les biens et services finaux produits par une économie au cours d'une période donnée.

La valeur utilisée pour convertir les produits est la valeur marchande;
>[!Important] PIB
>PIB = ∑(prix × quantité)

Les biens usagés ne comptent pas car transfert de richesse .
On fait comme si l'entreprise achetait ses stocks, ce qui augmente le PIB. Le stock sera par la suite un bien usagé.
Le PIB ne compte que les biens finaux car prix des biens intermédiaires inclus dedans.

>[!Important] PIB
>PIB = ∑(VA)
>où VA = valeur de production - valeur des biens intermédiaires.

Si le produit n'est pas vendu sur le marché, la valeur est imputée (= estimée). (eg pour les logements et le service public)
Certains produit ne sont pas imputés pour des raisons de simplicité.
L'économie souterraine n'est pas compté (10% de l'économie des US).

⟹PIB est une mesure imparfaite.

### 1.3 Le PIB réel et le PIB nominal
Variation du PIB nominal si variation du prix ou des quantités mais ne représente pas vraiment la satisfaction de besoin car tous les prix pourraient doubler sans représenter une meilleur richesse.
>[!Define] PIB réel
>PIB qui part du principe que le prix est constant pour ne compter que les quantités.
>Il utilise un instant de référence pour les prix.

### 1.4 Le déflateur du PIB.
>[!Define]
>Déflateur = PIBnominal ÷ PIBréel

Reflète l'évolution des prix de l'unité

### 1.5 La pondération en chaîne du PIB réel
Problèmes du PIB réel: l'année de référence ne reflète plus forcément les valeurs d'importance relative des produits.
Au départ, nouvelle année de référence tous les 5 ans aux US.
En 1995, proposition de pondération en chaîne: prix\_moyen(n + 1) - prix\_moyen(n) = croissance_reelle(n, n + 1)
Croissances réelles ajoutées pour comparer la production.

Variation_pourcentage(P × Y) ≈ Variation_pourcentage(P) + Variation_pourcentage(Y)

### 1.6 Les composantes de la dépense
>[!Important] Identité comptable
>PIB = Consommation + Investissement + DépensePublique + ExportationsNettes
>où:
>- Consommation: Biens et services achetés par les ménages (biens non durables, biens durables et services),
>- Investissement: Achat pour utilisation future (fixe des entreprises, fix résidentiel des ménages, en stocks des entreprises),
>- Dépense publique: Biens et services achetés par l'État,
>- Exportation nette: Échanges avec les autres pays (positive si exportations > importations: excédent commercial; négative si exportations < importations: déficit commercial).

### 1.7 Les autres mesures du revenu
>[!Important] PNB
>Produit National Brut = PIB + (revenus des facteurs en provenance du reste du monde) - (revenus des facteurs versés au reste du monde)

Mesure mieux revenu total des résidents du pays (et non sur son territoire).
Différence de 4% aux US.

>[!Important] PNN
>Produit National Net = PNB - Amortissements (= consommation de capital fixe = perte annuelle de stock de capital existant = 16% du PNB)

>[!Important] RN
>Revenu National = Ce qu'ont gagné tous les agents de l'économie = PNN - Divergence statistique (si sources diffèrentes).

Catégories du RN:
- rémunération des salariés (62%),
- revenu des entrepreneurs individuels (8%),
- revenus de la propriété des particuliers et de l'État (4%),
- bénéfices réservés des entreprises (13%),
- intérêts nets (4%),
- impôts indirects liés à la production (9%) (eg TVA)

>[!Important] Revenu personnel
>= Revenu national - impôts indirects liés à la production
>\- bénéfices des entreprises - cotisation de sécurité sociale - Intérêts nets
>\+ dividendes + transferts publics aux ménages + revenus d'intérêt personnels

>[!Important] Revenu Personnel disponible
>= Revenu Personnel - (Impôts sur les personnes physiques et prélèvements non fiscaux)

### 1.8 Les ajustements saisonniers
Ces variables suivent une variation saisonnière entre trimestres (augmentation de 8% sur l'année).
Données ajustées pour compenser ces variations.

## 2. La mesure du coût de la vie: l'Indice des Prix à la Consommation
Mesure l'inflation
### 2.1 Les prix d'un panier de biens et de services
Indice des Prix à la Consommation calculé par le Bureau of Labor Statistics aux US
>[!Define] IPC
>Mesure du prix de tous les biens et services pondérés par leurs consommations

>[!Important] Calcul de l'IPC
>IPC = ∑(quantité × prix) ÷ ∑(quantité × prix_anneeBase)

>[!Define] IPP
>Similaire à l'IPC mais pour les entreprises.

>[!Define] Inflation sous-jacente
>= Core inflation = mesure de l'évolution des prix à la consommation sans les matières premières agricoles et énergétiques (rop de variations)

### 2.2 Indice des prix à la consommation, déflateur du PIB et déflateur des dépenses de consommation (PCE)
Déflateur du PIB est une mesure de l'inflation mais non-centrée sur le consommateur, restreint au territoire national, et avec un panier de consommation qui "évolue".
>[!Define] Indice de Laspeyres
>Indice de prix calculé sur la base d'un panier constant de biens, surestime l'impact d'un hausse du prix.

>[!Define] Indice de Paasche
>Indice de prix calculé sur la base d'un panier évolutif, sous-estime l'impact d'une hausse du prix (n'y apparaît pas).

>[!Define] Déflateur PCE
>= Déflateur implicite des prix pour les dépenses de consommation = déflateur du PIB sans les investissements des ménages et les achats de biens et services effectués par les pouvoirs publics

### 2.3 L'indice des prix à la consommation surestime-t'il l'inflation?
L'IPC permet de baser le rattrapage sur le coût de la vie car mesure précise.
Mais: 
- Inclut un biais de substitution qui surestime l'évolution des prix,
- Il ne baisse pas quand de nouveaux produits et donc choix apparaissent,
- Il ne tient pas compte de l'évolution de la qualité des biens (voitures, informatique, ...).

Une étude de 1995 montrait une surestimation de 0.8 à 1.6%/ans.
Maintenant, on le baisse de 1%.

## 3. La mesure du chômage: le taux de chômage
### 3.1 Les enquêtes auprès des ménages
En France, enquêtes par l'INSEE avec trois réponses:
- Ayant un emploi,
- N'ayant pas d'emploi,
- Ne faisant pas partie de la population active (incluant les travailleurs découragés).

>[!Define] Population active
>Population ayant un emploi + Population en recherche d'emploi

>[!Important] Taux de chômage
>= Population en recherche d'emploi ÷ Population active

>[!Important] Taux de participation
>= Population active ÷ Population en âge de travailler

### 3.2 L'enquête auprès des établissements
Possibilité d'estimer l'emploi par des organisation regroupant des entreprises recrutant beaucoup.
Les indicateurs peuvent être contradictoires car enquêtes imparfaites et critères différents.