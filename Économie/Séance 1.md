**Notation**: note d'exam + note d'oral + note de TD
**CAC40**: 40 plus grandes entreprises en valorisation boursière

>[!Define] Macroéconomie
>Comportement agrégé, étudie les phénomènes économiques globaux, explique les relations entre les indicateurs macroéconomiques au niveau national et international.
>On fait des hypothèses sur un groupe entier.

# Indicateurs
- **Revenu national brut**: moyenne des revenus bruts au niveau national.
- **PIB**: cumul des valeurs ajoutées au sein d'un pays.
- **Taux d'inflation**
Catégories homogènes.

# Utilité
Prédire l'évolution des différentes variables → éviter les crises économiques (choc pétrolier 1979, ...)
Modifier les taux d'épargne → modifier l'argent que les consommateurs réinjectent → relancer l'économie et augmenter le PIB.

**Balance commerciale déficitaire/excédentaire**: plus/mois d'import que d'export.

# Keynes
Autre modèles échouent face à la crise de 1929.
Consommation plus importante que l’offre.
Priorise la consommation pour renforcer la demande et inciter à l’offre.
Inciter à des hausses de salaires imposés qui décourage l’investissement.
Donne un rôle central à l’état qui investit, fait du déficit pour augmenter la consommation pour amener une augmentation de la production pour diminuer le chômage. → cercle vertueux

passage du modèle classique au modèle de Keynes au passage à l’euro (avant, on imprimait des billets pour combattre les crises).

Crise de 2008 →production industrielle chute mais reprend contrairement à celle de 1929 grâce à l'utilisation de Keynes → explication par choc d'offre donne place à une explication par la consommation.
Crise de production →suffit d'attendre pour revenir à la normal
==Effondrement de la bourse → pauvreté de la population → rien dans les banques → rien dans les entreprises → pauvreté ⟹ cercle vicieux==
Soutenir les banques pour les prêts d'urgence
Baisser les impôts, dépenser plus

→ Plan de relance budgétaire