Semaine prochaine: théorie des jeux.
# Suite de l'impact de la production sur le climat
Les énergies fossiles coûtent beaucoup moins cher et sont moins efficaces.

>[!Example] Comparaison: Robert Förstemann (cyclist) vs Toaster
>Vidéo [Olympic Cyclist Vs. Toaster: Can He Power It?](youtube.com/watch?v=S4O5voOCqAq):
>Un cycliste olympique peut produire 700W pendant 3 minutes.
>Par le passé, les longs déplacement nécessitaient de changer de chevaux régulièrement dans des relais de postes. Aujourd'hui, la plupart des déplacements sont effectués par des moyens de déplacement équivalent à plusieurs centaines de chevaux pouvant se déplacer pour de très longue durée.

## Nouvelles sources d'énergie
- L'exploitation du charbon a sauvé la forêt européenne,
- L'exploitation initiale du pétrole comme lubrifiant a sauvé les baleines.

Aujourd'hui, les **nouvelles sources s'empilent**: il n'y a pas de véritable transition énergétique.

## Croissance du PIB
Pour régler les problèmes énergétiques (inégalité, pollution, épuisement des ressources, ...) il suffirait de **revenir au confort de vie des années 1970**.
Le **prix réel des objets** depuis a été divisé par entre 30 et 100 sur les 150 dernières années.

### Évolution de la production mondiale
PPP en milliards d'euros 2012:
| Année | PPP en milliards d'euros | Facteur de croissance | Raison                               |
| ----- | ------------------------ | --------------------- | ------------------------------------ |
| 0     | 142                      |                       |                                      |
| 1800  | 930                      | ×6 en 1800 ans         | Croissance de la population mondiale |
| 1950  | 7134                     | ×7 en 130 ans          | Industrialisation                    |
| 2012  | 71170                    | ×10 en 60 ans          | Société de consommation              |
Le PIB et la consommation d'énergie évoluent de manière **proportionnelle** depuis 1965.

### Croissance future d'après l'économie
- Les modèles économiques ne prévoient **pas de limite**.
- Les courants économiques étudient uniquement la répartition entre **travail et capital** et non le système productif, l'énergie, les ressources naturelles, ...
- En effet, depuis **Jean-Baptiste Say**, les ressources sont gratuites et donc peuvent être considérées comme inépuisables et n'ont pas besoin d'être étudiée.

- Cependant, le travail n'est pas limitant du fait de la population mondiale, et le capital n'est pas limitant grâce à l'inflation: le **confort est limité par les ressources naturelles et l'énergie**.
- La croissance de l'énergie n'est pas infinie: la croissance de l'exploitation du pétrole diminue fortement au fur et à mesure que les ressources ayant le meilleur **EROEI** sont épuisées.

>[!Define] EROEI
>Energy Received Over Energy Invested.

La solution économique est d'inonder les acteurs en capital (= dette) mais cela n'améliore la situation que pour les individus ayant accès à ce système économique.
Aujourd'hui, un américain consomme 2 fois plus qu'un européen.