>[!Define] Macroéconomie
>Comportement agrégé, étudie les phénomènes économiques globaux, explique les relations entre les indicateurs macroéconomiques au niveau national et international.
>On fait des hypothèses sur un groupe entier.

# Indicateurs
- **Revenu national brut**: moyenne des revenus bruts au niveau national.
- **PIB**: cumul des valeurs ajoutées au sein d'un pays.
- **Taux d'inflation**
Catégories homogènes.

# Keynes
Priorise la consommation pour renforcer la demande et inciter à l’offre.

# Produit intérieur brut: Croissance économique
>[!Define] PIB
>Indicateur économique important car utilisé pour d’autres indicateurs.

>[!important] Calcul du PIB
>PIB = ∑(VA) = ∑(CA - CI)
>- où VA = Valeur ajoutée
>- où CA = Chiffre d'affaire
>- où CI = Consommations intermédiaires.
>
>PIB = Revenu du travail + EBE + RMB - Impôts sur la production et l'importation
>- où EBE = Excédent Brut d'Exploitation = VA - rémunération du travail
>- où RMB = Revenu Mixte Brut = les revenus des entrepreneurs individuels
>
>==PIB = Dépense de consommation finale + FBCF + exportations - importations==
>- où FBCF = Formation Brute de Capitale Fixe = Investissements
>
>PIB = VA + TVA + douane - subventions sur l'importation

## Mesures dérivées du PIB
- Taux de croissance économique de l'année $n$: ${{PIB_{n} - PIB_{n - 1}} \over {PIB_{n - 1}}} \times 100$
- Niveau de vie: $PIB \over population$
- Le Revenu National Brut (RNB): PIB - revenus reçues de l'étranger + revenus versés à l'étranger

## PIB réel et nominal
PIB réel → prix fixe (ne dépend que des quantités)
PIB nominal → prix variables (moins bon indicateur de la croissance)
>[!Important] Formule
>$PIB_{reel} = {PIB_{nominal}\over IPC} \times 100$

# Taux d'inflation: Indice des Prix à la Consommation
>[!Define] Taux d'inflation
>Rapport entre les prix de deux paniers identiques d'une année à une autre.

>[!Define] IPC
>Indice calculé par l'INSEE permettant de calculer le PIB réel, l'inflation, faire des conversions.
>Plus on produit, plus l'IPC augmente.
>l'INSEE surveille 1000 produits sur 250 entreprises pour le calculer.

## Autres indicateurs de l'inflation
- **Déflateur du PIB**: Rapport entre PIB réel et PIB nominal
- **PCE** similaire à IPC mais avec une variation plus discrète de l'importance.

## Calcul du taux d'inflation
>[!important] Formule
>$Inflation = {{IPC_f - IPC_i} \over {IPC_i}} \times 100\%$

## Causes de l'inflation
- Par la **monnaie**: trop de création de monnaie,
- Par la **demande**: demande trop supérieur à l'offre,
- Par les **coûts**: augmentation des coûts de production, on parle inflation importée si elle se répercute sur le reste de l'économie (eg essence).

## Conséquences de l'inflation
- Baisse du **pouvoir d'achat** (calculée en nominal et réel),
- Baisse de la **dette** ⟶ transfert du pouvoir d'achat (eg le remboursement des prêt sur l'immobilier devient mois significatif),
- Les prix de l'**importation** devienne plus faible que ceux de la production interne.

⟹ L'inflation doit être comprise entre 2% et 4%.

Déflation (baisse des prix, possibilité de spirale déflationniste) ≠ Désinflation (baisse de l'inflation)

# Chômage
>[!Important] Formule
>$u = {U \over L}$
>où $u$ est le taux de chômage,
>$U$ est le nombre de chômeurs,
>$L$ est la population active.

>[!Define] Population active
>Ceux qui reçoivent un salaire (ceux qui consomment).

## Définition du BIT (INSEE)
Utilisée pour les comparaisons internationales:
Prérequis:
- Avoir plus de 15 ans,
- Être sans travail,
- Possibilité de travailler sous 15 jours,
- Rechercher un emploi.

## Définition de Pôle Emploi
Plus souvent utilisé au niveau national.
5 catégories (tous demandeurs d'emploi):
- Catégorie A: Sans emploi + en recherche d'emploi
- Catégorie B: En emploi de courte durée (moins de 78h/mois) + en recherche d'emploi
- Catégorie C: En emploi de longue durée + en recherche d'emploi
- Catégorie D: Sans recherche active + sans emploi (stage, formation, maladie, ...)
- Catégorie E: Sans recherche active + en emploi (contrats aidés, ...)

## Halo du chômage
>[!Define] Halo du chômage
>Personnes inactives pour le BIT mais proche du marché de l'emploi.

## Types de chômage
- Chômage **technique**: manque de ressource consommées par l'entreprise (plus d'énergie ⟶ licenciement),
- Chômage **partiel**: moins de travailleurs pour couvrir les coûts fixes,
- Chômage **conjectural**: inflation ⟶ moins de consommation ⟶ moins de production ⟶ licenciement
- Chômage **structurel**: déséquilibre entre ou dans une zone économique (eg fermeture du charbon ou gaz),
- Chômage **frictionnel**: période entre deux emplois.

# Loi d'Okun
>[!Important] Loi d'Okun
>Corrélation négative entre le PIB et le chômage.

# Les courbes de Phillips
>[!Important] Courbes de Phillips (1958)
>Relation inverse entre le taux de croissance des salaires nominaux et le taux de chômage.

## Limites
### Courbe en sapin
![](https://finance-heros.fr/wp-content/uploads/2021/12/image-2.png)
**Anticipation adaptative**: Les salariés se rendent compte que les augmentations de salaires ne font que suivre l'évolution des prix  et le chômage ré-atteint sa valeur précédente pour une inflation supérieure.
>[!Define] Stagflation
>Situation économique où la croissance est faible et l'inflation est forte.

### Courbes verticales
**Anticipation rationnelle**: les agents économiques comprennent et connaissent à l'avance l'évolution des prix, et l'inflation n'a pas d'effet.

# Mondialisation
>[!Define] Mondialisation (années 90)
>Libre échange ⟶ on peut acheter partout.
> Tous les biens économiques circulent sans entrave, les entreprises, les systèmes productifs et les États devenant concurrents.

>[!Define] [CEPII](http://www.cepii.fr/CEPII/fr/cepii/cepii.asp)
>Équivalent international de l'INSEE.

# Le commerce extérieur
## Indicateurs:
>[!Define] Solde commercial
>**Solde  commercial** = exportations - importations

>[!Define] Taux de couverture
>**Taux de couverture** = exportations / importations × 100

>[!Define] Termes de l'échange
>**Termes de l'échange** = prix des exportations / prix des importations

>[!Define] Degré d'ouverture d'une économie
>**Degré d'ouverture d'une économie** = moyenne des exportations et importations / PIB
>Mesure l'importance des produits importés dans l'économie.

>[!Taux de pénétration d'un marché]
>**Taux de pénétration d'un marché** = importations / marché intérieur × 100
>où marché intérieur = Production nationale + importations - exportations

>[!Define] Efforts à l'exportation
>**Effort à l'exportation** = exportation / production × 100

## Compétitivité
>[!Define] Compétitivité
>Capacité d'une entreprise ou d'un secteur à faire face à la **concurrence**.

>[!Define] Compétivité prix
>**Prix** inférieure à la concurrence.

>[!Define] Compétitvité coût
>**Coût** de production inférieure à la concurrence.

>[!Define] Compétitivité structurelle
>-  **Qualité**, services, ... meilleure que la concurrence.

>[!Define] Contrainte extérieure
>**Dépendance** d'une économie à l'égard des autres économies.

# Protectionnisme
>[!Define] Protectionnisme
> Doctrine et politique économique qui est fondée sur l'application de mesures visant à **favoriser les activités nationales** et à pénaliser la concurrence étrangère.

On peut jouer sur les **droits de douane ad valorem** (= sur la valeur).

# Spécialisation
## Avantages
### Avantage absolu
>[!Define] Avantage absolu (Adam Smith)
>Un acteur a une meilleur productivité et coût salarial qu'un autre.

### Avantage relatif
>[!Define] Avantage relatif (Ricardo)
>Le pays A à un avantage comparatif sur le produit 1 ssi:
>${{prod_{A1}} \over {prod_{A2}}} \gt {{prod_{A1}} \over {prod_{A2}}}$.

>[!Define] Coût d'opportunité
>Quantité d'un produit ou service gagné sur le temps que l'on ne passe pas à produire un autre produit ou service.
