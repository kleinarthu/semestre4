# Produit intérieur brut: Croissance économique
>[!Define] PIB
>Indicateur économique important car utilisé pour d’autres indicateurs.
>Permet de mesurer la richesse économique sur une année.
>Variation → croissance (on retire la variation du prix).

Plusieurs approches pour le calcul :
- par production (somme des VA)
- par revenu (consommation, les dépenses étatiques, des exports et des investissements)
>[!Failure] Critiqué car
>- Ignore la valeur de l'activité économique non enregistrée.
>- Met trop en avant le matériel par rapport à l'humain.
>- Besoin de nombreuses estimation (5% d'erreur).
>- Les transferts de richesses prennent en compte les transferts de richesse mais par leur création.
>- Production des armes prises en compte (mais pas dans l'IDH).

## Vidéo
Certaines choses sont estimées (agences étatiques, ...)
Ne prend pas en compte les activités personnelles.
PIB marchand (entreprise) et PIB non-marchand (services).
Croissance négative = **récession**
Permet de mesurer le développement.

>Bonne présentation = 15 slides / h
>Contient des Images, commentaires, mots-clés.

# Qu'est ce que le PIB
>[!important] Calcul du PIB
>$PIB = \sum(VA) = \sum(CA - CI)$ où $VA$: Valeur ajoutée; $CA$: Chiffre d'affaire; $CI$: Consommations intermédiaires.
>$PIB = Revenu\ du\ travail + EBE + RMB - Impots\ sur\ la\ production\ et\ l'importation$
>$EBE = Excédent\ Brut\ d'Exploitation = VA - rémunération\ du\ travail$
>$RMB = Revenu\ Mixte\ Brut = les\ revenus\ des\ entrepreneurs\ individuels$
>$PIB = Dépense\ de\ consommation\ finale + FBCF + exportations - importations$
>$FBCF = Formation\ Brute\ de\ Capitale\ Fixe = Investissements$

# PIB réel et nominal
PIB réel → prix fixe (ne dépend que des quantités)
PIB nominal → prix variables (moins bon indicateur de la croissance)

# Taux d'inflation: Indice des Prix à la Consommation
>[!Define] Taux d'inflation
>Rapport entre les prix de deux paniers identiques d'une année à une autre.

Causes:
- Loi de l'offre et de la demande
- Augmentation des prix de l'énergie
- Politique monétaire des banques centrales

>[!Define] IPC
>Indice calculé par l'INSEE permettant de calculer le PIB réel, l'inflation, faire des conversions.
>Plus on produit, plus l'IPC augmente.
>l'INSEE surveille 1000 produits sur 250 entreprises pour le calculer.

## Autres indicateurs de l'inflation
- **Déflateur du PIB**: trois différences avec l'IPC (prend tout en compte y compris l'achat des entreprises et de l'État, ne prend pas en compte les biens importés, l'importance de chaque produit au cours du temps change). ==Rapport entre PIB réel et PIB nominal== = indice du PIB réel.
- **PCE** similaire à IPC mais avec une variation plus discrète de l'importance.

## Calcul du taux d'inflation
Généralement calculé avec l'IPC. Comparation des IPCs.
Deux conditions pour le calculer: indice des prix au début de la période, indice des prix à la fin de la période.
>[!important] Formule
>$Inflation = {{IPC_f - IPC_i} \over {IPC_i}} \times 100\%$
