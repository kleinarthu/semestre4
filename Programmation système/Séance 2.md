# Recouvrement
Permet de charger en mémoire un nouveau code exécutable.
Primitives de la famille EXEC.
⟶ le processus repart à 0 avec le texte d'un autre programme.
## Code de nommage des fonctions
- **l pour list**: paramètres données dans une liste terminée par NULL,
- **v pour vector**: tableau,
- **p pour path**: recherche du fichier avec la variable d'environnement PATH,
- **e pour environment**: transmission d'un environnement en dernier paramètre, en remplacement de l'environnement courant.

```C
int execl(const char* path, const char* arg, ...);
int execlp(const char* file, const char* arg, ...);
int execle(const char* path, const char* arg, ..., char* const envp[]);
int execv(const char* path, char* const argv[]);
int execvp(const char* file, char* const argv[]);
int execvpe(const char* file, char* const argv[], char* const envp[]);
```
>[!info]
>```shell
>man 3 exec

>[!Example]
>```C
>#include <stdio.h>
>int main() {
>	execl("bin/ls", "ls", "-l", NULL);
>	printf("Erreur lors de l'appel à ls \n");
>}
>```

```C
#inslude <stdlib.h>
int system(const char* command);
```
pour exécuter une commande comme dans le shell (plus lente ⟶ déconseillé).

# Entrée / Sortie
3 descripteurs de fichiers à la création:
- 0 pour stdin,
- 1 pour stdout,
- 2 pour stderr.
## Variables d'environnement
```C
#include <stdlib.h>
char* getenv (const char* variables_environnement);
int putenv(const char* chaine); // 0 si pas d'erreur
```

## Manipulation des fichiers en C
```C
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int desc;
int open(const char *nomfich, int mode_ouverture, mode_t droits); // mode_t droits optionnel, utilisé uniquement si création de fichier
desc = open(nomfich, mode_ouverture, droits);
close(desc);
```

| mode_ouverture | Mode                                |
| -------------- | ----------------------------------- |
| O_RDONLY       | En lecture                          |
| O_WRONLY       | En écriture                         |
| O_RDWR         | En lecture et en écriture           |
| O_NONBLOCK     | Ouverture dans un mode non bloquant |
| O_APPEND       | Ajout à la fin du fichier           |
| O_CREAT        | Crée le fichier s'il n'existe pas   |
| O_TRUNC        | Remet le fichier à 0 s'il existe    |
| O_EXCL         | Échoue si le fichier existe         |

Pour les droits:
- S_I + {W/R/X} + {USR/GRP/OTH}

```C
#include <unistd.h>
#inclde <sys/types.h>
ssize_t read(int desc, void *ptf, size_t nb_octets);
ssize_t write(int desc, void *ptf, size_t nb_octets);
off_t lseek(int desc, off_t offset, int origine); // modifie l'offset par rapport à une origine dans le fichier, où origine dans {SEEK_SET, SEEK_CUR, SEEK_END}
int stat(const char* path, struct stat *buf);// récupère l'état du fichier
struct stat {
	dev_t st_dev; // ID du périphérique du fichier
	ino_t st_ino; // Numéro inoeud
	mode_t st_mode; // Protection
	nlink_t st_nlink; // Nombre de liens matériels
	uid_t st_uid; // UID propriétaire
	gid_t st_gid; // GID propriétaire
	dev_t st_rdev; // ID du périphérique si fichier spécial
	off_t st_size; // Taille totale en octets
	blksize_t st_blksize; // Taille de bloc pour E/S
	blkcnt_t st_blocks; // Nombre de blocs alloués
	time_t st_atime; // Heure dernier accès
	time_t st_mtime; // Heure derniere modification
	time_t st_ctime; // Heure dernier changement état
}
```

# Communication inter-processus
## Tubes anonymes et nommés
>[!Define] Signal
>Interruption logicielle délivrée à un processus (voir man 7 signal)

>[!Define] Tube
>Lien entre deux processeurs pour leur permettre de communiquer.
>Moyen de communication entre deux processus s'exécutant sur une même machine.
>Fichier particulier géré par le noyau.
>La lecture est destructrice.

>[!Example]
>```shell
>ls -l | wc -l
>```

Le pipe est une file FIFO.
>[!Warning]
>Possibilité d'effets de bord si un écrit et les messages ne sont pas lus ou l'inverse.
>Blocage si lecture de tube vide ou écriture de tube plein.
>Possibilité d'inter-blocage entre le lecteur et l'écrivain.

Il faut deux tubes si les deux lisent et écrivent.

>[!Define] Tubes anonymes
>- Sans nom,
>- Communication entre deux processus,
>- Deux descripteurs: lecture et écriture,
>- Pointeurs de lecture et d'écriture,
>- Permet la communication dans les deux sens mais un seul à la fois.

Processus de même filiation.
Le fils hérite du tube du père (car le tube est dans la table des descripteurs).
⟶ Communication avec le fils.

### Primitives
```C
#include <unistd.h>
int pipe(int p[2];)
```
pipe crée un tube anonyme et retourne 2 descripteurs placés dans p:
- p[0] pour la lecture,
- p[1] pour l'écriture.
```C
int read(int desc[0], char* buf, int nb); // si rien n'est lu ⟶ 0
int write(int desc[1], char* buf, int nb); // Broken pipe si personne pour lire ou écrire
close(int desc);
```
Le lecteur doit fermer son descripteur en écriture et l'écrivain ferme son processus en lecture pour éviter les blocages.
⟶ **communication unidirectionnelle**
Il vaut mieux utiliser deux tubes pour la communication bi-directionnelle.