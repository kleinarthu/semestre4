**fork** retourne:
- Le PID du fils chez le père,
- 0 chez le fils
- -1 s'il y a une erreur.

# Wait
```C
#include <unistd.h>
int sleep (int secondes);
#include <sys/wait.h>
pid_t wait(int* status);
pid_t waitpid(pid_t pid, int* status, int estNonBloquant);
// pid = -1 pour n'importe lequel et 0 pour n'importe quel élément du groupe
```

# Recouvrement
>[!Example]
>```C
>execl("bin/ls", "ls", "-l", NULL);
>```

## Variables d'environnement
```C
#include <stdlib.h>
char* getenv (const char* variables_environnement);
int putenv(const char* chaine); // 0 si pas d'erreur
```

## Manipulation des fichiers en C
```C
#include <fcntl.h>
int open(const char *nomfich, int mode_ouverture, mode_t droits); // droits uniquement pour création de fichier
close(int desc);
```
| O_RDONLY | O_WRONLY | O_RDWR              | O_NONBLOCK   | O_APPEND               | O_CREAT         | O_TRUNC           | O_EXCL                      |
| -------- | -------- | ------------------- | ------------ | ---------------------- | --------------- | ----------------- | --------------------------- |
| Lecture  | Écriture | Lecture et écriture | Non bloquant | Ajout après le fichier | Crée le fichier | Efface le contenu | Échoue si le fichier existe |
Pour les droits:
- S_I(W|R|X)(USR|GRP|OTH)
```C
#include <unistd.h>
ssize_t read(int desc, void *ptf, size_t nb_octets);
ssize_t write(int desc, void *ptf, size_t nb_octets);
off_t lseek(int desc, off_t offset, int origine); // modifie l'offset par rapport à une origine dans parmis SEEK_(SET|CUR|END)
```

# Communication inter-processus
## Tubes anonymes
>[!Define] Tubes anonymes
>- Communication entre père et fils,
>- Deux descripteurs et pointeurs: lecture et écriture,

### Primitives
```C
#include <unistd.h>
int pipe(int desc[2]);
int read(desc[0], char* buf, int nb);
int write(desc[1], char* buf, int nb);
close(desc[i]);
```

## Duplication de descripteurs
```C
int dup(int desc); // copie desc dans le premier emplacement libre et le renvoie ou -1 pour échec
int dup2(int olddesc, int newdesc); // ferme newdesc et copie olddesc dans newdesc
```

## Tubes nommés
>[!Define] Tube nommé
>Fichier avec un nom, accessible depuis n'importe quel processeur.
```C
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
int mkfifo(const char* nomfichier, mode_t mode);
int unlink(const char* nom_du_tube);
```

## Signaux
>[!Define] Signal
>Interruption logicielle délivrée à un processus. Signal identifié par un entier de 0 à 63, certains non déroutables.

### Envoi de signaux
``` C
#include <signal.h>
int kill(pid_t pid, int signal); //pid = 0 pour tout le groupe
int raise(int signal); // équivalent à kill pour pid = getpid()
int alarm(int seconds); // pour s'envoyer un SIGALARM
```

### Modification du comportement
```C
#include <signal.h>
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
struct sigaction {
	void (*sa_handler) (int signal); // SIG_IGN pour ignorer OU SIG_DFL pour restaurer le comportement par défaut
	void (*sa_sigaction)(int signal, siginfo_t * infos, void* context);
	int sa_flags; // SA_SIGINFO pour utiliser sa_sigaction
}
struct siginfo_t {
	int si_signo; // Numéro de signal
	int si_errno; // Numéro d'erreur
	int si_code; // Code du signal
	pid_t si_pid; // pid de l'expéditeur
	uid_t si_uid; // uid de l'expéditeur
	int si_status; // Valeur de sortie ou signal
}
```

## Files de messages
### Import
```C
// Inter Processus Communication
#include <sys/ipc.h> 
// Sémaphores
#include <sys/shm.h>
// Définit les types par rapport à la machine
#include <sys/types.h>
```

### Commandes terminal
```shell
ipcs # voire les ipc
ipcrm # en effacer un
```

### Structure d'ipc.h
``` C
// Type du message
type desf long mtyp_t;
// Propriétaire du message
struct ipc_perm {
	uid_t uid;
	gid_t gid;
	uid_t cuit;
	git_t uguid;
	mod_t mode; // mode d'accès classique
}
key_t key; // Clé unique pour accéder à une ressource depuis plusieurs processus
// Défini dans le code ou avec la commande suivante:
key_t ftok(const char* ref, int numero);
```

### File de message
>[!Important] Différence avec les tubes.
>Les files de messages sont structurées en paquets.

>[!info] Structure du message
>```C
>#include <msg.h>
>struct msgbuf {
>	long mtype;
>	char mtext[1];
>}
>```

### Utilisation
#### Commandes
```C
int msgget(key_t key, int msgflg);
// key est la clé ipc qui peut être créée avec ftok ou IPC_PRIVATE si IPC_CREAT en second argument.
// msgflg contient les droits d'accès comme nombre octal ou IPC_CREAT ou IPC_EXCL.
int msgctl(int msqid, int cmd, struct mysqid_ds *buf);
// msqid est l'id de la fdm
// cmd est la commande à effectuer parmi IPC_RMID, IPC_STAT, IPC_SET, IPC_INFO, ...
int msgsnd(int mysqid, const void* msgp, size_t msgsz, int msgflg);
// msgflg = IPC_NOWAIT pour lecture non bloquante
ssize_t msgrcv(int mysqid, void* msg, size_t msgsz, long msgtyp, int msgflg);
// msgtyp = 0 ⟹ plus ancien
// msgtyp > 0 ⟹ plus ancien de type donné
// msgtyp < 0 ⟹ plus ancien de type infèrieur ou égal au type donné
```

#### Structures
```C
struct mysqid_ds {
	struct ipc_perm msg_perm; // Appartenance, permissions
	time_t msg_stime; // Temps du dernier msgsnd
	time_t msg_rtime; // Temps du dernier msgrcv
	time_t msg_ctime; // Temps du dernier modif
	unsigned long __msg_cbyte; // Taille du message en octet.
	msgqnum_t msg_qnum; // Nombre de messages.
	msglen_t msg_qbytes; // Taille maximale possible.
	pid_t msg_lspid; // PID du dernier msgsnd
	pid_t msg_lrpid; // PID du dernier msgrcv
}

struct ipc_perm {
	key_t key;
	uid_t uid;
	gid_t gid;
	uid_t cuid;
	gid_t cgid;
	unsigned short mode;
	unsigned short seq;
}
```

# Segments de mémoire partagée
>[!Define] Mémoire partagée
>Zone de **mémoire commune** à plusieurs processus.
>Identificateur SHM (shmid) fourni par le système.
>Données non typées.
>Lecture **non destructrice** (en mémoire jusqu'à ce que le PC soit éteint).
```C
int shmget(key_t cle, int taille, int option); // Crée une zone de mémoire partagée ou accède à une zone existante.
// Renvoie l'identifiant ou -1 et errno parmis EACCESS(droits), EEXIT(identifiant occupé), EIDRM(ipc détruit), EINVAL(paramètres), ...
int shmctl(int shmid, int op, struct shmid_ds *buf); // op parmi IPC_RMID, IPC_STAT, IPC_SET, ...
void* shmat(int shmid, const void* shmadd, int option);// si l'adresse shmadd est null, on va à la première dispo, sinon, si option contient SHM_RND, on attache à l'adresse shmadd%SHMLBA et si option ne contient pas SHM_RND, on attache à shmadd.
void* shmdt(const void* shmadd);
```

# Sockets
>[!Define] TCP (≠UDP)
>Demande autorisation de connection.

## Création
```C
int socket(int af, int type, int protocole); // af parmis AF_INET et AF_UNIX; type parmis SOCK_STREAM, SOCK_DGRAM et SOCK_RAW; protocole parmis IPPROTO_TCP et IPPROTO_UDP

struct sockaddr_in {
	short sin_family; // AF-INET
	ushort sin_port; // n° de port
}
struct hostent {
	char *h_name; // nom de la machine
	char** h_aliases;
	int h_addrtype; // type d'adresse
	int h_length; // Longueur de l'adresse
	char** h_addr_list; // Liste des adresses IP DNS
}
struct hostent* gethostname(char* nom);

int bind(int sock, struct sockaddr_in *p_adresse, int lg); // lg = longueur de la p_adresse
```

## Communication UDP
```C
int sendto(int sock, char* msg, int taille, 0, struct sockaddr_in *p_dest, int taille_p_dest);
int recvfrom(int sock, char* msg, int taille, 0 | MSG_PEEK, struct sockaddr_in* p_exp, int* taille_p_exp);
```

## Communication TCP
```C
int listen(int sock, int nombreConnections);
int accept(int sock, struct sockaddr_in* p_addr, int* taille_adresse);
int connect(int sock, struct sockaddr_in* p_adr, int taille_adr);

int write(int sock, char* msg, int taille);
int read(int sock, char* msg, int taille);
```
```C
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
void serveur() {
	char buf[20]; struct sockaddr_in adr_s, adr_c;
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	socketbzero(&adr_s,sizeof(adr_s));
	adr_s.sin_family = AF_INET; adr_s.sin_port = htons(2058); adr_s.sin_addr.s_addr = htonl(INADDR_ANY);
	bind (sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
	listen(sock, 5);
	int lg = sizeof(adr_c);
	int nsock = accept(sock, (struct sockaddr *) &adr_c, &lg);
	close(sock);
	read(nsock,buf,20);
	close(nsock);
	printf("Message reçu : %s \n", buf);
	exit(0);
}
void client() {
	struct sockaddr_in adr_s, adr_c;
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	bzero(&adr_s,sizeof(adr_s));
	adr_s.sin_family = AF_INET; adr_s.sin_port = htons(2058); adr_s.sin_addr.s_addr = inet_addr("192.168.0.12");
	connect(sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
	write(sock, "bonjour, serveur !!!", 20);
	close(sock);
}
```