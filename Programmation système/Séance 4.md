# Files de messages
3 fonctions:
- files de messages,
- segments de mémoire partagée,
- sémaphores.

Identifiés et manipulés à travers une clé numérique.

## Import
```C
// Inter Processus Communication
#include <sys/ipc.h> 
// Sémaphores
#include <sys/shm.h>
// Définit les types par rapport à la machine
#include <sys/types.h>
```

## Commandes terminal
```shell
ipcs # voire les ipc
ipcrm # en effacer un
```

## Structure d'ipc.h
``` C
// Type du message
type desf long mtyp_t;
// Propriétaire du message
struct ipc_perm {
	uid_t uid;
	gid_t gid;
	uid_t cuit;
	git_t uguid;
	mod_t mode; // mode d'accès classique
}
key_t key; // Clé unique pour accéder à une ressource depuis plusieurs processus
// Défini dans le code ou avec la commande suivante:
key_t ftok(const char* ref, int numero);
```

## File de message

>[!Define] File de messages
>**Message** sous forme de données structurées.

On peut ne lire les messages que d'un certain type.
On accède toujours aux messages avec un ordre FIFO.
L'attente est bloquante.
Lecture destructrice
⟹Permet de communiquer des informations de manière similaires à une boîte au lettre: on dépose le message et quelqu'un viendra le chercher.

>[!Important] Différence avec les tubes.
>Les files de messages sont structurées en paquets.

>[!Define] Identificateur MQ
>L'identificateur des files de messages MQ est un entier fourni par le système à la création.

>[!info] Structure du message
>```C
>#include <msg.h>
>// Commence par un champ de type long qui constitue le type de message.
>// Poursuit avec une suite d'octets consécutifs en mémoire pour le message.
>struct msgbuf {
>	long mtype;
>	char mtext[1];
>}
>```

>[!Warning] Attention
>Pas possible de communiquer des pointeurs.

## Utilisation
### Commandes
```C
int msgget(key_t key, int msgflg);
// key est la clé ipc qui peut être créée avec ftok ou IPC_PRIVATE si IPC_CREAT en second argument.
// msgflg contient les droits d'accès comme nombre octal ou IPC_CREAT ou IPC_EXCL.
int msgctl(int msqid, int cmd, struct mysqid_ds *buf);
// msqid est l'id de la fdm
// cmd est la commande à effectuer parmi IPC_RMID, IPC_STAT, IPC_SET, IPC_INFO, ...
int msgsnd(int mysqid, const void* msgp, size_t msgsz, int msgflg);
// msgflg = IPC_NOWAIT pour lecture non bloquante
ssize_t msgrcv(int mysqid, void* msgsz, size_t msgsz, long msgtyp, int msgflg);
// msgtyp = 0 ⟹ plus ancien
// msgtyp > 0 ⟹ plus ancien de type donné
// msgtyp < 0 ⟹ plus ancien de type infèrieur ou égal au type donné
```

### Structures
```C
struct mysqid_ds {
	struct ipc_perm msg_perm; // Appartenance, permissions
	time_t msg_stime; // Temps du dernier msgsnd
	time_t msg_rtime; // Temps du dernier msgrcv
	time_t msg_ctime; // Temps du dernier modif
	unsigned long __msg_cbyte; // Taille du message en octet.
	msgqnum_t msg_qnum; // Nombre de messages.
	msglen_t msg_qbytes; // Taille maximale possible.
	pid_t msg_lspid; // PID du dernier msgsnd
	pid_t msg_lrpid; // PID du dernier msgrcv
}

struct ipc_perm {
	key_t key;
	uid_t uid;
	gid_t gid;
	uid_t cuid;
	gid_t cgid;
	unsigned short mode;
	unsigned short seq;
}
```