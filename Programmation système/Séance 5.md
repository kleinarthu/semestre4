# Segments de mémoire partagée
Mêmes imports que Files de messages.

>[!Define] Mémoire partagée
>Zone de **mémoire commune** à plusieurs processus.
>Identificateur SHM (shmid) fourni par le système.
>Les processus se rattachent à la zone mémoire.
>Données non typées.
>Lecture **non destructrice** (en mémoire jusqu'à ce que le PC soit éteint).

Accès par pointeurs.

## Accès
```C
int shmget(key_t cle, int taille, int option); // Crée une zone de mémoire partagée ou accède à une zone existante.
// Renvoie l'identifiant ou -1 et errno parmis EACCESS(droits), EEXIT(identifiant occupé), EIDRM(ipc détruit), EINVAL(paramètres), ...
```

## Contrôle
```C
int shmctl(int shmid, int op, struct shmid_ds *buf); // op parmi IPC_RMID, IPC_STAT, IPC_SET, ...
```

## Attachement
```C
void* shmat(int shmid, const void* shmadd, int option);// si l'adresse shmadd est null, on va à la première dispo, sinon, si option contient SHM_RND, on attache à l'adresse shmadd%SHMLBA et si option ne contient pas SHM_RND, on attache à shmadd.
```

## Détachement
```C
void* shmdt(const void* shmadd);
```

>[!Example]
>```C
>#include <sys/types.h>
>#include <sys/ipc.h>
>#include <stdio.h>
>#include <stdlib.h>
>#include <string.h>
>#include <sys/shm.h>
>#define CLE 217 // cle de la SHM
>int main() {
>	char* mem;
>	int shmid;
>	shmid = shmget(
>		(key_t)CLE,
>		1000,
>		IPC_CREAT | 0750
>	);
>	mem = shmat(shmid, NULL, 0);
>	strcpy(mem, "Vive la mémoire partagée");
>	return 0;
>}
>```
>
>```C
>#include <sys/types.h>
>#include <sys/ipc.h>
>#include <stdio.h>
>#include <stdlib.h>
>#include <string.h>
>#include <sys/shm.h>
>#define CLE 217 // cle de la SHM
>int main() {
>	char* mem;
>	int shmid;
>	shmid = shmget(
>		(key_t)CLE,
>		0,
>		0
>	);
>	mem = shmat(shmid, NULL, 0);
>	printf("Lu dans SHM: %s\n", mem);
>	shmdt(mem);
>	shmctl(shmid, IPC_RMID, NULL); // Destruction de la SHM
>	return 0;
>}
>```
