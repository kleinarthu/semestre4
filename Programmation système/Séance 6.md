# Sockets

>[!Define] TCP
>Demande autorisation de connection.

>[!Define] UDP
>Communication directe.

## Création
```C
int socket(int af, int type, int protocole); // af parmis AF_INET et AF_UNIX; type parmis SOCK_STREAM, SOCK_DGRAM et SOCK_RAW; protocole parmis IPPROTO_TCP et IPPROTO_UDP

struct sockaddr_in {
	short sin_family; // AF-INET
	ushort sin_port; // n° de port
}
struct hostent {
	char *h_name; // nom de la machine
	char** h_aliases;
	int h_addrtype; // type d'adresse
	int h_length; // Longueur de l'adresse
	char** h_addr_list; // Liste des adresses IP DNS
}
struct hostent* gethostname(char* nom);

int bind(int sock, struct sockaddr_in *p_adresse, int lg); // lg = longueur de la p_adresse
```

## Communication UDP
```C
int sendto(int sock, char* msg, int taille, 0, struct sockaddr_in *p_dest, int taille_p_dest);
int recvfrom(int sock, char* msg, int taille, 0 | MSG_PEEK, struct sockaddr_in* p_exp, int* taille_p_exp);
```

## Communication TCP
```C
int listen(int sock, int nombreConnections);
int accept(int sock, struct_sockaddr_in* p_addr, int* taille_adresse);
int connect(int sock, struct sockaddr_in* p_adr, int taille_adr);

int write(int sock, char* msg, int taille);
int read(int sock, char* msg, int taille);
```

>[!Example] Serveur
>```C
>#include <stdio.h>
>#include <stdlib.h>
>#include <sys/types.h>
>#include <sys/socket.h>
>#include <netinet/in.h>
>#include <netdb.h>
>#include <string.h>
>#define PORT 2058
>int main() {
>	int sock, nsock, lg, n;
>	pid_t pid;
>	char buf[20];
>	struct sockaddr_in adr_s, adr_c;
>	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
>	socketbzero(&adr_s,sizeof(adr_s));
>	adr_s.sin_family = AF_INET;
>	adr_s.sin_port = htons(PORT);
>	adr_s.sin_addr.s_addr = htonl(INADDR_ANY);
>	bind (sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
>	listen(sock, 5);
>	while (1) {
>		lg = sizeof(adr_c);
>		nsock = accept(sock, (struct sockaddr *) &adr_c, &lg);
>		pid = fork();
>		if (pid==0) {
>			close(sock);
>			read(nsock,buf,20);
>			close(nsock);
>			printf("Message reçu : %s \n", buf);
>			exit(0);
>		}
>	}
>}
>```

>[!Example] Client
>```C
>#include <stdio.h>
>#include <sys/types.h>
>#include <sys/socket.h>
>#include <netinet/in.h>
>#include <netdb.h>
>#include <string.h>
>#define PORT 2058
>int main() {
>	int sock;
>	struct sockaddr_in adr_s, adr_c;
>	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
>	bzero(&adr_s,sizeof(adr_s));
>	adr_s.sin_family = AF_INET;
>	adr_s.sin_port = htons(PORT);
>	adr_s.sin_addr.s_addr = inet_addr("192.168.0.12");
>	connect(sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
>	write(sock, "bonjour, serveur !!!", 20);
>	close(sock);
>}
>```
