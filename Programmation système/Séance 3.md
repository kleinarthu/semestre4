# La table des descripteurs
On peut lier l'entrée du tube à stdout (printf) ou la sortie à stdin (scanf).
On utilise **dup** et **dup2** pour dupliquer les entrées de la table des descripteurs du processus.
⟹Quand on a accès au noyau, on a accès à tout.

``` C
int dup(int desc);
int dup2(int olddesc, int newdesc);
// Renvoient -1 s'ils échouent
// errno contient:
// - EBADF: pas un descripteur valide
// - EMFILE: nombre max de descripteurs atteint
```

## Redirection:
- On crée un tube et on a 2 descripteurs qui pointent sur la table des fichiers.
- On ferme le descripteur 1 ou 2.
- On duplique le 4 ou le 3 et on le recopie dans l'entrée libérée.

>[!Example]
>```C
>#include <stdlib.h>
>#include <stdio.h>
>#include <fcntl.h>
>#include <unistd.h>
>
>int main () {
>	int retour, retour1, old;
>	char buff[5];
>	int fds[2];
>	pipe(fds); old = dup(1);
>	close(1); retour1 = dup(fds[1]); close(fds[1]);
>	close(0); retour0 = dup(fds[0]); close(fds[0]);
>	write(1, "Test\n", 5);
>	read(0, buff, 5);
>	dup2(old, 1);
>	printf("%s", buff);
>	return 0;
>}
>```

# Tubes nommés
>[!Define] Tube nommé
>Fichier avec un nom, accessible depuis n'importe quel processeur.
>Fichier de type p.

>[!Success] Avantages
>Permet la communication entre processus différents.

>[!Failure] Inconvénients
>Plus lent.

```shell
mkfifo nom_fichier
mknod nom_fichier
```

```C
#include <sys/types.h>
#include <sys/stat.h>

int mkfifo(const char* nomfichier, mode_t mode);
// Renvoie 0 si succès et -1 si échec
```
>[!Info]
>```shell
>man 3 mkfifo
>```

Mêmes descripteurs que pour un fichier classique.

```C
unlink(nom_du_tube);
rm(nom_du_tube);
```

>[!Example]
>``` C
>#include <stdio.h>
>#include <stdlib.h>
>#include <unistd.h>
>#include <sys/types.h>
>#include <sys/stat.h>
>#include <fcntl.h>
>int main() { // ecrivain.c
>	mode_t mode; int tub;
>	mode = 0644;
>	mkfifo("fictub", mode);
>	tub = open("fictub", O_WRONLY);
>	write(tub, "0123456789", 10);
>	close(tub);
>	exit(0);
>}
>```
>
>``` C
>#include <stdio.h>
>#include <stdlib.h>
>#include <unistd.h>
>#include <sys/types.h>
>#include <sys/stat.h>
>#include <fcntl.h>
>int main() { // lecteur.c
>	char buf[11]; int tub;
>	tub = open("fictub", O_RDONLY);
>	read(tub, buf, 10);
>	buf[11] = '\0';
>	printf("J'ai lu %s\n", buf);
>	unlink("fictub");
>	exit(0);
>}
>```
>
>```shell
>./ecrivain
>ls -l
>./lecteur
>```

Penser à détruire le fichier.

# Signaux
>[!Define] Signaux
>Information atomique envoyée de manière asynchrone et entre processeurs.

>[!Info]
>```shell
>man 7 signal
>```

>[!Define] Moniteur
>Système qui scrute en permanence l’occurrence des signaux.

Provenance interne si:
- violation mémoire,
- erreur io,
- segmentation fault.

Externe si:
- kill,
- CTRL-C,
- déconnexion du terminal.

Signal identifié paru n entier de 0 à 64.
Tous dans /usr/include/signal.h.

>[!Example]
>- SIGHUP (1): ctrl-d
>- SIGINT (2): ctrl-c
>- SIGKILL(9) *(non déroutable)*
>- SIGALARM(14)
>- SIGCONT(18) *(non déroutable)*
>- SIGSTOP(19) *(non déroutable)*
>- SIGSTP(20): ctrl-z
>- ...

## Envoi de signaux
### Interne
- Erreur d'adressage (eg. SIGSEVG)
- Erreur arithmétique (eg. SIGFPE)
- ...

### Externe
- Ctrl-C (SIGINT)
- Ctrl-\ (SIGQUIT)
- ...

### Dans un programme
``` C
int kill(pid_t pid, int signal); // 0 si succès, -1 sinon
//pid = 0 pour tout le groupe
int raise(int signal); // équivalent à kill pour pid = getpid()
int alarm(int seconds); // pour envoyer un SIGALARM
```
``` shell
kill [-option] pid_processus # option = 15 par défaut
```

## Modification du comportement
>[!Define] Handler
>Définit le comportement pour un signal.

⟹Possibilité de modifier l'action par défaut (eg ctrl-c).

```C
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
// Renvoie 0 si tout s'est bien passé ou -1 en cas d'erreur.
// act contient la nouvelle action si non nul
// oldact contiendra l'ancienne action du signal si non nul
struct sigaction {
	void (*sa_handler) (int);
	// pointeur vers une fonction avec pour paramètre le signal OU SIG_IGN pour ignorer OU SIG_DFL pour restaurer le comportement par défaut
	void (*sa_sigaction)(int, siginfo_t *, void*);
	// pointeur vers une fonction avec en paramètre le signal, des informations, et un contexte rarement utilisé
	sigset_t sa_mask;
	int sa_flags;
	// SA_SIGINFO pour utiliser sa_sigaction
	void (*sa_restorer) (void); // inutil
}

struct siginfo_t {
	int si_signo; // Numéro de signal
	int si_errno; // Numéro d'erreur
	int si_code; // Code du signal
	pid_t si_pid; // pid de l'expéditeur
	uid_t si_uid; // uid de l'expéditeur
	int si_status; // Valeur de sortie ou signal
	//...
}
```

## Réception du signal
Champ signal (entier de 32 bits) dans la table du processus.
⟶ signals pendants
⟶ traitement par le handler
⟹ Gestion par le système par défaut.

Pas d'héritage entre le père et le fils.

## Masquage de signaux
Possibilité de mettre un masque sur les signaux que l'on ne veut pas recevoir ou pas traiter.

## Exemple
``` C
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
static void fun(int sig, siginfo_t *siginfo, void *context) {
	printf("Sending PID: %ld, UID: %ld\n", (long)siginfo->si_pid, (long)siginfo->si_uid);
}
int main(int argc, char** argv) {
	// On affiche les infos du processus après une alarme
	struct sigaction act1;
	act1.sa_sigaction = &fun;
	act1.sa_flags = SA_SIGINFO;
	sigaction(SIGALARM, &act1, NULL);
	// On ignore les signaux de terminaison
	struct sigaction act2;
	act2.sa_sigaction = SIG_IGN;
	sigaction(SIGTERM, &act2, NULL);
	return 0;
}
```

# Multithreading: Thread POSIX
Fork = appel système lourd ⟶ communication lente et partage de données délicat.

⟹ Solution =
>[!Define] Thread
>Processus léger.
>Partie du processus.
>Pile d'exécution indépendante mais même espace de mémoire.

⟹ Partagent tout (sauf les identifiant des threads, la pile, le masque de signal, errno, ...)