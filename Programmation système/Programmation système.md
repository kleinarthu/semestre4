[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progSys)
# Fonctions fournies par l'OS
- gestion des fichiers (ouvrir, fermer, lire, écrire, ...)
- gestion du système de fichiers (créer, supprimer, parcourir les répertoires)
- gestion de la mémoire (allouer, libérer, partager)
- gestion des processus (créer, terminer, arrêter, attendre, signaler, ...)
- gestion des communications entre processus (signaux, BAL, tubes, ...)
- gestion des E/S (écrans, souris, clavier, ...)
- gestion du réseau (sockets)
→ appels standardisés par POSIX sur UNIX et win32 pour Windows

# Rappels: les processus
>[!Define] Processus
>Exécution de programme.
>Peut-être exécuté à plusieurs en même temps.
>**Objectifs**:
>- Séparer les différentes tâches
>- Gérer les fichiers et applications
>- Permettre le multitâche

## Identifiants
PID attribué à chacun.
Au démarrage, processus init reçoit PID = 1.
Création de processus par duplication (fork).
PID du père = PPID du fils
Le père se met en attente à la fin du fils

## Espace d'adressage
- **code**: instructions en langage d'assemblage
- **données**: variables globales ou statiques et allocations dynamiques
- **pile**: appels de fonctions, paramètres et variables locales

# Fork
L'appel de fork demande au système de créer une copie du processus en cours d'exécution.
Ce nouveau processus débute par le retour de la primitive fork.
Dans le processus père, ==fork retourne le PID du processus fils créé==.
Dans le processus fils, ==fork retourne la valeur 0==.
Si le fork échoue, ==fork retourne -1==.

## Caractéristiques héritée par le fils
- UID: identifiant du ou des propriétaires
- GID: identifiant du groupe
- Toutes les valeurs de variables
- Les descripteurs des fichiers ouverts

## Caractéristiques non héritées
- PID
- PPID
- Temps d'exécution (initialisés à zéro)
- Les signaux pendants
- La priorité d'exécution
- Les verrous sur fichiers

## Wait
```C
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int sleep (int secondes);
wait(); // récupére les informations de terminaisons et supprime les processus zombie
pid_t wait(int* status);
pid_t waitpid(pid_t pid, int* status, int option);
// pid = -1 pour n'importe lequel et 0 pour n'importe quel élément du groupe
// option = 0 pour bloquant (informations sur les fils bloqués)
// option = 1 pour non-bloquant: ne pas bloquer si aucun fils ne s'est terminé
// return -1 si erreur (bloquant ou non bloquent)
// return 0 si échec
```
Si status ≠ NULL, informations de terminaison du fils qui peut être analysée par:
- WIFEXITED(status): true si terminaison normale du fils
- WEXITSTATUS(status): renvoie la valeur du fils, si WIFEXITED(status)
- WIFSIGNALED(status): vrai si terminaison du fils à cause d'un signal
- WIFSTOPPED(status): indique si le fils est actuellement arrêté.
- WTERMSIG(status): renvoie le numéro du signal qui a causé l'arrêt du fils. Ne peut être évaluée que si WIFSTOPPED renvoie vrai.

# Recouvrement
```C
int execl(const char* path, const char* arg, ...);
int execlp(const char* file, const char* arg, ...);
int execle(const char* path, const char* arg, ..., char* const envp[]);
int execv(const char* path, char* const argv[]);
int execvp(const char* file, char* const argv[]);
int execvpe(const char* file, char* const argv[], char* const envp[]);
```

>[!Example]
>```C
>#include <stdio.h>
>int main() {
>	execl("bin/ls", "ls", "-l", NULL);
>	printf("Erreur lors de l'appel à ls \n");
>}
>```

# Entrée / Sortie
3 descripteurs de fichiers à la création:
- 0 pour stdin,
- 1 pour stdout,
- 2 pour stderr.
## Variables d'environnement
```C
#include <stdlib.h>
char* getenv (const char* variables_environnement);
int putenv(const char* chaine); // 0 si pas d'erreur
```

## Manipulation des fichiers en C
```C
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int desc;
int open(const char *nomfich, int mode_ouverture, mode_t droits); // mode_t droits optionnel, utilisé uniquement si création de fichier
desc = open(nomfich, mode_ouverture, droits);
close(desc);
```

| mode_ouverture | Mode                                |
| -------------- | ----------------------------------- |
| O_RDONLY       | En lecture                          |
| O_WRONLY       | En écriture                         |
| O_RDWR         | En lecture et en écriture           |
| O_NONBLOCK     | Ouverture dans un mode non bloquant |
| O_APPEND       | Ajout à la fin du fichier           |
| O_CREAT        | Crée le fichier s'il n'existe pas   |
| O_TRUNC        | Remet le fichier à 0 s'il existe    |
| O_EXCL         | Échoue si le fichier existe         |

Pour les droits:
- S_I + {W/R/X} + {USR/GRP/OTH}

```C
#include <unistd.h>
#inclde <sys/types.h>
ssize_t read(int desc, void *ptf, size_t nb_octets);
ssize_t write(int desc, void *ptf, size_t nb_octets);
off_t lseek(int desc, off_t offset, int origine); // modifie l'offset par rapport à une origine dans le fichier, où origine dans {SEEK_SET, SEEK_CUR, SEEK_END}
int stat(const char* path, struct stat *buf);// récupère l'état du fichier
struct stat {
	dev_t st_dev; // ID du périphérique du fichier
	ino_t st_ino; // Numéro inoeud
	mode_t st_mode; // Protection
	nlink_t st_nlink; // Nombre de liens matériels
	uid_t st_uid; // UID propriétaire
	gid_t st_gid; // GID propriétaire
	dev_t st_rdev; // ID du périphérique si fichier spécial
	off_t st_size; // Taille totale en octets
	blksize_t st_blksize; // Taille de bloc pour E/S
	blkcnt_t st_blocks; // Nombre de blocs alloués
	time_t st_atime; // Heure dernier accès
	time_t st_mtime; // Heure derniere modification
	time_t st_ctime; // Heure dernier changement état
}
```

# Communication inter-processus
## Tubes anonymes et nommés
>[!Define] Signal
>Interruption logicielle délivrée à un processus (voir man 7 signal)

>[!Define] Tube
>Moyen de communication entre deux processus s'exécutant sur une même machine.
>Fichier particulier géré par le noyau.
>La lecture est destructrice.

>[!Warning]
>Possibilité d'effets de bord si un écrit et les messages ne sont pas lus ou l'inverse.
>Blocage si lecture de tube vide ou écriture de tube plein.
>Possibilité d'inter-blocage entre le lecteur et l'écrivain.

>[!Define] Tubes anonymes
>- Sans nom,
>- Communication entre deux processus,
>- Deux descripteurs: lecture et écriture,
>- Pointeurs de lecture et d'écriture,
>- Permet la communication dans les deux sens mais un seul à la fois.

Le fils hérite du tube du père (car le tube est dans la table des descripteurs).
⟶ Communication avec le fils.

### Primitives
```C
#include <unistd.h>
int pipe(int p[2];)
```
pipe crée un tube anonyme et retourne 2 descripteurs placés dans p:
- p[0] pour la lecture,
- p[1] pour l'écriture.
```C
int read(desc[0], char* buf, int nb); // si rien n'est lu ⟶ 0
int write(desc[1], char* buf, int nb); // Broken pipe si personne pour lire ou écrire
close(int desc);
```
Le lecteur doit fermer son descripteur en écriture et l'écrivain ferme son processus en lecture pour éviter les blocages.

# La table des descripteurs
## Duplication d'entrée
```C
int dup(int desc);
int dup2(int olddesc, int newdesc);
// Renvoient -1 s'ils échouent
// errno contient:
// - EBADF: pas un descripteur valide
// - EMFILE: nombre max de descripteurs atteint
```

## Redirection:
- On crée un tube et on a 2 descripteurs qui pointent sur la table des fichiers.
- On ferme le descripteur 1 ou 2.
- On duplique le 4 ou le 3 et on le recopie dans l'entrée libérée.

# Tubes nommés
>[!Define] Tube nommé
>Fichier avec un nom, accessible depuis n'importe quel processeur.
>Fichier de type p.

```shell
mkfifo nom_fichier
```

```C
#include <sys/types.h>
#include <sys/stat.h>

int mkfifo(const char* nomfichier, mode_t mode);
// Renvoie 0 si succès et -1 si échec
```

```C
unlink(nom_du_tube);
```

>[!Example]
>```C
>#include <stdio.h>
>#include <stdlib.h>
>#include <unistd.h>
>#include <sys/types.h>
>#include <sys/stat.h>
>#include <fcntl.h>
>int main() { // ecrivain.c
>	mode_t mode; int tub;
>	mode = 0644;
>	mkfifo("fictub", mode);
>	tub = open("fictub", O_WRONLY);
>	write(tub, "0123456789", 10);
>	close(tub);
>	exit(0);
>}
>```
>
>```C
>#include <stdio.h>
>#include <stdlib.h>
>#include <unistd.h>
>#include <sys/types.h>
>#include <sys/stat.h>
>#include <fcntl.h>
>int main() { // lecteur.c
>	char buf[11]; int tub;
>	tub = open("fictub", O_RDONLY);
>	read(tub, buf, 10);
>	buf[11] = '\0';
>	printf("J'ai lu %s\n", buf);
>	unlink("fictub");
>	exit(0);
>}
>```
>
>```shell
>./ecrivain
>ls -l
>./lecteur
>```

# Signaux
>[!Define] Signaux
>Information atomique envoyée de manière asynchrone et entre processeurs.


>[!Info]
>```shell
>man 7 signal
>```

>[!Define] Moniteur
>Système qui scrute en permanence l’occurrence des signaux.

Signal identifié paru n entier de 0 à 64.
Tous dans /usr/include/signal.h.

>[!Example]
>- SIGHUP (1): ctrl-d
>- SIGINT (2): ctrl-c
>- SIGKILL(9) *(non déroutable)*
>- SIGALARM(14)
>- SIGCONT(18) *(non déroutable)*
>- SIGSTOP(19) *(non déroutable)*
>- SIGSTP(20): ctrl-z
>- ...

## Envoi de signaux
``` C
int kill(pid_t pid, int signal); // 0 si succès, -1 sinon
//pid = 0 pour tout le groupe
int raise(int signal); // équivalent à kill pour pid = getpid()
int alarm(int seconds); // pour envoyer un SIGALARM
```
``` shell
kill [-option] pid_processus # option = 15 par défaut
```

## Modification du comportement
>[!Define] Handler
>Définit le comportement pour un signal.

```C
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
// Renvoie 0 si tout s'est bien passé ou -1 en cas d'erreur.
// act contient la nouvelle action si non nul
// oldact contiendra l'ancienne action du signal si non nul
struct sigaction {
	void (*sa_handler) (int);
	// pointeur vers une fonction avec pour paramètre le signal OU SIG_IGN pour ignorer OU SIG_DFL pour restaurer le comportement par défaut
	void (*sa_sigaction)(int, siginfo_t *, void*);
	// pointeur vers une fonction avec en paramètre le signal, des informations, et un contexte rarement utilisé
	sigset_t sa_mask;
	int sa_flags;
	// SA_SIGINFO pour utiliser sa_sigaction
	void (*sa_restorer) (void); // inutil
}

struct siginfo_t {
	int si_signo; // Numéro de signal
	int si_errno; // Numéro d'erreur
	int si_code; // Code du signal
	pid_t si_pid; // pid de l'expéditeur
	uid_t si_uid; // uid de l'expéditeur
	int si_status; // Valeur de sortie ou signal
	//...
}
```

## Exemple
``` C
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
static void fun(int sig, siginfo_t *siginfo, void *context) {
	printf("Sending PID: %ld, UID: %ld\n", (long)siginfo->si_pid, (long)siginfo->si_uid);
}
int main(int argc, char** argv) {
	// On affiche les infos du processus après une alarme
	struct sigaction act1;
	act1.sa_sigaction = &fun;
	act1.sa_flags = SA_SIGINFO;
	sigaction(SIGALARM, &act1, NULL);
	// On ignore les signaux de terminaison
	struct sigaction act2;
	act2.sa_sigaction = SIG_IGN;
	sigaction(SIGTERM, &act2, NULL);
	return 0;
}
```

# Multithreading: Thread POSIX
Fork = appel système lourd ⟶ communication lente et partage de données délicat.

⟹ Solution =
>[!Define] Thread
>Processus léger.
>Partie du processus.
>Pile d'exécution indépendante mais même espace de mémoire.

# Files de messages
## Import
```C
// Inter Processus Communication
#include <sys/ipc.h> 
// Sémaphores
#include <sys/shm.h>
// Définit les types par rapport à la machine
#include <sys/types.h>
```

## Commandes terminal
```shell
ipcs # voire les ipc
ipcrm # en effacer un
```

## Structure d'ipc.h
``` C
// Type du message
type desf long mtyp_t;
// Propriétaire du message
struct ipc_perm {
	uid_t uid;
	gid_t gid;
	uid_t cuit;
	git_t uguid;
	mod_t mode; // mode d'accès classique
}
key_t key; // Clé unique pour accéder à une ressource depuis plusieurs processus
// Défini dans le code ou avec la commande suivante:
key_t ftok(const char* ref, int numero);
```

## File de message
>[!Important] Différence avec les tubes.
>Les files de messages sont structurées en paquets.

>[!info] Structure du message
>```C
>#include <msg.h>
>struct msgbuf {
>	long mtype;
>	char mtext[1];
>}
>```

## Utilisation
### Commandes
```C
int msgget(key_t key, int msgflg);
// key est la clé ipc qui peut être créée avec ftok ou IPC_PRIVATE si IPC_CREAT en second argument.
// msgflg contient les droits d'accès comme nombre octal ou IPC_CREAT ou IPC_EXCL.
int msgctl(int msqid, int cmd, struct mysqid_ds *buf);
// msqid est l'id de la fdm
// cmd est la commande à effectuer parmi IPC_RMID, IPC_STAT, IPC_SET, IPC_INFO, ...
int msgsnd(int mysqid, const void* msgp, size_t msgsz, int msgflg);
// msgflg = IPC_NOWAIT pour lecture non bloquante
ssize_t msgrcv(int mysqid, void* msgsz, size_t msgsz, long msgtyp, int msgflg);
// msgtyp = 0 ⟹ plus ancien
// msgtyp > 0 ⟹ plus ancien de type donné
// msgtyp < 0 ⟹ plus ancien de type infèrieur ou égal au type donné
```

### Structures
```C
struct mysqid_ds {
	struct ipc_perm msg_perm; // Appartenance, permissions
	time_t msg_stime; // Temps du dernier msgsnd
	time_t msg_rtime; // Temps du dernier msgrcv
	time_t msg_ctime; // Temps du dernier modif
	unsigned long __msg_cbyte; // Taille du message en octet.
	msgqnum_t msg_qnum; // Nombre de messages.
	msglen_t msg_qbytes; // Taille maximale possible.
	pid_t msg_lspid; // PID du dernier msgsnd
	pid_t msg_lrpid; // PID du dernier msgrcv
}

struct ipc_perm {
	key_t key;
	uid_t uid;
	gid_t gid;
	uid_t cuid;
	gid_t cgid;
	unsigned short mode;
	unsigned short seq;
}
```

# Segments de mémoire partagée
>[!Define] Mémoire partagée
>Zone de **mémoire commune** à plusieurs processus.
>Identificateur SHM (shmid) fourni par le système.
>Données non typées.
>Lecture **non destructrice** (en mémoire jusqu'à ce que le PC soit éteint).

## Accès
```C
int shmget(key_t cle, int taille, int option); // Crée une zone de mémoire partagée ou accède à une zone existante.
// Renvoie l'identifiant ou -1 et errno parmis EACCESS(droits), EEXIT(identifiant occupé), EIDRM(ipc détruit), EINVAL(paramètres), ...
```

## Contrôle
```C
int shmctl(int shmid, int op, struct shmid_ds *buf); // op parmi IPC_RMID, IPC_STAT, IPC_SET, ...
```

## Attachement
```C
void* shmat(int shmid, const void* shmadd, int option);// si l'adresse shmadd est null, on va à la première dispo, sinon, si option contient SHM_RND, on attache à l'adresse shmadd%SHMLBA et si option ne contient pas SHM_RND, on attache à shmadd.
```

## Détachement
```C
void* shmdt(const void* shmadd);
```

# Sockets

>[!Define] TCP
>Demande autorisation de connection.

>[!Define] UDP
>Communication directe.

## Création
```C
int socket(int af, int type, int protocole); // af parmis AF_INET et AF_UNIX; type parmis SOCK_STREAM, SOCK_DGRAM et SOCK_RAW; protocole parmis IPPROTO_TCP et IPPROTO_UDP

struct sockaddr_in {
	short sin_family; // AF-INET
	ushort sin_port; // n° de port
}
struct hostent {
	char *h_name; // nom de la machine
	char** h_aliases;
	int h_addrtype; // type d'adresse
	int h_length; // Longueur de l'adresse
	char** h_addr_list; // Liste des adresses IP DNS
}
struct hostent* gethostname(char* nom);

int bind(int sock, struct sockaddr_in *p_adresse, int lg); // lg = longueur de la p_adresse
```

## Communication UDP
```C
int sendto(int sock, char* msg, int taille, 0, struct sockaddr_in *p_dest, int taille_p_dest);
int recvfrom(int sock, char* msg, int taille, 0 | MSG_PEEK, struct sockaddr_in* p_exp, int* taille_p_exp);
```

## Communication TCP
```C
int listen(int sock, int nombreConnections);
int accept(int sock, struct_sockaddr_in* p_addr, int* taille_adresse);
int connect(int sock, struct sockaddr_in* p_adr, int taille_adr);

int write(int sock, char* msg, int taille);
int read(int sock, char* msg, int taille);
```

>[!Example] Serveur
>```C
>#include <stdio.h>
>#include <stdlib.h>
>#include <sys/types.h>
>#include <sys/socket.h>
>#include <netinet/in.h>
>#include <netdb.h>
>#include <string.h>
>#define PORT 2058
>int main() {
>	int sock, nsock, lg, n;
>	pid_t pid;
>	char buf[20];
>	struct sockaddr_in adr_s, adr_c;
>	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
>	socketbzero(&adr_s,sizeof(adr_s));
>	adr_s.sin_family = AF_INET;
>	adr_s.sin_port = htons(PORT);
>	adr_s.sin_addr.s_addr = htonl(INADDR_ANY);
>	bind (sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
>	listen(sock, 5);
>	while (1) {
>		lg = sizeof(adr_c);
>		nsock = accept(sock, (struct sockaddr *) &adr_c, &lg);
>		pid = fork();
>		if (pid==0) {
>			close(sock);
>			read(nsock,buf,20);
>			close(nsock);
>			printf("Message reçu : %s \n", buf);
>			exit(0);
>		}
>	}
>}
>```

>[!Example] Client
>```C
>#include <stdio.h>
>#include <sys/types.h>
>#include <sys/socket.h>
>#include <netinet/in.h>
>#include <netdb.h>
>#include <string.h>
>#define PORT 2058
>int main() {
>	int sock;
>	struct sockaddr_in adr_s, adr_c;
>	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
>	bzero(&adr_s,sizeof(adr_s));
>	adr_s.sin_family = AF_INET;
>	adr_s.sin_port = htons(PORT);
>	adr_s.sin_addr.s_addr = inet_addr("192.168.0.12");
>	connect(sock, (struct sockaddr *) &adr_s, sizeof(adr_s));
>	write(sock, "bonjour, serveur !!!", 20);
>	close(sock);
>}
>```