>[!Define] Programmation Système
>Développement de programmes qui font partie du système d'exploitation d'un ordinateur, qui en réalisent les fonctions et qui utilisent les fonctions avancées de celui-ci.
>>[!Exemple]
>>- La gestion des processus
>>- L'accès aux fichiers,
>>- La programmation réseau, les entrées/sorties, la gestion de la mémoire

>[!Question] Comment créer des applications qui communiquent avec l'OS ?

On utilisera Linux car:
- concepts généraux
- noyaux open source
- interface normalisés (POSIX)
- noyau en C

# Rôle de l'OS
Gérer:
- la mémoire
- les E/S
- les fichiers
- les utilisateurs
- les processus

# Outils de l'OS
- la mémoire virtuelle
- les ITs pour les E/S et les processus
- le contexte (registre et environnement)
- les droits pour les utilisateurs et les fichiers

# Fonctions fournies par l'OS
- gestion des fichiers (ouvrir, fermer, lire, écrire, ...)
- gestion du système de fichiers (créer, supprimer, parcourir les répertoires)
- gestion de la mémoire (allouer, libérer, partager)
- gestion des processus (créer, terminer, arrêter, attendre, signaler, ...)
- gestion des communications entre processus (signaux, BAL, tubes, ...)
- gestion des E/S (écrans, souris, clavier, ...)
- gestion du réseau (sockets)
→ appels standardisés par POSIX sur UNIX et win32 pour Windows

# Structure du SE
![[ArchitectureSE.png]]

# Rappels: les processus
>[!Define] Processus
>Exécution de programme.
>Peut-être exécuté à plusieurs en même temps.
>**Objectifs**:
>- Séparer les différentes tâches
>- Gérer les fichiers et applications
>- Permettre le multitâche

Processeur gère plusieurs processus en attente et donne du temps d'exécution à chacun
Processus conserve son contexte pour reprendre l'exécution en cours

## Commandes shell
```shell
pstree # affiche l'arborescence des processus
ps # donne la liste des processus actifs selon certains critères
```


## Identifiants
PID attribué à chacun.
Au démarrage, processus init reçoit PID = 1.
Création de processus par duplication (fork).
PID du père = PPID du fils
Le père se met en attente à la fin du fils

## Espace d'adressage
- **code**: instructions en langage d'assemblage
- **données**: variables globales ou statiques et allocations dynamiques
- **pile**: appels de fonctions, paramètres et variables locales

## Déroulement
1. Appel système fork par le père. Le processus père continue son exécution en arrière plan.
2. Appel système exec par le processus fils.
3. Déroulement du programme.
4. Fin du programme, envoi du code retour (exit)
5. Récupération du processus fils à l'état zombie par le processus père.
6. Si le processus père a fini son exécution avant le processus fils, le processus fils à l'état zombie est "rattaché" au processus originel. Son PPID est alors de PID de init.
7. Tout processus Linux qui se termine possèdent une valeur retour appelée code de retour (exit status) à laquelle le père peut accéder.

# Fork
L'appel de fork demande au système de créer une copie du processus en cours d'exécution.
Ce nouveau processus débute par le retour de la primitive fork.
Dans le processus père, ==fork retourne le PID du processus fils créé==.
Dans le processus fils, ==fork retourne la valeur 0==.
Si le fork échoue, ==fork retourne -1==.

## Caractéristiques héritée par le fils
- UID: identifiant du ou des propriétaires
- GID: identifiant du groupe
- Toutes les valeurs de variables
- Les descripteurs des fichiers ouverts

## Caractéristiques non héritées
- PID
- PPID
- Temps d'exécution (initialisés à zéro)
- Les signaux pendants
- La priorité d'exécution
- Les verrous sur fichiers

```C
#include <stdlib.h>

getpid(); // pid
getppid(); // ppid
char* getcwd(char* buf, size_t taille); // répertoire de travail dans buf avec taille taille

exit();
void exit(int status);

wait(); // récupére les informations de terminaisons et supprime les processus zombie
pid_t wait(int* status);
```
Si on a pas de fils, wait retourne tout de suite -1.
```C
#include <sys/types.h>
#includes <sys/wait.h>
pid_t waitpid(pid_t pid, int* status, int option);
// pid = -1 pour n'importe lequel et 0 pour n'importe quel élément du groupe
// option = 0 pour bloquant (informations sur les fils bloqués)
// option = 1 pour non-bloquant: ne pas bloquer si aucun fils ne s'est terminé
// return -1 si erreur (bloquant ou non bloquent)
// return 0 si échec
```
Si status ≠ NULL, informations de terminaison du fils qui peut être analysée par:
- WIFEXITED(status): true si terminaison normale du fils
- WEXITSTATUS(status): renvoie la valeur du fils, si WIFEXITED(status)
- WIFSIGNALED(status): vrai si terminaison du fils à cause d'un signal
- WIFSTOPPED(status): indique si le fils est actuellement arrêté.
- WTERMSIG(status): renvoie le numéro du signal qui a causé l'arrêt du fils. Ne peut être évaluée que si WIFSTOPPED renvoie vrai.
>[!info]
>```shell
>man 2 wait

```shell
#include <unistd.h>
int sleep (int secondes);
```