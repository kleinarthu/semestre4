# Héritage
>[!Define] Héritage
>A hérite de B ⟺ A est un B
>A est la **classe dérivée**.
>B est la **classe de base ou classe parente**.
>La classe dérivée hérite tous les membres de la classe de base.

## Implémentation
>[!Exemple]
>```cpp
>class Personne {
>	private:
>		string nom;
>	public:
>		string getNom() const;
>};
>
>class Etudiant: public Personne {
>	private:
>		string id;
>	public:
>		string getId()  const;
>};
>```

## Redéfinition
Il est possible de **redéfinir** l'implémentation d'une fonction dans la classe dérivée.
>[!Exemple]
>```cpp
>void Etudiant:afficher() const {
>	Personne::afficher();
>	std::cout << id << endl;
>}
>```

## Types d'héritages
```cpp
class Derivee: <mode de dérivation> Parent {
};
```

| Mode \\ Dans Parent | public    | private       | protected |
| ------------------- | --------- | ------------- | --------- |
| public              | public    | inaccessible  | protected |
| private             | private   | inaccessible  | private   |
| protected           | protected | inaccessible | protected |

L'héritage privée n'est pas un vrai héritage et est uniquement un outil pour réutiliser le code.

## Polymorphisme
>[!Define] Polymorphisme
>Capacité du système à **choisir dynamiquement** la méthode qui correspond au type de l'objet en cours de manipulation.

En Cpp, le mot-clé **virtual** dans la déclaration d'une méthode de la classe parente permet de **déléguer l'implémentation** d'une méthode à la classe fille.

## Classe abstraite
>[!Define] Classe abstraite
>Classe contenant au moins une fonction virtuelle pure.

>[!Define] Fonction virtuelle pure
>```cpp
>virtual type nom(parametres) = 0;
>```

Les classes abstraites ne peuvent pas être directement implémentées.

# Exceptions

## Définition
```cpp
#include <exception>
class MonException: public exception {
	virtual const char* what() const throw() {
	return "Oups! Mon exception ...";
	}
}
```

## Utilisation
>[!Example]
>```cpp
>throw 0;
>throw string("Erreur de calcul");
>throw MonException();
>```

## Récupération
```cpp
try {
	// code pouvant ammener une exception
} catch (int code) {
	cerr << "Exception" << code << endl;
} catch (exception& e) {
	cout << e.what() << endl;
}
```

## Exceptions pré-définies:
### Exceptions de logique
- domain_error
- invalid_argument
- length_error
- out_of_range
- logic_error

### Exceptions d'exécution
- range_error
- overflow_error
- runtime_error