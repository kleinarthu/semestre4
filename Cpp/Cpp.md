[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_cpp)
# Introduction
Par rapport à java:
- plus efficace,
- plus de confiance à l'utilisateur,
- ...
>[!Info] Références bibliographiques
>Bjarne Stroustrup, [*A tour of C++*](https://www.stroustrup.com/tour2.html)

>[!Info] Style guide
>[Google c++ style guide](https://google.github.io/styleguide/cppguide.html)

# Conversion d'un code en C en C++
- **const** pour créer des constantes,
- **inline** pour les fonctions en une ligne, remplacer à la compilation (⟺ macros),
- **namespace** pour créer un espace de nommage (encapsulation).
## Sorties
```Cpp
std::cout << "texte" << std::endl;
```

## Entrées
```Cpp
int trials;
cin >> trials;
```

## Conversion
```Cpp
static_cast<double>(valeur);
```

## Template
```Cpp
template <typename T>
inline void swap(T &i, T &j) {
	T tmp = i;
	i = j;
	j = tmp;
}
```

## Compilation
- fichiers d'implémentation: file.cpp
- fichiers de déclaration: file.hpp
```shell
g++ -Wall -std=c++14 -c file.cpp
g++ -o prog file1.o file2.o
```

## Classes
>[!Example] Light.hpp
>```Cpp
>#ifndef __LIGHT_HPP_
>#define __LIGHT_HPP_
>class Light {
>private :
>	bool on;
>public :
>	Light();
>	void toggle();
>	bool isOn();
>}
>#endif
>```

>[!Example] Light.cpp
>```Cpp
>#include "Light.hpp"
>Light::Light() {
>	on = false;
>}
>
> void Light::toggle() {
>	on = (!on);
> }
>
>bool Light::isOn() {
>	return (on);
>}
>```

# Constructeurs, Destructeurs, Allocation dynamique
## Constructeur
>[!Define] Constructeur
>Fonction qui a le même  nom que la classe, sans type de retour et qui permet d'**initialiser un objet**.

```Cpp
Fraction::Fraction(int n, int d): numerateur{n} denominateur{d} {
}
// Équivalent
Fraction::Fraction(int n, int d) {
	numerateur = n;
	denominateur = d;
}
```
Mot-clé **new**.

## Destructeur
>[!Define] Destructeur
>**Libère l'espace** occupé par l'objet.
>Appelée automatiquement lorsque l'objet est détruit.

```Cpp
Fraction::~Fraction() {
}
```
Mot-clé **delete**.
>[!Warning] Attention
>Penser à libérer le contenu avant de libérer le conteneur.

## Copie
```Cpp
Fraction f1(5, 6);
Fraction f2 {f1};
```

## Fonction amie
>[!Define] Fonction amie
>Donne accès aux attributs ou méthodes privés à une méthode non-membre.

Mot-clé **friend**.

# Surcharge des opérateurs
But: augmenter la lisibilité.

## Opérateurs
Pas possible de modifier les priorités.
### Arithmétiques
- operator+,
- operator-,
- operator*,
- ...

### Comparaison
- operator\=\=,
- operator<,
- operator>\=,
- ...

### Raccourcis
- operator+=,
- operator/=,
- operator++,
- ...

### Autres
- operator<<,
- operator\[\],
- operator->,
- ...

## Redéfinition
```Cpp
// Dans la classe
void operator*= (const Fraction& autre);
void operator* (const Fraction& autre) const;

void Fraction::operator*= (const Fraction& autre) {
	numerateur *= autre.numerateur;
	denominateur *= autre.denominateur;
}

Fraction Fraction::operator* (const Fraction& autre) const {
	return Fraction(numerateur * autre.numerateur, denominateur * autre.denominateur);
}

// Hors classe
Fraction operator* (const Fraction& f1, const Fraction& f2);

Fraction operator* (const Fraction& f1, const Fraction& f2) {
	return Fraction(f1.getNum() * f2.getNum(), f1.getDenom() * f2.getDenom());
}
```

# Héritage
>[!Define] Héritage
>A hérite de B ⟺ A est un B
>A est la **classe dérivée**.
>B est la **classe de base ou classe parente**.
>La classe dérivée hérite tous les membres de la classe de base.

## Implémentation
>[!Exemple]
>```cpp
>class Personne {
>	private:
>		string nom;
>	public:
>		string getNom() const;
>};
>
>class Etudiant: public Personne {
>	private:
>		string id;
>	public:
>		string getId()  const;
>};
>```

## Redéfinition
Il est possible de **redéfinir** l'implémentation d'une fonction dans la classe dérivée.
>[!Exemple]
>```cpp
>void Etudiant:afficher() const {
>	Personne::afficher();
>	std::cout << id << endl;
>}
>```

## Types d'héritages
```cpp
class Derivee: <mode de dérivation> Parent {
};
```

| Mode \\ Dans Parent | public    | private       | protected |
| ------------------- | --------- | ------------- | --------- |
| public              | public    | inaccessible  | protected |
| private             | private   | inaccessible  | private   |
| protected           | protected | inaccessible | protected |

L'héritage privée n'est pas un vrai héritage et est uniquement un outil pour réutiliser le code.

## Polymorphisme
>[!Define] Polymorphisme
>Capacité du système à **choisir dynamiquement** la méthode qui correspond au type de l'objet en cours de manipulation.

En Cpp, le mot-clé **virtual** dans la déclaration d'une méthode de la classe parente permet de **déléguer l'implémentation** d'une méthode à la classe fille.

## Classe abstraite
>[!Define] Classe abstraite
>Classe contenant au moins une fonction virtuelle pure.

>[!Define] Fonction virtuelle pure
>```cpp
>virtual type nom(parametres) = 0;
>```

Les classes abstraites ne peuvent pas être directement implémentées.

# Exceptions

## Définition
```cpp
#include <exception>
class MonException: public exception {
	virtual const char* what() const throw() {
	return "Oups! Mon exception ...";
	}
}
```

## Utilisation
>[!Example]
>```cpp
>throw 0;
>throw string("Erreur de calcul");
>throw MonException();
>```

## Récupération
```cpp
try {
	// code pouvant ammener une exception
} catch (int code) {
	cerr << "Exception" << code << endl;
} catch (exception& e) {
	cout << e.what() << endl;
}
```

## Exceptions pré-définies:
### Exceptions de logique
- domain_error
- invalid_argument
- length_error
- out_of_range
- logic_error

### Exceptions d'exécution
- range_error
- overflow_error
- runtime_error

# Gestion des entrées sorties
## Flux standards:
- cin (entrées),
- cout (sorties),
- cerr (erreurs).

Avec pour **opérateurs**:
- << (vers sortie),
- >> (vers entrée).

>[!Warning] Attention
>Gérer les erreurs de types lors de la définition de l'opérateur >>.
>>[!Exemple]
>>```Cpp
>>if (!(in >> f.numerateur >> c >> f.denominateur) || (c != '\')) {
>>	in.setstate(ios::failbit);
>>}
>>```

## Flux physiques
```Cpp
#include <fstream>
```
- ofstream,
- ifstream,
- fstream.

### Utilisation
```Cpp
ifstream nomFlux("chemin"[, mode]);
ofstream nomFlux("chemin"[, mode]);
nomFlux.close;
```

### Modes d'ouverture
- ios::in (input, défaut pour ifstream),
- ios::out (output, défaut pour ofstream),
- ios::app (append),
- ios::ate (at the end),
- ios::binary (binary),
- ios::trunc (truncate).

### Lecture par ligne
```Cpp
bool getline(ifstream& is, string& line); // true iff there are other lines
```

### État de la lecture
- **bad** est true ssi une opération échoue,
- **fail** est identique à bad mais renvoie faux pour les erreurs de format,
- **eof** est true ssi on est à la fin du fichier,
- **good** est l'inverse des précédents.

### Position dans un fichier
- **tellg** pour ifstream,
- **tellp** pour ofstream.

### Déplacement
```Cpp
seekg(pos, mode);
seekp(pos, mode);
// où pos est le nombre d'octet par rapport à mode parmis ios::beg, ios::cur et ios::end
```

# Template
>[!Define] Template
>Permet à une fonction ou classe d'utiliser des types différents.

>[!Example]
>``` Cpp
>template <typename T\>
>T mini(const T& a, const T& b) {
>	return (a < b ? a : b);
>}
>mini<int\>(i1, i2);
>```

>[!Warning] Attention
>À implémenter dans le .hpp.

# STL
>[!Define] STL
>Librairie très efficace contenant des algorithmes et structures de données fondamentaux.

## Conteneurs
- **Séquentiels**: vector, list(push_front(), pop_front, remove), stack, ...
- **Associatifs**: set, multiset, map, multimap, ...

## Iterator
```Cpp
for (vector<int>::iterator it = myvector.begin(); it != myvector.end(); ++it) {}
for (auto it = myvector.begin(); it != myvector.end(); ++it) {}
```

>[!Define] Algorithme
>Fonction template utilisée avec des itérateurs.

>[!Define] Foncteur
>Objet surchargeant **opertator()** pour être utilisé comme paramètre.

>[!Example]
>```Cpp
>template <typename T\>
>class PlusGrand {
>	public:
>		bool operator() (const T& x, conts T& y) const {
>			return x > y;
>		}
>}
>int numbers[] = {20, 40, 50, 10, 30};
>sort(numbers, numbers + 5, plusGrand<int\>());

### for_each
>[!Example]
>```cpp
>for_each(v.begin(), v.end(), [](int i) {cout << i << endl;});