# Surcharge des opérateurs
But: augmenter la lisibilité.

## Opérateurs
Pas possible de modifier les priorités.
### Arithmétiques
- operator+,
- operator-,
- operator*,
- ...

### Comparaison
- operator\=\=,
- operator<,
- operator>\=,
- ...

### Raccourcis
- operator+=,
- operator/=,
- operator++,
- ...

### Autres
- operator<<,
- operator\[\],
- operator->,
- ...

## Redéfinition
```Cpp
// Dans la classe
void operator*= (const Fraction& autre);
void operator* (const Fraction& autre) const;

void Fraction::operator*= (const Fraction& autre) {
	numerateur *= autre.numerateur;
	denominateur *= autre.denominateur;
}

Fraction Fraction::operator* (const Fraction& autre) const {
	return Fraction(numerateur * autre.numerateur, denominateur * autre.denominateur);
}

// Hors classe
Fraction operator* (const Fraction& f1, const Fraction& f2);

Fraction operator* (const Fraction& f1, const Fraction& f2) {
	return Fraction(f1.getNum() * f2.getNum(), f1.getDenom() * f2.getDenom());
}
```