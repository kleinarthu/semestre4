# Constructeurs, Destructeurs, Allocation dynamique
## Constructeur
>[!Define] Constructeur
>Fonction qui a le même  nom que la classe, sans type de retour et qui permet d'**initialiser un objet**.

```Cpp
Fraction::Fraction(int n, int d): numerateur{n} denominateur{d} {
}
// Équivalent
Fraction::Fraction(int n, int d) {
	numerateur = n;
	denominateur = d;
}
```
Mot-clé **new**.

## Destructeur
>[!Define] Destructeur
>**Libère l'espace** occupé par l'objet.
>Appelée automatiquement lorsque l'objet est détruit.

```Cpp
Fraction::~Fraction() {
}
```
Mot-clé **delete**.
>[!Warning] Attention
>Penser à libérer le contenu avant de libérer le conteneur.

## Copie
```Cpp
Fraction f1(5, 6);
Fraction f2 {f1};
```

## Fonction amie
>[!Define] Fonction amie
>Donne accès aux attributs ou méthodes privés à une méthode non-membre.

Mot-clé **friend**.