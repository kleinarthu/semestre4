# Gestion des entrées sorties
## Flux standards:
- cin (entrées),
- cout (sorties),
- cerr (erreurs).

Avec pour **opérateurs**:
- << (vers sortie),
- >> (vers entrée).

>[!Warning] Attention
>Gérer les erreurs de types lors de la définition de l'opérateur >>.
>>[!Exemple]
>>```Cpp
>>if (!(in >> f.numerateur >> c >> f.denominateur) || (c != '\')) {
>>	in.setstate(ios::failbit);
>>}
>>```

## Flux physiques
```Cpp
#include <fstream>
```
- ofstream,
- ifstream,
- fstream.

### Utilisation
```Cpp
ifstream nomFlux("chemin"[, mode]);
ofstream nomFlux("chemin"[, mode]);
nomFlux.close;
```

### Modes d'ouverture
- ios::in (input, défaut pour ifstream),
- ios::out (output, défaut pour ofstream),
- ios::app (append),
- ios::ate (at the end),
- ios::binary (binary),
- ios::trunc (truncate).

### Lecture par ligne
```Cpp
bool getline(ifstream& is, string& line); // true iff there are other lines
```

### État de la lecture
- **bad** est true ssi une opération échoue,
- **fail** est identique à bad mais renvoie faux pour les erreurs de format,
- **eof** est true ssi on est à la fin du fichier,
- **good** est l'inverse des précédents.

### Position dans un fichier
- **tellg** pour ifstream,
- **tellp** pour ofstream.

### Déplacement
```Cpp
seekg(pos, mode);
seekp(pos, mode);
// où pos est le nombre d'octet par rapport à mode parmis ios::beg, ios::cur et ios::end
```