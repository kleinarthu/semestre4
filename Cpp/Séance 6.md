# Template
>[!Define] Template
>Permet à une fonction ou classe d'utiliser des types différents.

>[!Example]
>``` Cpp
>template <typename T\>
>T mini(const T& a, const T& b) {
>	return (a < b ? a : b);
>}
>mini<int\>(i1, i2);
>```

>[!Warning] Attention
>À implémenter dans le .hpp.

# STL
>[!Define] STL
>Librairie très efficace contenant des algorithmes et structures de données fondamentaux.

## Conteneurs
- **Séquentiels**: vector, list(push_front(), pop_front, remove), stack, ...
- **Associatifs**: set, multiset, map, multimap, ...

## Iterator
```Cpp
for (vector<int>::iterator it = myvector.begin(); it != myvector.end(); ++it) {}
for (auto it = myvector.begin(); it != myvector.end(); ++it) {}
```

>[!Define] Algorithme
>Fonction template utilisée avec des itérateurs.

>[!Define] Foncteur
>Objet surchargeant **opertator()** pour être utilisé comme paramètre.

>[!Example]
>```Cpp
>template <typename T\>
>class PlusGrand {
>	public:
>		bool operator() (const T& x, conts T& y) const {
>			return x > y;
>		}
>}
>int numbers[] = {20, 40, 50, 10, 30};
>sort(numbers, numbers + 5, plusGrand<int\>());

### for_each
>[!Example]
>```cpp
>for_each(v.begin(), v.end(), [](int i) {cout << i << endl;});