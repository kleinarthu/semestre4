[Voir les TPs](https://gitlab.etude.cy-tech.fr/kleinarthu/td_cpp)
**Déroulement:** 6 séances de 3h (30min de cours + 2h30 de TP).
**Évaluation**: TP notés + examen
⟹ *envoyer les TPs dans la journée*
# Introduction
Par rapport à java:
- plus efficace,
- plus de confiance à l'utilisateur,
- ...
>[!Info] Références bibliographiques
>Bjarne Stroustrup, [*A tour of C++*](https://www.stroustrup.com/tour2.html)

>[!Info] Style guide
>[Google c++ style guide](https://google.github.io/styleguide/cppguide.html)

# Conversion d'un code en C en C++
- **const** pour créer des constantes,
- **inline** pour les fonctions en une ligne, remplacer à la compilation (⟺ macros),
- **namespace** pour créer un espace de nommage (encapsulation).
## Sorties
```Cpp
std::cout << "texte" << std::endl;
```

## Entrées
```Cpp
int trials;
cin >> trials;
```

## Conversion
```Cpp
static_cast<double>(valeur);
```

## Template
```Cpp
template <typename T>
inline void swap(T &i, T &j) {
	T tmp = i;
	i = j;
	j = tmp;
}
```

## Compilation
- fichiers d'implémentation: file.cpp
- fichiers de déclaration: file.hpp
```shell
g++ -Wall -std=c++14 -c file.cpp
g++ -o prog file1.o file2.o
```

## Classes
>[!Example] Light.hpp
>```Cpp
>#ifndef __LIGHT_HPP_
>#define __LIGHT_HPP_
>class Light {
>private :
>	bool on;
>public :
>	Light();
>	void toggle();
>	bool isOn();
>}
>#endif
>```

>[!Example] Light.cpp
>```Cpp
>#include "Light.hpp"
>Light::Light() {
>	on = false;
>}
>
> void Light::toggle() {
>	on = (!on);
> }
>
>bool Light::isOn() {
>	return (on);
>}
>```