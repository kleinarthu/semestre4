# Conversion d'un code en C en C++
- **const** pour créer des constantes,
- **inline** pour les fonctions en une ligne, remplacer à la compilation (⟺ macros),
- **namespace** pour créer un espace de nommage (encapsulation).
## Sorties
```Cpp
#include <iostream>
std::cout << "texte" << std::endl;
```

## Entrées
```Cpp
int trials;
std::cin >> trials;
```

## Conversion
```Cpp
static_cast<double>(valeur);
```

## Template
```Cpp
template <typename T>
inline void swap(T &i, T &j) {
	T tmp = i;
	i = j;
	j = tmp;
}
```

# Constructeur
>[!Define] Constructeur
>Fonction qui a le même  nom que la classe, sans type de retour et qui permet d'**initialiser un objet**.

```Cpp
Fraction::Fraction(int n, int d): numerateur{n} denominateur{d} {
}
// Équivalent
Fraction::Fraction(int n, int d) {
	numerateur = n;
	denominateur = d;
}
```
Mot-clé **new**.

# Destructeur
>[!Define] Destructeur
>**Libère l'espace** occupé par l'objet.
>Appelée automatiquement lorsque l'objet est détruit.

```Cpp
Fraction::~Fraction() {
}
```
Mot-clé **delete**.

# Copie
```Cpp
Fraction f1(5, 6);
Fraction f2 {f1};
```

# Fonction amie
>[!Define] Fonction amie
>Donne accès aux attributs ou méthodes privés à une méthode non-membre.

Mot-clé **friend**.

# Redéfinition d'opérateurs
```Cpp
// Dans la classe
Fraction Fraction::operator* (const Fraction& autre) const {
	return Fraction(numerateur * autre.numerateur, denominateur * autre.denominateur);
}

// Hors classe
Fraction operator* (const Fraction& f1, const Fraction& f2) {
	return Fraction(f1.getNum() * f2.getNum(), f1.getDenom() * f2.getDenom());
}
```

# Héritage
## Types d'héritages
```cpp
class Derivee: <mode de dérivation> Parent {
};
```
| Mode \\ Dans Parent | public    | private       | protected |
| ------------------- | --------- | ------------- | --------- |
| public              | public    | inaccessible  | protected |
| private             | private   | inaccessible  | private   |
| protected           | protected | inaccessible | protected |

## Redéfinition
```cpp
void Etudiant:afficher() const {
	Personne::afficher();
	std::cout << id << endl;
}
```

## Polymorphisme
>[!Define] Polymorphisme
>Capacité du système à **choisir dynamiquement** la méthode qui correspond au type de l'objet en cours de manipulation.

**virtual** dans la déclaration d'une méthode de la classe parente permet de **déléguer l'implémentation**.

## Classe abstraite
>[!Define] Classe abstraite
>Classe contenant au moins une fonction virtuelle pure.

>[!Define] Fonction virtuelle pure
>```cpp
>virtual type nom(parametres) = 0;
>```

# Exceptions
## Définition
```cpp
#include <exception>
class MonException: public exception {
	virtual const char* what() const throw() {
	return "Oups! Mon exception ...";
	}
}
```

## Utilisation
>[!Example]
>```cpp
>throw string("Erreur de calcul");
>throw MonException();
>```

## Récupération
```cpp
try {
	// code pouvant ammener une exception
} catch (int code) {
	cerr << "Exception" << code << endl;
} catch (exception& e) {
	cout << e.what() << endl;
}
```

## Exceptions pré-définies:
- **Exceptions de logique** : domain_error ; invalid_argument ; length_error ; out_of_range ; logic_error
- **Exceptions d'exécution** : range_error ; overflow_error ; runtime_error

# Gestion des entrées sorties
## Flux standards:
- cin (entrées),
- cout (sorties),
- cerr (erreurs).
```Cpp
if (!(in >> f.numerateur >> c >> f.denominateur) || (c != '\')) {
	in.setstate(std::ios::failbit);
}
```

## Flux physiques
```Cpp
#include <fstream>
```
- ofstream,
- ifstream,
- fstream.

### Utilisation
```Cpp
std::ifstream nomFlux("chemin"[, mode]);
std::ofstream nomFlux("chemin"[, mode]);
nomFlux.close();
```

### Modes d'ouverture
| Nom           | std::ios::in | std::ios::out | std::ios::app | std::ios::ate | std::ios::binary | std::ios::trunc |
| ------------- | ------------ | ------------- | ------------- | ------------- | ---------------- | --------------- |
| Signification | input        | output        | append        | at the end    | binary           | truncate                |

### Lecture par ligne
```Cpp
bool getline(ifstream& is, string& line); // true iff there are other lines
```

### État de la lecture
| Fonction | bad                  | fail                                     | eof            | good                       |
| -------- | -------------------- | ---------------------------------------- | -------------- | -------------------------- |
| True ssi | une opération échoue | une opération échoue ou erreur de format | fin de fichier | tous les autres sont false |

### Position dans un fichier
- **tellg** pour ifstream,
- **tellp** pour ofstream.

### Déplacement
```Cpp
seekg(pos, mode);
seekp(pos, mode);
// où pos est le nombre d'octet par rapport à mode parmis ios::beg, ios::cur et ios::end
```

# Template
>[!Define] Template
>Permet à une fonction ou classe d'utiliser des types différents.

>[!Example]
>``` Cpp
>template <typename T\>
>T mini(const T& a, const T& b) {
>	return (a < b ? a : b);
>}
>mini<int\>(i1, i2);
>```

>[!Warning] Attention
>À implémenter dans le .hpp.

# STL
>[!Define] STL
>Librairie très efficace contenant des algorithmes et structures de données fondamentaux.

## Conteneurs
- **Séquentiels**: vector, list(push_front(), pop_front, remove), stack, ...
- **Associatifs**: set, multiset, map, multimap, ...

## Iterator
```Cpp
for (vector<int>::iterator it = myvector.begin(); it != myvector.end(); ++it) {}
```

>[!Define] Algorithme
>Fonction template utilisée avec des itérateurs.

>[!Define] Foncteur
>Objet surchargeant **opertator()** pour être utilisé comme paramètre.

### for_each et lambda
>[!Example]
>```cpp
>for_each(v.begin(), v.end(), [](int i) {cout << i << endl;});