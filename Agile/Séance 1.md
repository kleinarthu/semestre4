Introduction aux méthodes agiles (notamment scrum).
Partie théorique cette séance, partie pratique la séance prochaine.
Groupe de 4: construction d'un backlog d'un logiciel (livrable pdf).

# I. Rappels de gestion de projet

Question classique d'entretien d'embauche:
>[!Define] Projet
>Mise en place de différents outils et méthodes pour réponde à un besoin, à des attentes.
>Ensemble d'activités coordonnées entrepris dans le but de répondre à des exigences:
>- un besoin défini
>- dans des délais définis
>-> cahier des charges (gestion des exigences)
>Avec des ressources allouées (notamment budgétaire mais aussi humaines, matériels, ...), et produisant des livrables (produit, documentation, ...)

>[!Define] Qualité
>Aptitude d'un projet à satisfaire ses exigences de départ.

## Gestion de projet classique:
La gestion de projet est l'ensemble des activités visant à organiser le bon déroulement d'un projet et à en atteindre les objectifs -> projet pour gérer un projet.

>[!Define] MOE
>**Le maître d'Œuvre** est la personne ou le groupe qui assure la production du projet dans le respect des délais, du budget et de la qualité attendue.

>[!Define] MOA
>**Le Maître d'Ouvrage** est la personne ou le groupe qui exprime le besoin. Il précise les objectifs, les délais et le budget alloué. Dans "ouvrage" il faut comprendre le produit qui sera livré à la fin du projet.

>[!Define] AMOA (= AMO)
>**L'Assistance à Maîtrise d'Ouvrage** est la personne ou le groupe qui fait l'interface entre la MOA et la MOE.

Dans une gestion de projet classique (Prins 2, PMI):

| Démarrage | ->  | Planification | ->  | Exécution    | ->  | Clôture |
| --------- | --- | ------------- | --- | ------------ | --- | ------- |
|           |     |               |     | Surveillance |     |         |

## Planification
1. PBS: Découper en sous-produits.
2. WBS: Découper en macro-tâches (conception, réalisation, vérification, permis, ...)
3. Analyse des risques ($gravité \times fréquence = criticité$) -> matrice de Farmer
4. Estimation du temps de chaque tâche grâce à un avis d'expert, à une analogie (méthode PERT: ${optimiste + pessimiste + 4 \times médian} \over 6$)
5. RBS: liste des moyens (surtout des participants), Matrice RACI (Réalisateur, Approbateur, Consultant, Intéressé)
6. OBS: "Mixer tout ça" -> faire un planning des tâches et employés (diagramme Gantt)
## Modèle en cascade
On ne va que vers l'avant, si ça ne marche pas on revient au début.

## Cycle en V
![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Cycle_V_temps_details.JPG)
Modèle déjà plus agile que le modèle waterfall (on se rend compte des erreurs plus rapidement), mais boucles non-optimisées.


| Besoin réel (inconnu) | →   | Besoin perçu    | →   | Besoin exprimé    |     |                    |
| --------------------- | --- | --------------- | --- | ----------------- | --- | ------------------ |
|                       |     |                 |     | ↳ Besoin retenu   | →   | Cahier des charges |
|                       |     | Besoin réalisé  | ←   | ↳ Besoin spécifié | →   | Doc. Spécification |
|                       |     | ↳ Produit livré |     |                   |     |                    |
**Urbanisation**: Par analogie avec l'architecture, prévoir comment la nouvelle application va s'intégrer dans l'écosystème existant.

$Conception = Conception unitaire + détaillée$

>[!Define] Test unitaire
>Tout test unitaire doit être écrit pour être répétable pour garantir qu'il fonctionnera toujours (non-régression).
>Peut et même devrait être écrit avant la programme.

>[!Define] Test d'intégration
>On vérifie que les composants s'assemblent bien. Joués à chaque modification des composants, ou livraison.

>[!Define] Test bout en bout (= n to n)
>Validation de la MOE avant livraison à la MOA

>[!Define] Recette
>Test complet par la MOA.
>Key Users qui connaissent le besoin testent le produit.
>Tests réalisé en boîte noire (la MOE n'a pas connaissance des tests effectués par la MOA).

>[!Define] MeP (= Mise en Production) / Release
>Après validation de la recette finale, le produit est mis en service.

>[!Define] Maintenance / TMA (= Tierce Maintenance Applicative)
>Nouveau projet à part entière, souvent pour une autre entreprise (contrat sur une certaine durée).
>C'est pour cela que les codes sources doivent être livrés avec l'application.

En cas de remontée de bug, vérifier si la résolution est un évolution (résolution nécessite un nouveau cahier des charges + devis) ou de la maintenance (résolution permet le cahier des charge initial).

# II. Agile
## Facteurs de succès d'un projet
- Spécifications complètes (pas de fonctionnalités inutiles ou manquantes), figées (participation des utilisateurs)
- Respect du planning ( -> coût fixe, possibilité de prendre moins d'employés pour travailler plus longtemps).

Gartner: Cabinet Américain dans l'IT

## Particularité de projets IT
Immatériels (intellectuels), uniques, innovants, besoins incertains, risqués
Taux de réussite de projets IT d'après Gartner: 29% de succès, 53% d'incertain, 18% d'échec
Taux d'utilisation de fonctionnalités particulières dans les projets IT par les utilisateurs: 7% tout le temps, 13% souvent, 16% de temps en temps, 19% rarement, 45% jamais

→ Volonté d'améliorations

## Modèle en W
![](https://c2iskillslouis.files.wordpress.com/2014/06/capture3.jpg)

## Roue de Deming
= PDCA (Planifier, Faire, Vérifier, Agir)→ roue de l'amélioration continue
Cale de système qualité (ISO 9001)

## Principe LEAN
1. Production de Valeur de produit
2. Processus: Flux de Valeur
3. Performance: pull/JIT
4. Personnes: respect, autonomie
5. Perfection: Amélioration continue

| Positionnement des méthodes | Niveau de management |
| --------------------------- | -------------------- |
| Lean                        | Business management  |
| Scrum                       | Project management   |
| Extreme programming         | Software engineering |

**Agile** apparaît dans la littérature en 2001 ([Manifeste Agile](https://manifesteagile.fr))
>[!Define] Agile
>Nous valorisons:
>- **Les individus et leurs interactions**, de préférence aux processus et outils
>- **Des solutions opérationnelles**, de préférence à un documentation exhaustive
>- **La collaboration avec les clients**, de préférence au respect d'un plan
>- **La réponse au changement**, de préférence au respect d'un plan.

## Principes
- Satisfaire le client
- Accueillir le changement de besoins
- Livrer souvent
- Construire le projet avec des personnes motivées
- Préférer la conversation en face-à-face
- La disponibilité de solutions opérationnelles est la meilleure mesure d'avancement.
- Garder un rythme constant
- Porter attention à la qualité
- La simplicité est essentielle
- Faire confiance aux gens
- Réfléchir plus pour faire moins d'erreur

>[!Define] Itération
>Période de temps entre deux livraisons.

>[!Define] Incrément
>Ce que l'on fait entre deux itérations.

Gestion classique favorise le respect du plan pré-établi.
Gestion agile favorise la valeur du produit.

# Scrum
Méthode la plus utilisée en entreprise, encore aujourd'hui.
Inventé par Ken Schwaber.
2010: [Guide scrum](https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-French.pdf)
![](https://www.pm-partners.com.au/wp-content/uploads/2021/06/blog-scrum-process-opt.jpg)
Réunions = Cérémonies (chronométrées)
Cahier des charges = User Story (courte phrase fonctionnelle et nominale)
>[!Define] Product Owner
>- Définit le besoin
>- Priorise le client
>- Valide les livraisons
>- N'est pas un supérieur hiérarchique

>[!Define] Scrum Master
>- Garant de la méthode
>- Protège et soutient la team
>- N'est pas le chef du projet

>[!Define] Team
>- 5 à 10 personnes
>- Analyse et estime
>- Réalise
>- Auto-organisée
>- Fixe ses propres règles

Les rôles peuvent tourner
## Projet Scrum
Suite de Sprint avec livraisons obligatoire entre eux et release plan initial après le Sprint 0.
Itération de 2 à 4 semaines.
Comprend suffisamment de User Stories pour occuper la Team pendant toute la période prévue.
Le nombre de User Stories que l'on peut faire est la capacité et elle est constatée à partir de la vélocité (déterminée lors du Sprint 0).

### Sprint 0:
- Test de la méthode
- Test la collaboration
- Permet d'estimer la productivité
- Test la DoD (Definition of Done)
- $Capacité = Vélocité \times durée du sprint$

### Anatomie du Sprint
Planning: → Réalisation (Affinage = retravailler le cahier des charges, Déploiement du sprint précédent, Dailys, Rétrospective = test et réflexion sur la qualité)→ Review

## Format des User Stories
Les Users Stories suivent le format des 3 C:
- Card
- Conversation: les détails qui ne sont pas sur la carte sont discutés à l'oral.
- Confirmation: Lister au dos de la carte les cas de tests.
>[!Example] Card
>- En tant que \<rôle\>
>- Je veux \<atteindre un but\>
>- En \<effectuant une action\> (facultatif)

>En tant qu'utilisateur, je veux m'authentifier avec un login/mdp.
>En tant qu'administrateur, je veux bannir un user ayant tenu des propos déplacés.

Possibilité d'utiliser des post-it ou les outils de github / gitlab.

Propriétés d'une story:
**INVEST** = Independent, Negotiable, Valuable, Estimable, Small (un sprint), Testable

L'estimation se fait en **Point d'Effort**.
>[!Define] PE
>Estimation en complexité et en incertitude et non en temps.
>Généralement, c'est un chiffre de la suite de Fibonacci entre 1 et 8.
>Pour l'estimer, on fait une séance de Planning Poker.
>L'estimation en temps se fait grâce à la vélocité.

Priorité déterminée par la méthode:
- informelle (MoSCoW = Must, Should, Could, Would)
- KANO
- RICE
- ...

>[!Define] Product Backlog
>Ensemble des Users Stories estimées et priorisées.
>Fait au début du projet et à chaque séance d'affinage.
>Tableau contenant le statut (fait/en cours/à faire).

## Artefacts
**Release plan** (= Product Backlog) effectué après la sprint 0 pour connaître la vélocité, et après avoir déterminer les stories.
Les itérations/releases sont planifiées.
Chaque User Story du Product Backlog est assignée à un sprint.

**Release Burndown-Chart** :
Visualisation de l’avancement global du projet (somme des points d’efforts à faire)

**Sprint Backlog** similaire au Product Backlog

**Sprint Burndown Chart**

## Cérémonies
### Sprint-planning:
Le PO choisit les stories qu’il désire afin de satisfaire un objectif de Sprint.
La Team découpe chaque US en tâches techniques et répartit le travail.
Time-boxée

### Réalisation
Dès qu’une tâche est terminée, on vérifie la DoD.

### Daily Meeting
Point quotidien à heure fixe
On reste debout (dynamisme)
- Qu’ai-je fait hier ?
- Que vais-je faire aujourd’hui ?
- Ai-je des problèmes ?
- Reste à faire ?
Très rapide, pas de hiérarchique.

### Rétrospective  
Séance de réflexion sur la méthode scrum elle-même (horaire de la réunion, …).
Souvent sous forme de jeu (Speedboat, Keep/Drop/Start).

>[!Define] Epic
>US trop large pour entrer dans un sprint. ($PE > vélocité$).
>→ Story Splitting (pendant le refinment): découpage en US.

WCAG: règles d'accessibilité sur internet (champs aria, ...)

>[!Define] DoR
>### Definition of Ready
>À définir au début du projet.
>Accepté par tout le monde.
>À afficher.
>Check-list à vérifier avant d'ajouter une US au sprint (INVEST, 3C).
>

>[!Define] DoD
>### Definition of Done
>Checklist destinée au développeur pour déterminer ce qu'est une US finie (code OK commit, conventions de nommages, tests unitaires, doc à jour).

