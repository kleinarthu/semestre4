#  Gestion de projet classique

## Rappel: Projet
>[!Define] Projet
>Mise en place de différents outils et méthodes pour réponde à un besoin, à des attentes.
>Ensemble d'activités coordonnées entrepris dans le but de répondre à des exigences:
>- un besoin défini
>- dans des délais définis
⟶ cahier des charges (gestion des exigences)
>Avec des ressources allouées (notamment budgétaire mais aussi humaines, matériels, ...), et produisant des livrables (produit, documentation, ...)

>[!Define] Qualité
>Aptitude d'un projet à satisfaire ses exigences de départ.

## Rôles
>[!Define] MOE
>**Le maître d'Œuvre** est la personne ou le groupe qui assure la production du projet dans le respect des délais, du budget et de la qualité attendue.

>[!Define] MOA
>**Le Maître d'Ouvrage** est la personne ou le groupe qui exprime le besoin. Il précise les objectifs, les délais et le budget alloué. Dans "ouvrage" il faut comprendre le produit qui sera livré à la fin du projet.

>[!Define] AMOA (= AMO)
>**L'Assistance à Maîtrise d'Ouvrage** est la personne ou le groupe qui fait l'interface entre la MOA et la MOE.

## Déroulement classique
>[!Define] Cycle en V
>![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Cycle_V_temps_details.JPG)

## Tests et correction
>[!Define] Test unitaire
>Tout test unitaire doit être écrit pour être répétable pour garantir qu'il fonctionnera toujours (non-régression).
>Peut et même devrait être écrit avant la programme.

>[!Define] Test d'intégration
>On vérifie que les composants s'assemblent bien. Joués à chaque modification des composants, ou livraison.

>[!Define] Test bout en bout (= n to n)
>Validation de la MOE avant livraison à la MOA

>[!Define] Recette
>Test complet par la MOA.
>Key Users qui connaissent le besoin testent le produit.
>Tests réalisé en boîte noire (la MOE n'a pas connaissance des tests effectués par la MOA).

>[!Define] MeP (= Mise en Production) / Release
>Après validation de la recette finale, le produit est mis en service.

>[!Define] Maintenance / TMA (= Tierce Maintenance Applicative)
>Nouveau projet à part entière, souvent pour une autre entreprise (contrat sur une certaine durée).
>C'est pour cela que les codes sources doivent être livrés avec l'application.

#  Méthodes Agiles
>[!Info]
>[Manifeste Agile](https://manifesteagile.fr)

>[!Define] Agile
>Nous valorisons:
>- **Les individus et leurs interactions**, de préférence aux processus et outils
>- **Des solutions opérationnelles**, de préférence à un documentation exhaustive
>- **La collaboration avec les clients**, de préférence au respect d'un plan
>- **La réponse au changement**, de préférence au respect d'un plan.

## Principes
- Satisfaire le client
- Accueillir le changement de besoins
- Livrer souvent
- Construire le projet avec des personnes motivées
- Préférer la conversation en face-à-face
- La disponibilité de solutions opérationnelles est la meilleure mesure d'avancement.
- Garder un rythme constant
- Porter attention à la qualité
- La simplicité est essentielle
- Faire confiance aux gens
- Réfléchir plus pour faire moins d'erreur

## Scrum
[Guide scrum](https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-French.pdf)
![](https://www.pm-partners.com.au/wp-content/uploads/2021/06/blog-scrum-process-opt.jpg)

### Rôles
>[!Define] Product Owner
>- Définit le besoin
>- Priorise le client
>- Valide les livraisons
>- N'est pas un supérieur hiérarchique

>[!Define] Scrum Master
>- Garant de la méthode
>- Protège et soutient la team
>- N'est pas le chef du projet

>[!Define] Team
>- 5 à 10 personnes
>- Analyse et estime
>- Réalise
>- Auto-organisée
>- Fixe ses propres règles

### Déroulement
>[!Define] DoR
>### Definition of Ready
>À définir au début du projet.
>Accepté par tout le monde.
>À afficher.
>Check-list à vérifier avant d'ajouter une US au sprint (INVEST, 3C).
>

>[!Define] DoD
>### Definition of Done
>Checklist destinée au développeur pour déterminer ce qu'est une US finie (code OK commit, conventions de nommages, tests unitaires, doc à jour).
Suite de Sprint avec livraisons obligatoire entre eux et release plan initial après le Sprint 0.
Itération de 2 à 4 semaines.

#### Sprint 0:
- Test de la méthode
- Test la collaboration
- Permet d'estimer la productivité
- Test la DoD (Definition of Done)
- $Capacité = Vélocité \times durée du sprint$

#### Anatomie du Sprint
Planning: → Réalisation (Affinage = retravailler le cahier des charges, Déploiement du sprint précédent, Dailys, Rétrospective = test et réflexion sur la qualité)→ Review

### Documents
#### Format des User Stories
Les Users Stories suivent le format des 3 C:
- Card
- Conversation: les détails qui ne sont pas sur la carte sont discutés à l'oral.
- Confirmation: Lister au dos de la carte les cas de tests.
>[!Example] Card
>- En tant que \<rôle\>
>- Je veux \<atteindre un but\>
>- En \<effectuant une action\> (facultatif)

Propriétés d'une story:
**INVEST** = Independent, Negotiable, Valuable, Estimable, Small (un sprint), Testable

>[!Define] PE
>Estimation en complexité et en incertitude et non en temps.
>Généralement, c'est un chiffre de la suite de Fibonacci entre 1 et 8.
>Pour l'estimer, on fait une séance de Planning Poker.
>L'estimation en temps se fait grâce à la vélocité.

Priorité déterminée par la méthode:
- informelle (MoSCoW = Must, Should, Could, Would)
- KANO
- RICE
- ...

>[!Define] Product Backlog
>Ensemble des Users Stories estimées et priorisées.
>Fait au début du projet et à chaque séance d'affinage.
>Tableau contenant le statut (fait/en cours/à faire).

>[!Define] Epic
>US trop large pour entrer dans un sprint. ($PE > vélocité$).
>→ Story Splitting (pendant le refinment): découpage en US.
formattage
#### Artefacts
**Release plan** (= Product Backlog) effectué après la sprint 0 pour connaître la vélocité, et après avoir déterminer les stories.
Les itérations/releases sont planifiées.
Chaque User Story du Product Backlog est assignée à un sprint.

**Release Burndown-Chart** :
Visualisation de l’avancement global du projet (somme des points d’efforts à faire)

**Sprint Backlog** similaire au Product Backlog

**Sprint Burndown Chart**

### Cérémonies
>[!Define] Sprint-planning:
>Le PO choisit les stories qu’il désire afin de satisfaire un objectif de Sprint.
>La Team découpe chaque US en tâches techniques et répartit le travail.
Time-boxé

## Construction du Backlog
### 1. Product Vision Board
- Vision: courte phrase type slogan pour décrire le produit
- Groupe Cibles
- Besoins auxquels répondent le produit
- Produit
- Valeur (inverse de besoin + valeur pour toi)

### 2. Persona
Imaginer des utilisateurs factices (avec nom, prénom, genre, âge, technologie possédée...).

### 3. User Journey
Ensemble de scénarios d'utilisation du produit par une persona, représentés sous forme de diagrammes.

### 4. Story Mapping
Chaque étape de User Journey devient en-tête de colonne.
Mettre plusieurs variantes permettant à l'utilisateur d'accomplir ce qui est en en-tête.
Déterminer le MVP en choisissant un ou deux éléments par colonne ⟶ minimum pour marketer.

### 5. User Stories
Écrire les User Stories en suivant les 3 C à partir des story mapping.
