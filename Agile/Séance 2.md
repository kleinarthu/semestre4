Ne pas dire la méthode agile:
- soit les méthodes agiles,
- soit une méthode agile et dire son nom.

Scrum Master = Ingénieur Qualité

# Construction du Backlog
1. Canvas de la Vision Produit
2. Personas
3. User Journey

## 1. Product Vision Board
- Vision: courte phrase type slogan pour décrire le produit
- Groupe Cibles
- Besoins auxquels répondent le produit
- Produit
- Valeur (inverse de besoin + valeur pour toi)

⟹Fixer les objectifs

## 2. Persona
Imaginer des utilisateurs factices (avec nom, prénom, genre, âge, technologie possédée...).

## 3. User Journey
Ensemble de scénarios d'utilisation du produit par une persona, représentés sous forme de diagrammes.

## 4. Story Mapping
Chaque étape de User Journey devient en-tête de colonne.
Mettre plusieurs variantes permettant à l'utilisateur d'accomplir ce qui est en en-tête.
Faire du plus simple au plus compliqué.
Déterminer le MVP en choisissant un ou deux éléments par colonne ⟶ minimum pour marketer.

>[!Quote]
>Si tu n'as pas honte de ta première version, c'est que tu l'as lancée trop tard.

## 5. User Stories
Écrire les User Stories en suivant les 3 C à partir des story mapping.
![[Agile/Séance 1#Format des User Stories]]