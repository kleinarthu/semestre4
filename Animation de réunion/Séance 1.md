# Organisation
Chaque cours est animé par un groupe d’élève.
Dans la classe, 14 personnes n’ont jamais animé de réunion, 2 en ont animé une fois et 6 en ont animé plusieurs fois.
Beaucoup d’expériences sont des expériences de stages ou d’associations de l’école (peu professionnelles ou informatives)
30 min/réunion + debrief
Roles : 1 animateur, 5 participants, 2 observateurs
Choix du sujet : sujets en lien avec le [développement durable](https://www.undp.org/fr/sustainable-development-goals)

## 3 Phases:
- Avant la réunion, fiche de préparation à envoyer à fmartinconseil@gmail.com + convocation à tous les participants (24h avant la réunion)
- Pendant la réunion, réguler le temps de parole, rester dans le sujet, gérer le temps, demander à quelqu'un de gérer le temps, assurer la prise de note pour le compte-rendu
- Tout de suite après, envoyer un compte-rendu

## Évaluation
- 1 point pour la fiche de préparation et la convocation
- 1 point pour le compte-rendu
- 4 points pour l'assiduité (être présent 8 fois)
- 14 points pour le rapport (3 pour la préparation + 2 pour la convocation + 2 pour le compte-rendu + 5 pour l'analyse en 1 pages + 2 pour la présentation du rapport).

# Généralités sur les réunions
1 cadre passe 16 ans en réunion

## Types de réunion
Réunion descendante: donner des informations
Réunion ascendante: demander des réunions
Réunion de synchronisation
Co-construction: créativité, brainstorming, résolution de problème, prise de décision

## Répartition du temps d'organisation
- 50% pour la préparation
- 20% pour la réunion
- 30% pour le compte-rendu

## Préparation
Type de réunions, objectifs (livrable), possiblement rôle à jouer, déroulé, outils, participants, …
Les objectifs doivent être SMART (spécifique, mesurable, adapté, réaliste, adapté dans le temps)
Ordre du jour : ce qui va être communiqué, les points abordés
Techniques pour chaque phase
Qui convoquer (CRI : compétence ou contributeur, responsable, intéressé)
Modèle de préparation dans le google slide
Convocation contient:
- objectif de la réunion
- date, heure de début, fin, durée
- lieu
- ...

## Déroulé
Déroulé : démarrage (contexte, objectif, cadre, règles, brise-glace) + plusieurs séquences + conclusion (synthèse, merci, prochaines étapes)